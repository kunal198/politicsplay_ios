//
//  NominateFirstVC.swift
//  LayoutScreens
//
//  Created by Brst on 30/10/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class NominateFirstVC: UIViewController {
    @IBOutlet weak var serByNameTxt: UITextField!
    @IBOutlet weak var whichOfficeTxt: UITextField!
    @IBOutlet weak var nominateBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        contentsBorder()
    }
    func contentsBorder()
    {
        nominateBtn.layer.cornerRadius = 22
       
        setFieldBorder(textField: serByNameTxt)
        setFieldBorder(textField: whichOfficeTxt)
    }
    func setFieldBorder(textField: UITextField)
    {
        textField.layer.cornerRadius = 20
        textField.layer.borderColor = UIColor .lightGray .cgColor
        textField.layer.borderWidth = 1.0
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
