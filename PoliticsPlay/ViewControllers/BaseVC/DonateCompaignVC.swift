//
//  DonateCompaignVC.swift
//  LayoutScreens
//
//  Created by Brst on 30/10/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class DonateCompaignVC: UIViewController {
    @IBOutlet weak var scroll_View: UIScrollView!
    @IBOutlet weak var donateBtn: UIButton!
    @IBOutlet weak var creCardNumTxt: UITextField!
    @IBOutlet weak var cvvNumTxt: UITextField!
    @IBOutlet weak var expirationTxt: UITextField!
     @IBOutlet weak var emailTxt: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        contentsBorder()
    }
    func contentsBorder()
    {
        setFieldBorder(textField: creCardNumTxt)
        setFieldBorder(textField: cvvNumTxt)
        setFieldBorder(textField: expirationTxt)
        setFieldBorder(textField: emailTxt)
        
        donateBtn.layer.cornerRadius = 22
        
    }

    func setFieldBorder(textField: UITextField)
    {
        textField.layer.cornerRadius = 5
        textField.layer.masksToBounds = false
        textField.layer.shadowRadius = 1.0
        textField.layer.shadowColor = UIColor.black.cgColor
        textField.layer.shadowOffset = CGSize(width: 1.0,height: 1.0)
        textField.layer.shadowOpacity = 0.4
    }
    override func viewWillLayoutSubviews() {
        scroll_View.contentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height+100)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
