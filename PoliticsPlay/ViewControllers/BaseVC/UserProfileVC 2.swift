//
//  UserProfileVC.swift
//  LayoutScreens
//
//  Created by Brst on 01/11/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class UserProfileVC: UIViewController, UITableViewDelegate, UITableViewDataSource{
    @IBOutlet weak var table_View: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        table_View.showsVerticalScrollIndicator = false
        // Do any additional setup after loading the view.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)as! UserProfileTableViewCell
        
        cell.cellContentView.layer.masksToBounds = false
        cell.cellContentView.layer.shadowColor = UIColor.black.cgColor
        cell.cellContentView.layer.shadowOpacity = 0.5
        cell.cellContentView.layer.shadowOffset = CGSize(width: -1, height: 1)
        cell.cellContentView.layer.shadowRadius = 1
        cell.cellContentView.layer.shadowPath = UIBezierPath(rect: cell.cellContentView.bounds).cgPath
        cell.cellContentView.layer.shouldRasterize = true
        cell.cellContentView.layer.rasterizationScale = UIScreen.main.scale
        return cell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
