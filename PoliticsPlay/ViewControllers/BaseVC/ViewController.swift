//
//  ViewController.swift
//  LayoutScreens
//
//  Created by Brst on 30/10/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var serByNameTxt: UITextField!
    @IBOutlet weak var serByOfficeTxt: UITextField!
    @IBOutlet weak var costTxt: UITextField!
    @IBOutlet weak var donateBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        contentsBorder()
    }
    func contentsBorder()
    {
        donateBtn.layer.cornerRadius = 22
        
        setFieldBorder(textField: serByNameTxt)
        setFieldBorder(textField: serByOfficeTxt)
        setFieldBorder(textField: costTxt)
    }
    func setFieldBorder(textField: UITextField)
    {
        textField.layer.cornerRadius = 20
        textField.layer.borderColor = UIColor .lightGray .cgColor
        textField.layer.borderWidth = 1.0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

