//
//  MyProfileVC.swift
//  PoliticsPlay
//
//  Created by Brst981 on 30/10/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage
import AVFoundation
class MyProfileVC: UIViewController,UITableViewDelegate,UITableViewDataSource,CustomizeProfileVCDelegate,WhatsOnMindVCDelegate,FollowingViewControllerDelegate,UIScrollViewDelegate {
    
    
  //Outlets
    
    @IBOutlet var viewStatus: UIView!
    @IBOutlet var viewWhatsOnMind: UIView!
    @IBOutlet var profileImageVC: UIImageView!
    @IBOutlet var lblNameVC: UILabel!
    @IBOutlet var lblFollowersVC: UILabel!
    @IBOutlet var lblFollowingVC: UILabel!
    @IBOutlet var lblSupportersVC: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var statusCaption: UILabel!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var lblPartyName: UILabel!
    @IBOutlet var lblLastNameVC: UILabel!
    @IBOutlet var activityIndicatorPagination: UIActivityIndicatorView!
    @IBOutlet var lblPost: UILabel!
    @IBOutlet var viewHeader: UIView!
    @IBOutlet var btnShuffle: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    
    
    //Variables
    var captilizedStringFName = String()
    var captilizedStringLName = String()
    let dataArray = NSMutableArray()
    var moreScroll = Bool()
    var indexingDict = [String: Any]()
    var currentIndex =  1
    var userIdValueAgain = String()
    var authTokenValue = String()
    var valueKey = Bool()
    var likeKeyStatus = String()
    var actionKey = String()
    var statusCaptionNew = String()
    var choosePartyNew = String()
    var descpStr = String()
  
    @IBOutlet weak var lblNoFeeds: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        customizeViews()
        // Do any additional setup after loading the view.
   
        self.lblPost.isHidden = true
        
      // code for making img view round
        profileImageVC.clipsToBounds = true
        profileImageVC.contentMode = .scaleAspectFill
        profileImageVC.layer.cornerRadius = profileImageVC.frame.size.height/2

        
      
        UITabBar.appearance().tintColor = UIColor.white
  
        
        
        self.activityIndicator.isHidden = true
        
        // code for fetching the fresh profile data.
        
        if UserDefaults.standard.value(forKey: "userId") != nil{
            
            userIdValueAgain = (UserDefaults.standard.value(forKey: "userId") as? String)!
            authTokenValue = (UserDefaults.standard.value(forKey: "authToken") as? String)!
            getProfileDataMethd(userId: userIdValueAgain , authToken: authTokenValue )
            
        }
        
        
        
        
        
        // code for showing post through pagination
        if UserDefaults.standard.value(forKey: "userId") != nil{
            
            userIdValueAgain = (UserDefaults.standard.value(forKey: "userId") as? String)!
            authTokenValue = (UserDefaults.standard.value(forKey: "authToken") as? String)!
            postData(userId: userIdValueAgain , authToken: authTokenValue , page: "1"){
            (result:
                Bool) in
                print("got back: \(result)")
                
            }
       
      }
        
        
        if valueKey {
            
            btnShuffle.setImage(UIImage.init(named: "back2"), for: .normal)
            
        }
            
        else{
            
            btnShuffle.setImage(UIImage.init(named: "tg-bar"), for: .normal)
        }
        
    
    
     
  
        self.lblNoFeeds.isHidden = true
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
      // code for fetching profile in start
        
        if UserDefaults.standard.value(forKey: "userId") != nil{
            
            userIdValueAgain = (UserDefaults.standard.value(forKey: "userId") as? String)!
            authTokenValue = (UserDefaults.standard.value(forKey: "authToken") as? String)!
            self.dataArray.removeAllObjects()
            postData(userId: userIdValueAgain , authToken: authTokenValue , page: "1"){
                (result:
                Bool) in
                print("got back: \(result)")
                
            }
            
        }
        
     
        self.navigationController?.isNavigationBarHidden = true

        
    }
    
   override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func customizeViews(){
        
    viewStatus.layer.shadowColor = UIColor.black.cgColor
    viewStatus.layer.shadowOpacity = 0.25
    viewStatus.layer.shadowRadius = 1.0
    viewStatus?.layer.shadowOffset = CGSize(width: 1, height: 1.0)

    viewWhatsOnMind.layer.shadowColor = UIColor.black.cgColor
    viewWhatsOnMind.layer.shadowOpacity =  0.25
    viewWhatsOnMind.layer.shadowRadius = 1.0
    viewWhatsOnMind?.layer.shadowOffset = CGSize(width: 1, height: 1.0)

    }
    
    @IBAction func toggleLeft(_ sender: Any) {
    
        
        if valueKey {
            
        self.navigationController?.popViewController(animated: true)
            
        }
            
        else{
            
            slideMenuController()?.toggleLeft()
            
        }
        
        
        
    
    }
    
    
   // code to push to cutomize your profile screen
    @IBAction func pushToCustomProfile(_ sender: Any){
    
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
              let nextViewController = storyBoard.instantiateViewController(withIdentifier:
                    "CustomizeProfileVC") as! CustomizeProfileVC
             nextViewController.delegate = self
            self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    @IBAction func pushToWhatsonMindScreen(_ sender: Any) {
   
    
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier:
            "WhatsOnMindVC") as!  WhatsOnMindVC
         nextViewController.delegate = self
         nextViewController.type = "whatsonmind"
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
        
    
    }
    
    @IBAction func pushToFolllowers(_ sender: Any) {
   
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier:
            "FolllowersViewController") as! FolllowersViewController
        nextViewController.checkProfile = true
        self.navigationController?.pushViewController(nextViewController, animated: true)
    
    }
    
    @IBAction func pushToFollowing(_ sender: Any) {
   
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier:
            "FollowingViewController") as! FollowingViewController
        nextViewController.checkProfile = true
        nextViewController.delegate = self
        self.navigationController?.pushViewController(nextViewController, animated: true)
    
    }

    @IBAction func pushToCandidateSupporters(_ sender: Any) {
   
    
    
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier:
            "CandidateSupportVC") as! CandidateSupportVC
        self.navigationController?.pushViewController(nextViewController, animated: true)
    
    }
    
    @IBAction func pushToDetailUserProfile(_ sender: Any) {
    
    }
    
  
    
    // table view methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return dataArray.count
       
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: "profileCell", for: indexPath) as! ProfileCell
        self.lblNoFeeds.isHidden = true
        let dict = self.dataArray[indexPath.row] as! NSDictionary
        self.likeKeyStatus = dict.value(forKey: "is_likedbyuser") as! String
        print(self.likeKeyStatus)
           print(self.dataArray)
         if (self.likeKeyStatus == "yes"){
                
                cell.lblLike.text = "Like"
                cell.lblLike.textColor = UIColor.blue
                cell.imgLike.image = UIImage(named:"unlike")
        }
        else{
            
            cell.lblLike.text = "Like"
            cell.lblLike.textColor = UIColor.black
            cell.imgLike.image = UIImage(named:"like")
            
        }
        
        let text = dict["description"] as? String
        if text == ""{
 
            cell.lblDescription.text =  "No description added"
        }
        else{
        
            cell.lblDescription.text = dict["description"] as? String
        }
        cell.lblDate.text = dict.value(forKey:"post_date") as? String
        cell.lblTime.text = dict.value(forKey:"post_time") as? String
        let thumbnailImage = dict.value(forKey: "thumbnail")
        print(thumbnailImage!)
       let img_url  = dict.value(forKey:"image") as? String
        let valueImage = NSURL(string :img_url!)?.pathExtension
        if valueImage == "jpg" || valueImage == "png" {
        
           // cell.lblImage.sd_setImage(with: URL(string: img_url!), placeholderImage: UIImage(named: "noimage"))
            cell.thumbnailIcon.image = UIImage(named: "")
            cell.outerImgView.sd_setImage(with: URL(string: img_url! ), placeholderImage: UIImage(named: "noimage"))
          //  return cell
        }
        
       
         else if(valueImage == "mp4") {
        
          cell.outerImgView.sd_setImage(with: URL(string: thumbnailImage as! String), placeholderImage: UIImage(named: "noimage"))
        //  cell.lblImage.sd_setImage(with: URL(string: thumbnailImage as! String), placeholderImage: UIImage(named: "noimage"))
          cell.thumbnailIcon.image = UIImage(named: "playbutton")
         // return cell
       
        }
       
        else{
          
        //  cell.lblImage.image = UIImage(named : "noimage")
          cell.outerImgView.sd_setImage(with: URL(string: thumbnailImage as! String), placeholderImage: UIImage(named: "noimage"))
          cell.thumbnailIcon.image = UIImage(named: "")
        
        }
        
        cell.cellContentView.layer.cornerRadius = 4
        cell.cellContentView.layer.shadowColor = UIColor.black.cgColor
        cell.cellContentView.layer.shadowOffset = CGSize(width: 1,height: 1)
        cell.cellContentView.layer.shadowOpacity = 0.5
        cell.cellContentView.layer.shadowRadius = 1.0
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        // Mark: For like unlike scenario
        cell.btnLike.tag = indexPath.row
        cell.btnLike.addTarget(self, action: #selector(btnLikeUnlikeMethod), for: UIControlEvents.touchUpInside)
        cell.selectionStyle = .none

        
        
        return cell
        
    }
    
    func btnLikeUnlikeMethod(_ sender: UIButton){
        
        self.authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
        print(self.authTokenValue)
        
        print(sender.tag)
        let dictUserProfile = dataArray[sender.tag] as! NSDictionary
        let postID = dictUserProfile.value(forKey: "post_id") as! Int
        var index = IndexPath()
        index = IndexPath.init(item: Int(sender.tag), section: 0)
        
        let json = ["user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":self.authTokenValue,
                    "post_id":postID
            
            ] as [String : Any]
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/politicsplay/api/web/v1/posts/likeunlikepost", method: .post, parameters:                                           json, encoding: URLEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
              //  self.actionKey = json["action"].string!
                if UserDefaults.standard.value(forKey: "userId") != nil{

                    self.userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as! String
                    self.authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
                    print(self.userIdValueAgain)
                    print(self.authTokenValue)
                    self.dataArray.removeAllObjects()
                    
                    self.postData(userId: self.userIdValueAgain,authToken: self.authTokenValue,page: "1"){
                        
                        (result:
                        Bool) in
                        print("got back: \(result)")
                    }

                }
                
                
                
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let postDetailViewController = storyBoard.instantiateViewController(withIdentifier:
            "DetailPost") as! DetailPost
        let dict = self.dataArray[indexPath.row] as! NSDictionary
        postDetailViewController.postTypeDetail = "Profile"
        postDetailViewController.postIDDetail = dict.value(forKey: "post_id") as! Int
        postDetailViewController.postArray = [self.dataArray[indexPath.row]]
        postDetailViewController.postDict = self.dataArray[indexPath.row] as! NSDictionary
        postDetailViewController.profile_Pic =  Singleton.sharedInstance.userImageStr
        postDetailViewController.likedByMe = dict.value(forKey: "is_likedbyuser") as! String
       self.navigationController?.pushViewController(postDetailViewController, animated: true)
     }
    
    // get post through pagination
    func postData(userId: String,authToken: String,page: String,completion: @escaping (_ result: Bool) -> Void){
        
        DispatchQueue.main.async {
          
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
            
        }
        let json = ["page":page,
                    "user_id":userId,
                    "auth_token":authToken,
                    ] as [String : Any]
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/politicsplay/api/web/v1/users/get_post", method: .post, parameters:  json, encoding: URLEncoding.default)
            .responseJSON { response in
               
                 print(response.result)   // result of response serialization
                print(response)
                if((response.error) != nil){
                    
                    let alertView = UIAlertController(title: "", message:(response.error?.localizedDescription)! , preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                        
                    })
                    alertView.addAction(action)
                    self.present(alertView, animated: true, completion: nil)
                    
                }
                DispatchQueue.main.async {
                    
                    let json = JSON(data: response.data!)
                    let data = json["data"].arrayObject
                    if data == nil{
                        
                     print("error")
                        
                    }
                    self.descpStr = (json["message"].string)!
                    if self.descpStr == "No data found"{
                        
               
                     self.lblNoFeeds.isHidden = false
                
                    }
                    if self.descpStr == "User id and authentication token not matched"{
                    
                        let alertView = UIAlertController(title: "", message:"Your session has been expired.You need to login again" , preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                       
                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                            
                            let signViewController = storyBoard.instantiateViewController(withIdentifier:
                                "LoginVC") as! LoginVC
                            self.navigationController?.pushViewController(signViewController, animated: true)
                         })
                        alertView.addAction(action)
                        self.present(alertView, animated: true, completion: nil)
                    
                    }
                    else if ((data) != nil){
                        
                        
                        
                        self.dataArray.addObjects(from: data!)
                          print(self.dataArray)
                        self.indexingDict = (json["pagination"].dictionaryObject)!
   
                    }
                  
                
                 
                    DispatchQueue.main.async {
                        self.activityIndicator.isHidden = true
                        self.activityIndicator.stopAnimating()
                        self.tableView.reloadData()

                    } // end of dispatch
                    
                    
                    
            } // end of one step ahead disaptch
                
                
                
        } // end of alamofire methd
        
    }
    
   
    // Mark: For managing the pagination work
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        
        guard indexingDict.count > 0, let currentpage = (indexingDict["currentpage"] as? NSString)?.integerValue, let lastpage = (indexingDict["lastpage"] as? NSString)?.integerValue, currentpage < lastpage , let nextpage = (indexingDict["nextpage"] as? NSString)?.integerValue else {
            print("We are on last page")
            return
        }
        
      
        
        postData(userId: userIdValueAgain , authToken: authTokenValue , page: String(nextpage)){
            (result:
            Bool) in
            print("got back: \(result)")
            
        }
        
    }
    
    
    
  
    
  
    
    // getProfileDataMethd api integration
    
    func getProfileDataMethd(userId: String,authToken: String){
        
        
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        
        let json = ["user_id":userId,
                    "auth_token":authToken,
                  
            ] as [String : Any]
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/politicsplay/api/web/v1/users/get_profile", method: .post, parameters:                                           json, encoding: URLEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if((response.error) != nil){
                    
                    let alertView = UIAlertController(title: "", message:(response.error?.localizedDescription)! , preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                        
                    })
                    alertView.addAction(action)
                    self.present(alertView, animated: true, completion: nil)
                    
                }
                DispatchQueue.main.async {
                    
                    let json = JSON(data: response.data!)
                    let descpStr = json["message"].string
               if  descpStr  == "User profile" {
                //self.lblPartyName.text = ""
                Singleton.sharedInstance.userImageStr = json["data"]["profile_pic"].string!
                print(Singleton.sharedInstance.userImageStr)
                Singleton.sharedInstance.emailStr = json["data"]["email"].string!
                Singleton.sharedInstance.firstNameStr = json["data"]["firstname"].string!
                print(Singleton.sharedInstance.firstNameStr)
                UserDefaults.standard.set(Singleton.sharedInstance.firstNameStr, forKey: "profileName")
                UserDefaults.standard.synchronize()
                Singleton.sharedInstance.lastNameStr = json["data"]["lastname"].string!
                Singleton.sharedInstance.noOfFollwers = json["data"]["no_of_followers"].int!
                Singleton.sharedInstance.noOfFollowing = json["data"]["no_of_following"].int!
                Singleton.sharedInstance.noOfSupporters = json["data"]["no_of_supporters"].int!
                self.choosePartyNew = json["data"]["party"].string!
                self.statusCaptionNew = json["data"]["status_caption"].string!
                Singleton.sharedInstance.userIdStr = String(describing: json["data"]["user_id"].int!)
                print(Singleton.sharedInstance.userIdStr)
                if self.choosePartyNew == ""{
                    
                    self.lblPartyName.text = "No Party Choosen"
                    self.lblPartyName.font = self.lblPartyName.font.withSize(17)
                    self.lblPartyName.textColor = UIColor.gray

                }
                else{
                    
                    self.lblPartyName.text = self.choosePartyNew
                    self.lblPartyName.font = self.lblPartyName.font.withSize(17)
                    self.lblPartyName.textColor = UIColor.gray

                    
                }
                
                if  self.statusCaptionNew == ""
                {
                    self.statusCaption.text = "No Caption Choosen"
                  //  self.statusCaption.font = UIFont.init(name: , size: size_Fixed)
                    self.statusCaption.font = self.statusCaption.font.withSize(17)
                    self.statusCaption.textColor = UIColor.gray

                }
                else{
                    
                    self.statusCaption.text =  self.statusCaptionNew
                    self.statusCaption.font = self.statusCaption.font.withSize(17)
                    self.statusCaption.textColor = UIColor.gray


                }
                
                
               self.showVisibleData()
        }
            
        if descpStr == "Userid and authentication token mismatched"{
            
            let alertView = UIAlertController(title:"", message:"Your session has been expired.Please login again" , preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
            
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                
                let signViewController = storyBoard.instantiateViewController(withIdentifier:
                    "LoginVC") as! LoginVC
                self.navigationController?.pushViewController(signViewController, animated: true)
            })
            alertView.addAction(action)
            self.present(alertView, animated: true, completion: nil)

       }
            
       
                    
            DispatchQueue.main.async {
                        
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
                        
            }
                    
                } // end of main dispatch
            } // end of alamofire methd
        
    }
    
    
    // func show data
    
    func showVisibleData(){
        
        captilizedStringFName = Singleton.sharedInstance.firstNameStr.capitalized
        print(captilizedStringFName)
        captilizedStringLName = Singleton.sharedInstance.lastNameStr.capitalized
        print(captilizedStringLName)
        self.lblNameVC.text = captilizedStringFName + " " + captilizedStringLName
        print(self.lblNameVC.text!)
      // self.lblLastNameVC.text = captilizedStringLName
      //  print(self.lblLastNameVC.text!)
       self.lblFollowersVC.text = String(Singleton.sharedInstance.noOfFollwers)
       self.lblFollowingVC.text = String(Singleton.sharedInstance.noOfFollowing)
       self.lblSupportersVC.text = String(Singleton.sharedInstance.noOfSupporters)
       print( self.lblPartyName.text!)
       profileImageVC.sd_setImage(with: URL(string: Singleton.sharedInstance.userImageStr), placeholderImage: UIImage(named: "placeholder.png"))
    }
    
    // Alert view Method
    func alertView(titleStr:String,messageStr:String){
        
        
        let alertView = UIAlertController(title: titleStr, message:messageStr , preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
            
        })
        alertView.addAction(action)
        self.present(alertView, animated: true, completion: nil)
 
    }
   


}


//MARK: SliderMenu Delegate Methods
extension MyProfileVC : SlideMenuControllerDelegate {
    
    func leftDidClose() {
        
        
        
    }
    
}








