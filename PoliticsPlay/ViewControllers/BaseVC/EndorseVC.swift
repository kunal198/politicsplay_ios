//
//  EndorseVC.swift
//  LayoutScreens
//
//  Created by Brst on 30/10/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class EndorseVC: UIViewController {
    @IBOutlet weak var serByNameTxt: UITextField!
    @IBOutlet weak var serByOfficeTxt: UITextField!
    @IBOutlet weak var endorseBtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        contentsBorder()
    }
    func contentsBorder()
    {      
        endorseBtn.layer.cornerRadius = 22
        
        setFieldBorder(textField: serByNameTxt)
        setFieldBorder(textField: serByOfficeTxt)
    }
    func setFieldBorder(textField: UITextField)
    {
        textField.layer.cornerRadius = 20
        textField.layer.borderColor = UIColor .lightGray .cgColor
        textField.layer.borderWidth = 1.0
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
