//
//  LeftMenuVC.swift
//  Unlock your 10
//
//  Created by brst on 10/9/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit

enum LeftMenu: Int {
    
    case HomeVC = 0
    case InboxVC
    case CreateMessageVC
    case MyPortfolioVC
    case KeysVC
    case Memorandums = 5
    case Elevator = 6
    case Complaints = 7
    case SubmitTraining = 8
    case LunchBreak = 9
    case AdvancedNotice = 10
    case MentorRegisteration = 11
    case ContactSupport = 12
    case logout = 13
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}


class LeftMenuVC: BaseViewController,LeftMenuProtocol {

//OUTLETS
    
    
    
//VARIABLES

    var menus = ["Home/Contacts","Inbox","Create a new message","My Portfolio","Keys","Memorandums","Elevator","Complaints","Training","Lunch Break","Advanced Notice","Register as Mentor","Contact Support","Logout"]
    var homeVC: UIViewController!
    var inboxVC: UIViewController!
    var createMessageVC : UIViewController!
    var myPortfolioVC :UIViewController!
    var keysVC : UIViewController!
    var contactSupportVC: UIViewController!
    var advancedNoticeVC: UIViewController!
    var mentorRegisteration: UIViewController!
    var lunchBreak: UIViewController!
    var visibleVC: UIViewController!
    var memorandums: UIViewController!
    var complaints: UIViewController!
    var submitTraining: UIViewController!
    var elevator: UIViewController!

 
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
        // Do any additional setup after loading the view.
    }

    // MARK: CustomizeUI
    func customizeUI()  {
        let storyboard = UIStoryboard(storyboard: .Main)
        let menuStoryboard = UIStoryboard(storyboard: .Menu)
        homeVC = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        inboxVC = storyboard.instantiateViewController(withIdentifier: "InboxVC") as! InboxVC
        createMessageVC = menuStoryboard.instantiateViewController(withIdentifier: "CreateMessageVC") as! CreateMessageVC
        myPortfolioVC = menuStoryboard.instantiateViewController(withIdentifier: "MyPortfolioVC") as! MyPortfolioVC
        keysVC = menuStoryboard.instantiateViewController(withIdentifier: "KeysVC") as! KeysVC
        memorandums = menuStoryboard.instantiateViewController(withIdentifier: "Memorandums") as! Memorandums
        advancedNoticeVC = menuStoryboard.instantiateViewController(withIdentifier: "AdvancedNotice") as! AdvancedNotice
        mentorRegisteration = menuStoryboard.instantiateViewController(withIdentifier: "MentorRegistration") as! MentorRegistration
        submitTraining = menuStoryboard.instantiateViewController(withIdentifier: "SubmitTrainingIdea") as! SubmitTrainingIdea
        lunchBreak = menuStoryboard.instantiateViewController(withIdentifier: "LunchBreak") as! LunchBreak
        contactSupportVC = menuStoryboard.instantiateViewController(withIdentifier: "ContactSupport") as! ContactSupport
        complaints = menuStoryboard.instantiateViewController(withIdentifier: "ComplaintsVC") as! ComplaintsVC
        elevator = menuStoryboard.instantiateViewController(withIdentifier: "ElevatorVC") as! ElevatorVC
        visibleVC = UIApplication.topViewController()
        
    }
 //MARK: Button Action
    @IBAction func btnCrossAction(_ sender: Any) {
        self.slideMenuController()?.closeLeft()
    }
    
    func changeViewController(_ menu: LeftMenu) {
        switch menu {
        case .HomeVC:
            self.slideMenuController()?.changeMainViewController(self.homeVC, close: true)
            //self.slideMenuController()?.closeLeft()
        case .InboxVC:
            self.slideMenuController()?.changeMainViewController(self.inboxVC, close: true)
        case .CreateMessageVC:
            self.slideMenuController()?.changeMainViewController(self.createMessageVC, close: true)
        case .MyPortfolioVC:
            self.slideMenuController()?.changeMainViewController(self.myPortfolioVC, close: true)
        case .KeysVC:
            self.slideMenuController()?.changeMainViewController(self.keysVC, close: true)
        case .Memorandums:
            self.slideMenuController()?.changeMainViewController(self.memorandums, close: true)
       
        case .Elevator:
            self.slideMenuController()?.changeMainViewController(self.elevator, close: true)

        case .Complaints:
            self.slideMenuController()?.changeMainViewController(self.complaints, close: true)
      
        case .SubmitTraining:
            self.slideMenuController()?.changeMainViewController(self.submitTraining, close: true)

        case .LunchBreak:
            self.slideMenuController()?.changeMainViewController(self.lunchBreak, close: true)
        case .AdvancedNotice:
            self.slideMenuController()?.changeMainViewController(self.advancedNoticeVC, close: true)
        case .MentorRegisteration:
            self.slideMenuController()?.changeMainViewController(self.mentorRegisteration, close: true)
        case .ContactSupport:
            self.slideMenuController()?.changeMainViewController(self.contactSupportVC, close: true)
        case .logout:
            
            UserVM.sharedInstance.isLogout = true
            self.slideMenuController()?.closeLeft()
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension LeftMenuVC : UITableViewDelegate {
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 60
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = LeftMenu(rawValue: indexPath.row) {
            self.changeViewController(menu)
        }
        
    }
    
    //    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    //        if self.tableView == scrollView {
    //
    //        }
    //    }
}

extension LeftMenuVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kCell, for: indexPath)
        let lblMenu = cell.viewWithTag(10) as! UILabel
        lblMenu.text = menus[indexPath.row]
        
        return cell
    }
}

