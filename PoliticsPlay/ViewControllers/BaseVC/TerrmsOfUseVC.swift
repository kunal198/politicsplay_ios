//
//  TerrmsOfUseVC.swift
//  PoliticsPlay
//
//  Created by Brst981 on 02/11/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit

class TerrmsOfUseVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func toggleLeft(_ sender: Any) {
        
        slideMenuController()?.toggleLeft()
        
    }

}
//MARK: SliderMenu Delegate Methods
extension TerrmsOfUseVC : SlideMenuControllerDelegate {
    
    func leftDidClose() {
        
        
        
    }
    
}
