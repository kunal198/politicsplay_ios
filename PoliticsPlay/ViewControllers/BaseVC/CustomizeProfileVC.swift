//
//  CustomizeProfileVC.swift
//  PoliticsPlay
//
//  Created by Brst981 on 31/10/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage
import AVFoundation
import Photos


protocol CustomizeProfileVCDelegate:class{
    
    func getProfileDataMethd(userId: String,authToken: String)
    
}


class CustomizeProfileVC: UIViewController,UITextFieldDelegate {
    
    //Outlets
    
    @IBOutlet var btnDone: UIButton!
    @IBOutlet var txtFieldCaption: UITextField!
    @IBOutlet var viewCaption: UIView!
    @IBOutlet var imgView: UIImageView!
    @IBOutlet var btnEditPicture: UIButton!
    @IBOutlet var lblFollowers: UILabel!
    @IBOutlet var lblSupporters: UILabel!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var btnPermYes: UIButton!
    @IBOutlet var btnPermsNo: UIButton!
    @IBOutlet var tableViewList: UITableView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    
    // variables
    var arrSelectionParty = [String]()
    var selected = "0"
    let imagePicker = UIImagePickerController()
    var tempIndex = Int()
    weak var delegate: CustomizeProfileVCDelegate?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        tempIndex = -1
        customizeView()

        // Do any additional setup after loading the view.
        
        
        //arr initialization
        arrSelectionParty = ["Republican","Democrat","Libertarian","Independent","Green Party","Other"]
        
        // code for setting delegate of image picker
        self.imagePicker.delegate = self

    
        txtFieldCaption.delegate = self as? UITextFieldDelegate
        
        
       
        // code for showing no of followers, no of lblSupporters
        self.lblSupporters.text = String(Singleton.sharedInstance.noOfSupporters)
        self.lblFollowers.text = String(Singleton.sharedInstance.noOfFollwers)
      //  self.txtFieldCaption.text = Singleton.sharedInstance.statusCaption
        imgView.sd_setImage(with: URL(string: Singleton.sharedInstance.userImageStr), placeholderImage: UIImage(named: "placeholder.png"))
      self.activityIndicator.isHidden = true
        
        if Singleton.sharedInstance.chooseParty == ""
        {
            
        }
        else{
            
            let indexofparty = self.arrSelectionParty.index(of: Singleton.sharedInstance.chooseParty)!
            print(indexofparty)
            self.tempIndex = indexofparty
            print(self.tempIndex)
        }
        
      
        
        if let value =  UserDefaults.standard.value(forKey:"selectedKey") as? NSString {
            
            if value == "0"{
                btnPermsNo.setImage(UIImage(named: "selected"), for: .normal)
                btnPermYes.setImage(UIImage(named: "radio"), for: .normal)
            }
                
                
            else{
                btnPermYes.setImage(UIImage(named: "selected"), for: .normal)
                btnPermsNo.setImage(UIImage(named: "radio"), for: .normal)
                }
            }
        else{
               btnPermsNo.setImage(UIImage(named: "radio"), for: .normal)
               btnPermYes.setImage(UIImage(named: "radio"), for: .normal)}

        // making the img view round
        imgView.clipsToBounds = true
        imgView.contentMode = .scaleAspectFill
        imgView.layer.cornerRadius = imgView.frame.size.height/2
        
 }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    override func viewWillLayoutSubviews() {
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height+100)
    }
    
    
    
    @IBAction func popBack(_ sender: Any) {
    
        if UserDefaults.standard.value(forKey: "userId") != nil{
            
            let userIdValue = UserDefaults.standard.value(forKey: "userId") as! String
            let authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
            delegate?.getProfileDataMethd(userId: userIdValue , authToken: authTokenValue )
            
        }
   // self.navigationController?.popViewController(animated: true)
       self.navigationController?.popToRootViewController(animated: true)
    
    }
    
    
    @IBAction func closeDropDown(_ sender: Any) {
   
        
        
        if (tableViewList.isHidden){
            
            tableViewList.isHidden = false
            
        }
    
        else{
            
            tableViewList.isHidden = true
          
        }
    
    
    
    
    }
    //Customize Views
    func customizeView(){
        
    btnDone.layer.cornerRadius = btnDone.frame.height/2
    btnDone.clipsToBounds = true
    viewCaption.layer.shadowColor = UIColor.black.cgColor
    viewCaption.layer.shadowOpacity = 0.25
    viewCaption.layer.shadowRadius = 1.0
    viewCaption?.layer.shadowOffset = CGSize(width: 1, height: 1.0)
    viewCaption?.layer.cornerRadius = 2.0
    
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        
        
        txtFieldCaption.resignFirstResponder()
        return true;
        
        
    }
    
    // MARK: Text Field Delegate
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool { // return NO to not change text
        
        
        
        if textField == txtFieldCaption {
            
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            if (newLength > 50) {
                
                let alertView = UIAlertController(title: "", message:"You cannot enter more than 50 characters" , preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                    
                })
                alertView.addAction(action)
                self.present(alertView, animated: true, completion: nil)
      }
            
}
      return true
    
    }
    
    
    //MARK: CheckCameraAuthorization
    
    func checkCameraAuthorization() {
        
        let status: AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        if status == .authorized {
            // authorized            picker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: { _ in })
        } else if status == .denied {
            // denied
            if AVCaptureDevice.responds(to: #selector(AVCaptureDevice.requestAccess(forMediaType:completionHandler:))) {
                AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {(_ granted: Bool) -> Void in
                    // Will get here on both iOS 7 & 8 even though camera permissions weren't required
                    // until iOS 8. So for iOS 7 permission will always be granted.
                    print("DENIED")
                    if granted {
                        // Permission has been granted. Use dispatch_async for any UI updating
                        // code because this block may be executed in a thread.
                        
                        DispatchQueue.main.async(execute: {() -> Void in
                            let picker = UIImagePickerController()
                            picker.delegate = self
                            picker.allowsEditing = true
                            picker.sourceType = .camera
                            self.present(picker, animated: true, completion: { _ in })
                        })
                        
                    }
                    else {
                        // Permission has been denied.
                        
                        
                        DispatchQueue.main.async(execute: {() -> Void in
                            self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
                        })
                        
                        
                    }
                })
            }
        } else if status == .restricted {
            // restricted
            DispatchQueue.main.async(execute: {() -> Void in
                self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
            })
        }
        else if status == .notDetermined {
            // not determined
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {(_ granted: Bool) -> Void in
                if granted {
                    // Access has been granted ..do something
                    DispatchQueue.main.async(execute: {() -> Void in
                        let picker = UIImagePickerController()
                        picker.delegate = self
                        picker.allowsEditing = true
                        picker.sourceType = .camera
                        self.present(picker, animated: true, completion: { _ in })
                        
                    })
                    
                    
                }
                else {
                    // Access denied ..do something
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
                    })
                    
                }
            })
        }
    }
    
    
    
    func accessMethod(_ message: String) {
        let alertController = UIAlertController(title: "Not Authorized", message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "OK", style: .default, handler: nil)
        let Settings = UIAlertAction(title: "Settings", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        })
        alertController.addAction(Settings)
        alertController.addAction(cancel)
        present(alertController, animated: true, completion: { _ in })
    }
    //MARK: CheckPhotoAuthorization
    
    func checkPhotoAuthorization()  {
        let status: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        if status == .authorized {
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = .photoLibrary
            present(imagePicker, animated: true, completion: { _ in })
        }
        else if status == .denied {
            DispatchQueue.main.async(execute: {() -> Void in
                self.accessMethod("Please go to Settings and enable the photos for this app to use this feature.")
            })
        } else if status == .notDetermined {
            // Access has not been determined.
            PHPhotoLibrary.requestAuthorization({(_ status: PHAuthorizationStatus) -> Void in
                if status == .authorized {
                    
                    DispatchQueue.main.async(execute: {() -> Void in
                       
                        self.imagePicker.delegate = self
                        self.imagePicker.allowsEditing = true
                        self.imagePicker.sourceType = .photoLibrary
                        self.present(self.imagePicker, animated: true, completion: { _ in })
                    })
                    
                }
                else {
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.accessMethod("Please go to Settings and enable the photos for this app to use this feature.")
                    })
                }
            })
        }
        else if status == .restricted {
            // Restricted access - normally won't happen.
        }
        
        
    }
    
    func updateProfileMethd(userId:String,authToken:String){
        
        
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        
        
        let parameters = ["user_id":userId,
                    "auth_token":authToken,
                    "party":Singleton.sharedInstance.chooseParty,
                    "permission":selected,
                    "status_caption":txtFieldCaption.text!,

            ] as [String : Any]
        
        print(parameters)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(UIImagePNGRepresentation(self.imgView.image!)!, withName: "profile_pic", fileName: "profilepic.jpg", mimeType: "image/jpg")
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:"http://beta.brstdev.com/politicsplay/api/web/v1/users/update_profile")
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                    //  print(progress)
                    
                })
                
                upload.responseJSON { response in
                    //print response.result
                    print(response)
                    DispatchQueue.main.async {
                        if response.result.value != nil {
                            let json = JSON(data: response.data! )
                            if let descriptionStr = json["message"].string {
                                print(descriptionStr)
                                Singleton.sharedInstance.chooseParty =  json["data"]["party"].string!
                             //   Singleton.sharedInstance.statusCaption =  json["data"]["status_caption"].string!
                                Singleton.sharedInstance.userImageStr =  json["data"]["profile_pic"].string!
                               let alertView = UIAlertController(title: "", message:descriptionStr , preferredStyle: .alert)
                                let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                                    
//                                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//
//                                let signViewController = storyBoard.instantiateViewController(withIdentifier:
//                                    "TabBarViewController") as! TabBarViewController
//                                signViewController.selectedIndex1 = 4
//                                    self.navigationController?.pushViewController(signViewController, animated: true)
                                    
                                  //  self.navigationController?.popViewController(animated: true)
                                    
                                })
                                alertView.addAction(action)
                                self.present(alertView, animated: true, completion: nil)
                                
                            if((response.error) != nil){
                        
//                              let alertView = UIAlertController(title: titleStr, message:(response.error?.localizedDescription)!) , preferredStyle: .
//                                alert)
//                                let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
//                                    
//                                })
//                                alertView.addAction(action)
//                                self.present(alertView, animated: true, completion: nil)
                            
                                }
                                DispatchQueue.main.async {
                                    
                                    self.tableViewList.reloadData()
                                    self.activityIndicator.isHidden = true
                                    self.activityIndicator.stopAnimating()
                                    
                               }
                                
                                
                                
                            }
                  } // dispatch end
                
                    }
                    
                }
                
            case .failure(let encodingError):
                
            //    self.alertViewMethod(titleStr: "", messageStr: encodingError.localizedDescription)
                break
                //print encodingError.description
                
            }
        }
   
        
   }
    

    // done btn action
    
    @IBAction func doneBtn(_ sender: Any) {
    
    
        if UserDefaults.standard.value(forKey: "userId") != nil{
            
            let userIdValue = UserDefaults.standard.value(forKey: "userId")
            let authTokenValue = UserDefaults.standard.value(forKey: "authToken")
           // print(userIdValue)
            //print(authTokenValue)
            updateProfileMethd(userId: userIdValue as! String, authToken: authTokenValue as! String )
            
        }
        
        
       

    }
    
    
    @IBAction func permsYes(_ sender: Any) {
    
       selected = "1"
      if selected == "1"{
        
       // Singleton.sharedInstance.selected = selected
        btnPermYes.setImage(UIImage(named: "selected"), for: .normal)
        btnPermsNo.setImage(UIImage(named: "radio"), for: .normal)
        UserDefaults.standard.set(selected, forKey:"selectedKey")
        UserDefaults.standard.synchronize()
   
        }

    }
    
    
    
    @IBAction func permsNo(_ sender: Any) {
    
    
        selected = "0"
        if selected == "0"{
            
            // Singleton.sharedInstance.selected = selected
            btnPermsNo.setImage(UIImage(named: "selected"), for: .normal)
            btnPermYes.setImage(UIImage(named: "radio"), for: .normal)
            UserDefaults.standard.set(selected, forKey:"selectedKey")
            UserDefaults.standard.synchronize()
        }
    }
    
    
    // code for picking image from image gallery
    @IBAction func pickingImageFromGallery(_ sender: Any) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let camera = UIAlertAction(title: "Camera", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.checkCameraAuthorization()
        })
        let gallery = UIAlertAction(title: "Gallery", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.checkPhotoAuthorization()
        })
        alertController.addAction(gallery)
        alertController.addAction(camera)
        alertController.addAction(cancel)
        present(alertController, animated: true, completion: { _ in })
       
    }
    
    //MARK: Resize Image
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0,y: 0,width: newWidth,height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    
}


extension CustomizeProfileVC:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrSelectionParty.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellSelectionParty", for: indexPath ) as!
        RunForOffcCell
        cell.lblDescription.text = arrSelectionParty[indexPath.row]
        if indexPath.row == tempIndex{
            cell.btnClickable.setImage(UIImage(named:"selected"), for:UIControlState.normal)

        }else{
           
            cell.btnClickable.setImage(UIImage(named:"radio"), for:UIControlState.normal)
 
        }

    return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tempIndex = indexPath.row
        
        Singleton.sharedInstance.chooseParty  = arrSelectionParty[indexPath.row]
        print(Singleton.sharedInstance.chooseParty)
        tableViewList.reloadData()
    
    
    }
    
    
}


extension CustomizeProfileVC:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    
func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if (info[UIImagePickerControllerEditedImage] != nil)
        {
            var chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage
            chosenImage = resizeImage(image: chosenImage, newWidth: 400)
            imgView.clipsToBounds = true
            imgView.contentMode = .scaleAspectFill
            imgView.layer.cornerRadius = imgView.frame.size.height/2
            imgView.image = chosenImage

        }
        dismiss(animated: true, completion: nil)
        
  }
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    



}









