//
//  ContactUsVC.swift
//  LayoutScreens
//
//  Created by Brst on 30/10/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class ContactUsVC: UIViewController {
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var phoneTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var contactUsBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        

        contentsBorder()
    }
    func contentsBorder()
    {
        contactUsBtn.layer.cornerRadius = 22
        
        setFieldBorder(textField: nameTxt)
        setFieldBorder(textField: phoneTxt)
        setFieldBorder(textField: emailTxt)
    }
    func setFieldBorder(textField: UITextField)
    {
        textField.layer.cornerRadius = 5
        textField.layer.masksToBounds = false
        textField.layer.shadowRadius = 1.0
        textField.layer.shadowColor = UIColor.black.cgColor
        textField.layer.shadowOffset = CGSize(width: 1.0,height: 1.0)
            
            
        textField.layer.shadowOpacity = 0.4
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
