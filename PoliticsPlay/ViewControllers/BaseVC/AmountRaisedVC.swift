//
//  AmountRaisedVC.swift
//  LayoutScreens
//
//  Created by Brst on 31/10/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class AmountRaisedVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var table_View: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath ) as! AmountRaisedTableViewCell
        table_View.separatorStyle = .none
        cell.selectionStyle = .none
        if indexPath.row % 2 == 0 {
            cell.cellContentView.backgroundColor = UIColor (colorLiteralRed: 231/255, green: 235/255, blue: 240/255, alpha: 1.0)
        }
        else {
            cell.cellContentView.backgroundColor = UIColor.white
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
