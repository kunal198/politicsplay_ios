//
//  LoginVC.swift
//  PoliticsPlay
//
//  Created by Brst981 on 01/11/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit
//import IQKeyboardManagerSwift
import Alamofire
import SwiftyJSON
import FBSDKLoginKit
import SystemConfiguration


class LoginVC: UIViewController,UITextFieldDelegate {
    
    
    //outlets
    @IBOutlet var btnLinkedIn: UIButton!
    @IBOutlet var btnFacebook: UIButton!
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var btnSignUp: UIButton!
    @IBOutlet var forgotPassword: UIButton!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var imgViewEmail: UIImageView!
    @IBOutlet var imgViewPassword: UIImageView!
    @IBOutlet var txtFieldEmail: UITextField!
    @IBOutlet var txtFieldPassword: UITextField!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    //Variables
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var firstNameSocialStr = String()
    var lastNameSocialStr  = String()
    var profilePicSocialStr = String()
    var socialIdStr = String()
    var emailSocialStr = String()
    let userDefaults = UserDefaults.standard
    var savedEmail = String()
    
    
//    private let linkedinHelper = LinkedinSwiftHelper(configuration: LinkedinSwiftConfiguration(clientId: "77jifcuypfio93", clientSecret: "jJZF55NZGbhcToYy",state: "DLKDJF46ikMMZADfdfds",permissions: ["r_basicprofile", "r_emailaddress"], redirectUrl: "<https://www.brihaspatitech.com>"))
    
   
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        customizeViews()
        customizeTF()
        
        txtFieldPassword.delegate = self as? UITextFieldDelegate
        txtFieldEmail.delegate = self as? UITextFieldDelegate
        
        //tap gesture for dismissing the keyboard
       let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginVC.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        
        self.navigationController?.isNavigationBarHidden = true
        
        self.activityIndicator.isHidden = true

     //   generateToken()
        
        
        
        // internet connection check
        if Reachability.isConnectedToNetwork() {
            
            print("Internet connection available")
        }
        else{
            
            alertViewMethod(titleStr: "", messageStr: "No Internet connection available")
        
        }
        
    
     // code  for fetching saved email id from user defaults
        
        
//        if UserDefaults.standard.object(forKey: "savedEmail" ) != ""{
//        
//             savedEmail = UserDefaults.standard.value(forKey: "savedEmail") as! String
//                if savedEmail != ""{
//                    
//                    print(savedEmail)
//                    self.txtFieldEmail.text = savedEmail
//                    print(self.txtFieldEmail.text)
//                }
//                else{
//                    
//                    txtFieldEmail.text = ""
//                }
//
//            
//       
//        }
        
        
        let value = Singleton.sharedInstance.savedEmail
        print(value)
        
        if Singleton.sharedInstance.savedEmail != ""{
            
         self.txtFieldEmail.text = Singleton.sharedInstance.savedEmail
            
        }
        else{
            
           self.txtFieldEmail.text = ""
        }
        
        
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
//            txtFieldEmail.text = ""
//            txtFieldPassword.text = ""
        
        
        print()
        if Singleton.sharedInstance.savedEmail != ""{
            
            self.txtFieldEmail.text = Singleton.sharedInstance.savedEmail
            
        }
        else{
            
            self.txtFieldEmail.text = ""
        }

    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func customizeTF(){
        
        
        let fullString = NSMutableAttributedString()
        let fullStringAgain = NSMutableAttributedString()

        
        let image1Attachment = NSTextAttachment()
        let image2Attachment = NSTextAttachment()
        
        
        
        let image = UIImage(named: "message")
        let image1 = UIImage(named: "padlock")

        image1Attachment.image = image
        image2Attachment.image = image1

        
        let font = txtFieldEmail.font!
        
        let mid = font.descender + font.capHeight
        image1Attachment.bounds = CGRect(x: 0, y: font.descender - image!.size.height / 2 + mid + 2, width: image!.size.width, height:image!.size.height).integral
        image2Attachment.bounds = CGRect(x: 0, y: font.descender - image!.size.height / 2 + mid + 2, width: image!.size.width, height:image!.size.height).integral

        
        let image1String = NSAttributedString(attachment: image1Attachment)
        let image2String = NSAttributedString(attachment: image2Attachment)

        
        fullString.append(image1String)
        fullStringAgain.append(image2String)
        fullString.append(NSAttributedString(string: "  Email*"))
        fullStringAgain.append(NSAttributedString(string: "  Password*"))

        
        txtFieldEmail.attributedPlaceholder = fullString
        txtFieldPassword.attributedPlaceholder = fullStringAgain

   
    }
    
    //tap gesture
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    
    func customizeViews(){
        
       
      btnLogin.layer.cornerRadius = btnLogin.frame.height/2
      btnLogin.clipsToBounds = true
        
      btnFacebook.layer.cornerRadius = btnLogin.frame.height/2
      btnFacebook.clipsToBounds = true
        
      btnLinkedIn.layer.cornerRadius = btnLogin.frame.height/2
      btnLinkedIn.clipsToBounds = true
        
        
        
    }
    
    
    override func viewWillLayoutSubviews() {
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height+100)
    }
    
    
    @IBAction func pusHProfileScreen(_ sender: Any) {
    

        
        if (txtFieldEmail.text?.isEmpty)! {
            print("enter email address") //prompt ALert or toast
            alertViewMethod(titleStr: "", messageStr: "Please enter the email.")
        } else if !isValidEmail(testStr: txtFieldEmail.text!) {
            alertViewMethod(titleStr: "", messageStr: "Please enter the valid email.")
        } else if (txtFieldPassword.text?.isEmpty)! {
            alertViewMethod(titleStr: "", messageStr: "Please enter the password.")
        }
        else{
        
         loginMethod()
            
        }
        
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        
        txtFieldEmail.resignFirstResponder()
        txtFieldPassword.resignFirstResponder()
        return true;
   
    }
    
  
    //push to sign up screen
    @IBAction func pushToSignUpScreen(_ sender: Any) {
    
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)

        let signViewController = storyBoard.instantiateViewController(withIdentifier:
                  "SignUpVC") as! SignUpViewController
        self.navigationController?.pushViewController(signViewController, animated: true)

    }
    
    // push to Forgot Pass vc
    
    @IBAction func pushToForgotVC(_ sender: Any) {
    
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let forgotViewController = storyBoard.instantiateViewController(withIdentifier:
            "ForgotPassVC") as! ForgotPassVC
        self.navigationController?.pushViewController(forgotViewController, animated: true)
    }
    
    // Login Api
    func loginMethod () {
        
       // let value = UserDefaults.standard.value(forKey: "deviceTokenStr")
       // print(value)
        //print(Singleton.sharedInstance.deviceTokenStr)
   
        
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        
        let json = ["email":txtFieldEmail.text!,
                    "password":txtFieldPassword.text!,
                    "device_token":"1234"
            ] as [String : Any]
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/politicsplay/api/web/v1/users/login", method: .post, parameters:                                           json, encoding: URLEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if((response.error) != nil){
                    
                    self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                    
                }
                let json = JSON(data: response.data! )
                DispatchQueue.main.async {
                    
                    if let descriptionStr = json["message"].string {
                        print(descriptionStr)
                        if  descriptionStr == "Login Successfully"{
                            
                            let userId = String(describing: json["data"]["user_id"].int!)
                            
                            Singleton.sharedInstance.userIdStr = userId
                          //  print(UserDefaults.standard.value(forKey:"userId"))
                        
                            // code for saving value in ns user default
                            UserDefaults.standard.set(userId, forKey:"userId")
                            UserDefaults.standard.synchronize()
                         // print(UserDefaults.standard.value(forKey:"userId"))
                           
                            let auth_token = json["data"]["auth_token"].string
                            Singleton.sharedInstance.auth_token = auth_token!
                
                           // code for saving value in ns user default
                            UserDefaults.standard.set(auth_token, forKey:"authToken")
                            UserDefaults.standard.synchronize()
                         //   print(UserDefaults.standard.value(forKey:"authToken"))

                            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                            
                            let zipController = storyBoard.instantiateViewController(withIdentifier:
                                "ZipCodeVC") as! ZipCodeVC
                            self.navigationController?.pushViewController(zipController, animated: true)
                        }
                            
                        else if descriptionStr == "Incorrect password"{
                            
                            self.alertViewMethod(titleStr: "", messageStr: "Incorrect password")
                        }
                        else if descriptionStr == "Email not found."{
                            
                            self.alertViewMethod(titleStr: "", messageStr: "Email not found.")
                        }
                       
                        else if((response.error) != nil){
                            
                            self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                            
                        }
                        
                        DispatchQueue.main.async {
                            
                            
                            self.activityIndicator.isHidden = true
                            self.activityIndicator.stopAnimating()
                        
                       }
                        
            }
        }
     
    }

   }
    
    
    
    // Social Login Method
    func socialLoginMethd(){
        
        
        DispatchQueue.main.async {
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
            self.txtFieldEmail.isUserInteractionEnabled = false
            self.txtFieldPassword.isUserInteractionEnabled = false
        }
       
        
        let json = ["social_id":socialIdStr,
                    "social_platform":Singleton.sharedInstance.socialLoginType,
                    "device_token":"1234"
            ] as [String : Any]
        print(json)
        Alamofire.request("http://beta.brstdev.com/politicsplay/api/web/v1/users/social_login", method: .post, parameters: json, encoding: URLEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                DispatchQueue.main.async {
                    
                    
                    if let jsonData = response.result.value {
                        print("JSON: \(jsonData)")
                        let json = JSON(data: response.data!)
                        let descriptionStr = json["message"].string
                        print(descriptionStr!)
                        let statusCode = json["message"].string
                        if statusCode == "Register Successfully"{
                         
                            
                            let userId = String(describing: json["data"]["user_id"].int!)
                            Singleton.sharedInstance.userIdStr = userId
                            // code for saving value in ns user default
                            UserDefaults.standard.set(userId, forKey:"userId")
                            UserDefaults.standard.synchronize()
                            print(UserDefaults.standard.value(forKey:"userId"))
                            let authToken = String(describing: json["data"]["auth_token"].string!)
                            Singleton.sharedInstance.auth_token = authToken
                            // code for saving value in ns user default
                            UserDefaults.standard.set(authToken, forKey:"authToken")
                            UserDefaults.standard.synchronize()
                            print(UserDefaults.standard.value(forKey:"authToken"))
                         let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                         let signViewController = storyBoard.instantiateViewController(withIdentifier:
                                "CompleteProfileVC") as! CompleteProfileVC
                        self.navigationController?.pushViewController(signViewController, animated: true)
                        Singleton.sharedInstance.firstNameStr = self.firstNameSocialStr
                        Singleton.sharedInstance.lastNameStr = self.lastNameSocialStr
                        Singleton.sharedInstance.userImageStr = self.profilePicSocialStr
                        Singleton.sharedInstance.emailStr = self.emailSocialStr

                            
                    }
                        if  descriptionStr == "Login Successfully"{
                            
                            let userId = String(describing: json["data"]["user_id"].int!)
                            Singleton.sharedInstance.userIdStr = userId
                            // code for saving value in ns user default
                            UserDefaults.standard.set(userId, forKey:"userId")
                            UserDefaults.standard.synchronize()
                            print(UserDefaults.standard.value(forKey:"userId")!)
                           let authToken = String(describing: json["data"]["auth_token"].string!)
                            Singleton.sharedInstance.auth_token = authToken
                            // code for saving value in ns user default
                            UserDefaults.standard.set(authToken, forKey:"authToken")
                            UserDefaults.standard.synchronize()
                            print(UserDefaults.standard.value(forKey:"authToken")!)

                            Singleton.sharedInstance.firstNameStr = self.firstNameSocialStr
                            print(Singleton.sharedInstance.firstNameStr)
                            Singleton.sharedInstance.lastNameStr = self.lastNameSocialStr
                            print(Singleton.sharedInstance.firstNameStr)

                            Singleton.sharedInstance.userImageStr = self.profilePicSocialStr
                            Singleton.sharedInstance.emailStr = self.emailSocialStr
                            self.appDelegate.createMenu()
                            
                        }
                        
                        else if((response.error) != nil){
                            
                            self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                            
                        }
                        DispatchQueue.main.async {
                            
                            
                            self.activityIndicator.isHidden = true
                            self.activityIndicator.stopAnimating()
                            self.txtFieldEmail.isUserInteractionEnabled = true
                            self.txtFieldPassword.isUserInteractionEnabled = true
                        }
                        
                    }
                    
            }
             
                
                
        }
        
    }
    
    // MARK: Email Validation
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    // MARK: AlertView
    
    func alertViewMethod(titleStr:String, messageStr:String) {
        
        let alertView = UIAlertController(title: titleStr, message:messageStr , preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
            
        })
        alertView.addAction(action)
        self.present(alertView, animated: true, completion: nil)

    }
    
    // internet connectivity
    func connectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
        
        
        
    }

    
    //Mark: Facebook Integration
    
    
    @IBAction func btnFacebook(_ sender: Any) {
        
    appDelegate.check = "Fb"
     Singleton.sharedInstance.socialLoginType = "fb"
        UserDefaults.standard.set(Singleton.sharedInstance.socialLoginType, forKey: "socialLoginType")
        
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email","public_profile","user_friends"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData()
                        //   fbLoginManager.logOut()
                    }
                    
                    print("token is \(FBSDKAccessToken.current().tokenString)")
                    print("token is \(FBSDKAccessToken.current().userID)")
                }
            }
        }
        
    }
    
    
    // facebook graph api
    
    func getFBUserData(){
        
        
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email,first_name,last_name"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    print(result!)
                    let fbDetails = result as! NSDictionary
                    print(fbDetails)
                    self.firstNameSocialStr = fbDetails.value(forKey: "first_name") as! String
                    print(self.firstNameSocialStr)
                    self.lastNameSocialStr  = fbDetails.value(forKey: "last_name")  as! String
                     print(self.lastNameSocialStr)
                    self.socialIdStr  = FBSDKAccessToken.current().userID!
                    print(self.socialIdStr)
                    UserDefaults.standard.set(self.socialIdStr, forKey:"socialId")
                    UserDefaults.standard.synchronize()

                    self.profilePicSocialStr = "http://graph.facebook.com/\(FBSDKAccessToken.current().userID!)/picture?type=large"
                    if fbDetails.value(forKey: "email") != nil{
                        self.emailSocialStr = fbDetails.value(forKey: "email") as! String
                    }
                    else{
                        self.emailSocialStr = ""
                        
                    }
                    
                    self.socialLoginMethd()
                    
                }
                    
                else{
                    
                    self.alertViewMethod(titleStr: "", messageStr: (error?.localizedDescription)!)
                    
                }
                
                
            })
        }
    }
    


//    // linked in
//    func generateToken(){
//        
//        
//        Singleton.sharedInstance.socialLoginType = "linkedin"
//        UserDefaults.standard.set(Singleton.sharedInstance.socialLoginType, forKey: "socialLoginType")
//        
//        
//        
//        linkedinHelper.authorizeSuccess({ (token) in
//            
//            print(token)
//            //This token is useful for fetching profile info from LinkedIn server
//        }, error: { (error) in
//            
//            print(error.localizedDescription)
//            //show respective error
//        }) {
//            //show sign in cancelled event
//        }
//        
//        linkedinHelper.requestURL("https://api.linkedin.com/v1/people/~?format=json",
//                                  requestType: LinkedinSwiftRequestGet,
//                                  success: { (response) -> Void in
//                                    
//                                    //Request success response
//                                    
//        }) { [unowned self] (error) -> Void in
//            
//            //Encounter error
//        }
//        
//    }
//    
    
    
//    @IBAction func linkedinbutton(_ sender: AnyObject)
//    {
//        
//        let permissions: [AnyObject] = [LISDK_EMAILADDRESS_PERMISSION as AnyObject]
//        
//        LISDKSessionManager.createSession(withAuth: permissions, state: nil, showGoToAppStoreDialog: true, successBlock: { (state: String!) -> Void in
//            print("Login \(state)")
//            let session: LISDKSession = LISDKSessionManager.sharedInstance().session
//            NSLog("Session  : %@", session.description)
//            LISDKAPIHelper.sharedInstance().getRequest("https://api.linkedin.com/v1/people/~:(id,industry,firstName,lastName,emailAddress,headline,summary,publicProfileUrl,specialties,positions:(id,title,summary,start-date,end-date,is-current,company:(id,name,type,size,industry,ticker)),pictureUrls::(original),location:(name))?format=json", success: {(response: LISDKAPIResponse!) -> Void in
//                let data: Data = response.data.data(using: String.Encoding.utf8)!
//                do {
//                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
//                    print(json)
//                    let firstname = json["firstName"] as? String
//                    print(firstname)
//                    
//                    
//                    let names = json["id"] as? String
//                    
//                    
//                    let emailid = json["emailAddress"] as? String
//                    
//                    
//                    
//                    
//                    
//                    let arr = ModelManager.getInstance().selectnamefield()
//                    var num=0
//                    for str in arr
//                    {
//                        
//                        if((str as AnyObject).isEqual(emailid))
//                        {
//                            let alert:UIAlertController=UIAlertController(title: "Warning", message: "Login not successfully done with this email id", preferredStyle: .alert)
//                            let okAction: UIAlertAction = UIAlertAction(title: "Ok", style: .default, handler:nil)
//                            alert.addAction(okAction)
//                            self.present(alert, animated: true, completion: nil)
//                            num=num+1
//                        }
//                        else
//                        {
//                            print("not same")
//                        }
//                        
//                    }
//                    if (num == 0)
//                    {
//                        
//                        let eventDescriptionVC = self.storyboard!.instantiateViewController(withIdentifier: "dashboard") as! DashboardViewController
//                        
//                        
//                        UserDefaults.standard.set("true", forKey: "isloggedin")
//                        
//                        UserDefaults.standard.set(firstname, forKey: "labelname")
//                        
//                        
//                        UserDefaults.standard.set(names, forKey: "username")
//                        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
//                        let documentsDirectory: AnyObject = paths[0] as AnyObject
//                        let dataPath = documentsDirectory.appendingPathComponent(names!)
//                        
//                        do {
//                            try FileManager.default.createDirectory(atPath: dataPath, withIntermediateDirectories: false, attributes: nil)
//                            let fileManager = FileManager.default
//                            print(dataPath)
//                            // let pth = (dataPath as NSString).stringByAppendingPathComponent("MyFolder25")
//                            // print(pth)
//                            // print(pth)
//                            if fileManager.fileExists(atPath: dataPath)
//                            {
//                                print(dataPath)
//                            }
//                        } catch let error as NSError
//                        {
//                            print(error.localizedDescription)
//                        }
//                        
//                        self.navigationController?.pushViewController(eventDescriptionVC, animated: true)
//                        
//                    }
//                    
//                    //  let arr = ModelManager.getInstance().selectnamefield()
//            } catch let error as NSError {
//                    print("Failed to load: \(error.localizedDescription)")
//                }                //NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingMutableContainers, error:nil)
//                //var authUsername: String = "\(names["firstName"]) \(names["lastName"])"
//                // print(authUsername)
//            }
//                , error: { (error: NSError!) -> Void in
//                    
//                    print("Login error: \(error)")
//            }
//            )
//        }, errorBlock: { (error: NSError!) -> Void in
//            
//            print("Login error: \(error)") } as! AuthErrorBlock)
//        
//        
//        }
//
    

    @IBAction func linkedinbutton(_ sender: AnyObject){
        
        appDelegate.check = "LinkedIn"
       let permissions = [LISDK_BASIC_PROFILE_PERMISSION,LISDK_EMAILADDRESS_PERMISSION,LISDK_RW_COMPANY_ADMIN_PERMISSION]
        print(permissions)
       LISDKSessionManager.createSession(withAuth: permissions,state: nil, showGoToAppStoreDialog:true, successBlock: { (_ returnState: String) in
       
        let session = LISDKSessionManager.sharedInstance().session
        print("Session  : \(String(describing: session?.description))")
       
        try? LISDKAPIHelper.sharedInstance().getRequest("https://api.linkedin.com/v1/people/~?format=json", success: {(_ response: LISDKAPIResponse) -> Void in

        let data: Data? = response.data.data(using: String.Encoding.utf8)
        let json = try JSONSerialization.jsonObject(with: data!, options: []) as! [String: AnyObject]
            print(json)
            
            
//        var dictResponse = try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [AnyHashable: Any]
//        var authUsername = "\(dictResponse.value(forKey:"firstName"))\(dictResponse?.value(forKey: "lastName"))"
//        print("Authenticated user name  : \(authUsername)")
//        lblAuthenticatedUser.text = authUsername
        
        } as! (LISDKAPIResponse?) -> Void,error: { (_ apiError: LISDKAPIError) in
        
        } as! (LISDKAPIError?) -> Void)
        
        
    } as? AuthSuccessBlock,
        
        errorBlock: { (_ error: NSError) in
            
            print(error)
            
        } as? AuthErrorBlock)
        
    }





}
