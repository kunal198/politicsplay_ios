//
//  ZipCodeVC.swift
//  PoliticsPlay
//
//  Created by Brst981 on 01/11/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class ZipCodeVC: UIViewController,UITextFieldDelegate {
    
    
    //Outlets
    
    @IBOutlet var txtFieldZipCode: UITextField!
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var viewZipCode: UIView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    //Variables
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var isReadonly : Bool = false
    

    override func viewDidLoad() {
        super.viewDidLoad()
        customizeView()
        txtFieldZipCode.delegate = self

        
        //tap gesture for dismissing the keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ZipCodeVC.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        
        // code to assign number pad to keyboard type
       txtFieldZipCode.keyboardType = UIKeyboardType.numberPad
        
         self.activityIndicator.isHidden = true
        
        
        
        // setting ns user default
        UserDefaults.standard.set(true, forKey: "firstTimeLaunch") //Bool
      
        
        
        
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //tap gesture
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    
    
    func customizeView(){
        
    btnSubmit.layer.cornerRadius = btnSubmit.frame.height/2
    btnSubmit.clipsToBounds = true
        
    txtFieldZipCode.layer.cornerRadius = txtFieldZipCode.frame.height/2
    txtFieldZipCode.clipsToBounds = true
        
        
     viewZipCode.layer.cornerRadius = viewZipCode.frame.height/2
     viewZipCode.clipsToBounds = true
        
        
   }
    
    
    @IBAction func pushToHomeScreen(_ sender: Any) {
    
      UserDefaults.standard.set(true, forKey:"isFirstTime")
        Singleton.sharedInstance.zipCodeValue = txtFieldZipCode.text! as NSString
        UserDefaults.standard.set(Singleton.sharedInstance.zipCodeValue , forKey: "zipCodeValue")
        zipCodeMeth()
  
    
    }
    @IBAction func popBack(_ sender: Any) {
    
      
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        
        txtFieldZipCode.resignFirstResponder()
        return true;
    }
    
    // api integration in zip code
    func zipCodeMeth(){
        
        
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()

        
        let json = ["zip_code":txtFieldZipCode.text!,
                    "user_id":Singleton.sharedInstance.userIdStr,
                    "auth_token":Singleton.sharedInstance.auth_token
    ] as [String : Any]
        print(json)
    
        
        Alamofire.request("http://beta.brstdev.com/politicsplay/api/web/v1/users/add_locations", method: .post, parameters:json, encoding: URLEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                DispatchQueue.main.async {
                 let json = JSON(data: response.data! )
                let descripStr = json["message"].string
               if descripStr == "Location Updated Successfully"{
                
                 self.appDelegate.createMenu()
                
               

            }
                
             if descripStr == "Location Added Successfully"{
                
                self.appDelegate.createMenu()
             
             }
                
                
            else if descripStr == "Zip code cannot be empty."{
                
                self.alertViewMethod(titleStr: "", messageStr:descripStr!)

            }
                
           else if((response.error) != nil){
                    
           self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                
          }
                    
                    DispatchQueue.main.async {
                        
                        self.activityIndicator.isHidden = true
                        self.activityIndicator.stopAnimating()
                        
                    }
                    
            }
                
    }
        
 }
    
// MARK: AlertView

    func alertViewMethod(titleStr:String, messageStr:String) {
        
        let alertView = UIAlertController(title: titleStr, message:messageStr , preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
            
        })
        alertView.addAction(action)
        self.present(alertView, animated: true, completion: nil)
    }
    
   //MARK: disabling copy paste
    open override func target(forAction action: Selector, withSender sender: Any?) -> Any? {
        guard isReadonly else {
            return super.target(forAction: action, withSender: sender)
        }
        
        if #available(iOS 10, *) {
            if action == #selector(UIResponderStandardEditActions.paste(_:)) {
                return nil
            }
        } else {
            if action == #selector(paste(_:)) {
                return nil
            }
        }
        
        return super.target(forAction: action, withSender: sender)
    }






}

