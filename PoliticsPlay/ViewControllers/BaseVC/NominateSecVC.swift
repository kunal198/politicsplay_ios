//
//  NominateSecVC.swift
//  LayoutScreens
//
//  Created by Brst on 30/10/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class NominateSecVC: UIViewController {
    @IBOutlet weak var statementTxtView: UITextView!
    @IBOutlet weak var nominateBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        contentsBorder() 
    }
    func contentsBorder()
    {
        statementTxtView.layer.cornerRadius = 10
        statementTxtView.layer.borderColor = UIColor .lightGray .cgColor
        statementTxtView.layer.borderWidth = 1.0
        
        nominateBtn.layer.cornerRadius = 22
        nominateBtn.layer.borderColor = UIColor .lightGray .cgColor
        nominateBtn.layer.borderWidth = 0.5
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
