//
//  ForgotPassVC.swift
//  PoliticsPlay
//
//  Created by Brst981 on 03/11/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class ForgotPassVC: UIViewController,UITextFieldDelegate {
    
    
    
    //Outlets
    
    @IBOutlet var btnResetPassword: UIButton!
    @IBOutlet var viewBorder: UIView!
    @IBOutlet var txtFieldEmail: UITextField!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    
    //variables
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()

        
       btnResetPassword.layer.cornerRadius = btnResetPassword.frame.height/2
        btnResetPassword.clipsToBounds = true

        
        txtFieldEmail.delegate = self as? UITextFieldDelegate
        
        
        //tap gesture for dismissing the keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginVC.dismissKeyboard))
        view.addGestureRecognizer(tap)

       self.activityIndicator.isHidden = true
        
   
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    @IBAction func popBack(_ sender: Any) {
    
    self.navigationController?.popViewController(animated: true)
    
    }
   
    
    
    
    
    //reset password action
    
    @IBAction func resetPassword(_ sender: Any) {
   
     
        
        if (txtFieldEmail.text?.isEmpty)! {
            alertViewMethod(titleStr: "", messageStr: "Please enter the email.")
        } else if !isValidEmail(testStr: txtFieldEmail.text!) {
            alertViewMethod(titleStr: "", messageStr: "Please enter the valid email.")
        } else {
//            indicator.startAnimating()
//            indicator.isHidden = false
            forgotPassMethd()
        }
        
    
    }
    
    
    // MARK: AlertView
    
    func alertViewMethod(titleStr:String, messageStr:String) {
        
        let alertView = UIAlertController(title: titleStr, message:messageStr , preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
            
//            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//            
//            let zipController = storyBoard.instantiateViewController(withIdentifier:
//                "LoginVC") as! LoginVC
//            self.navigationController?.pushViewController(zipController, animated: true)
        })
        alertView.addAction(action)
        self.present(alertView, animated: true, completion: nil)

    }
    
    // MARK: Email Validation
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    

    
    
    // text field delegate methd
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        
        txtFieldEmail.resignFirstResponder()
        return true;
        
    }
    
    //tap gesture
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    // forgo password api
    
    func forgotPassMethd(){
        
        
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        
        
        let json = ["email":txtFieldEmail.text!] as [String : Any]
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/politicsplay/api/web/v1/users/forget_password", method: .post, parameters:                                           json, encoding: URLEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                DispatchQueue.main.async {
                   
              
                    let json = JSON(data: response.data! )
                    let descriptionStr = json["message"].string
                if  descriptionStr == "Please check your email to get new password" {
                    
               let alertView = UIAlertController(title: "", message:descriptionStr , preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                      
                        
                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                        
                        let signViewController = storyBoard.instantiateViewController(withIdentifier:
                            "LoginVC") as! LoginVC
                        self.navigationController?.pushViewController(signViewController, animated: true)
                    })
                    alertView.addAction(action)
                    self.present(alertView, animated: true, completion: nil)
                    
                    }
                    
                if  descriptionStr == "Email not found" {
                        
                let alertView = UIAlertController(title: "", message:descriptionStr , preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                    
                })
                alertView.addAction(action)
                self.present(alertView, animated: true, completion: nil)
                
           }
                    
            else if ((response.error) != nil){
                    
                    let alertView = UIAlertController(title: "", message:(response.error?.localizedDescription)! , preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                        
                    })
                    alertView.addAction(action)
                    self.present(alertView, animated: true, completion: nil)
                    
                    }
                    
                    
                    
                    DispatchQueue.main.async {
                        
                        
                        self.activityIndicator.isHidden = true
                        self.activityIndicator.stopAnimating()
                        
                    }
            }
    
        }
        
   }
    
    }
//MARK: SliderMenu Delegate Methods
extension ForgotPassVC : SlideMenuControllerDelegate {
    
    func leftDidClose() {
        
        
        
    }
    
}
