//
//  RunForOfficeVC.swift
//  LayoutScreens
//
//  Created by Brst on 30/10/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class RunForOfficeVC: UIViewController {
    @IBOutlet weak var scroll_View: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var submitBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        

       contentsBorder()
    }
    func contentsBorder()
    {
        submitBtn.layer.cornerRadius = 22
    }
    override func viewWillLayoutSubviews() {
        scroll_View.contentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height+100)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
