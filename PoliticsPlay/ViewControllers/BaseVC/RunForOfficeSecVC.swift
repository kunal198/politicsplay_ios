//
//  RunForOfficeSecVC.swift
//  LayoutScreens
//
//  Created by Brst on 30/10/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class RunForOfficeSecVC: UIViewController {
    @IBOutlet weak var linkTxt: UITextField!
    @IBOutlet weak var takePhotoBtn: UIButton!
    @IBOutlet weak var submitBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

       contentsBorder()
    }
    func contentsBorder()
    {
        linkTxt.layer.cornerRadius = 20
        linkTxt.layer.borderColor = UIColor .lightGray .cgColor
        linkTxt.layer.borderWidth = 1.0
        
        takePhotoBtn.layer.cornerRadius = 22
        takePhotoBtn.layer.borderColor = UIColor .red .cgColor
        takePhotoBtn.layer.borderWidth = 1.0
        
        submitBtn.layer.cornerRadius = 22
   
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
