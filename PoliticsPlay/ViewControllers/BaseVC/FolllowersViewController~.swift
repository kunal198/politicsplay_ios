//
//  FolllowersViewController.swift
//  PoliticsPlay
//
//  Created by Brst981 on 31/10/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON




class FolllowersViewController: UIViewController,TTTAttributedLabelDelegate {
    
   
    // Variables
    var arrFollowersStatus = [String]()
    var userIdValueAgain = String()
    var authTokenValue = String()
    var dataMutableArray = NSMutableArray()
    var followerUserId = Int()
    var status = Int()
    var otherUserId = Int()
    var checkProfile = Bool()
    var dataOtherUserArray = NSMutableArray()
    var concatenateString = String()
    var  btnStatus = NSInteger()
    var btnStatusMyProfile = NSInteger()
    var otherFollowerId = NSInteger()
    //outlets
    @IBOutlet var tableView: UITableView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     if UserDefaults.standard.value(forKey: "userId") != nil{
    
      if  checkProfile == true{
    
            userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as! String
            authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
            print(userIdValueAgain)
            print(authTokenValue)
            self.followersList(userId: userIdValueAgain, authToken: authTokenValue)
        
      }
      else{
    
        userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as! String
        authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
        print(userIdValueAgain)
        print(authTokenValue)
        self.otherFollowersList(userId: userIdValueAgain, authToken: authTokenValue)
        
        }
    
    }
        

        
        
        
}
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func popBack(_ sender: Any) {
    
    self.navigationController?.popViewController(animated: true)
    }
   
    
    // code to follow/unfollow any person
    
//    @IBAction func btnFollowUnfollowCandidates(_ sender: UIButton){
//
//    }
    
    
    // code for follow/unfollow people
    func followUnfollowUser(userId: String, authToken: String, status: NSInteger, otherUsersId: Int){


        DispatchQueue.main.async {

                 self.activityIndicator.isHidden = false
                 self.activityIndicator.startAnimating()

        }  // end of dispatch

      let json = [
            "user_id":userId,
            "auth_token":authToken,
            "other_user_id":otherUsersId,
            "status":status
            ] as [String : Any]

        print(json)

        Alamofire.request("http://beta.brstdev.com/politicsplay/api/web/v1/users/follow_user", method: .post, parameters:                                           json, encoding: URLEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                DispatchQueue.main.async {
                    let json = JSON(data: response.data! )
                    let jsonDict = json.dictionaryObject
                    let descriptionStr = jsonDict!["message"] as? NSString
                    if descriptionStr == "No follower found to unfollow"{

                        self.alertView(title: "", message: descriptionStr! as String)
                    }
                    else if ((descriptionStr == "Unfollow Successfully") || (descriptionStr == "Follow Successfully"))
                    {
                        if UserDefaults.standard.value(forKey: "userId") != nil{
                            
                            
                            if  self.checkProfile == true{
                                
                                self.userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as! String
                                self.authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
                                print(self.userIdValueAgain)
                                print(self.authTokenValue)
                                self.followersList(userId: self.userIdValueAgain, authToken: self.authTokenValue)
                            }
                                
                            else{
                                
                                self.userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as! String
                                self.authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
                                print(self.userIdValueAgain)
                                print(self.authTokenValue)
                                self.otherFollowersList(userId: self.userIdValueAgain, authToken: self.authTokenValue)
                                
                            }
                            
                        }
                   
                    }
                 else if descriptionStr == "Already followed to this user"{
                        
                        self.alertView(title: "", message: descriptionStr! as String)
                    }
                    
                    else if((response.error) != nil){
                        
                        self.alertView(title: "", message: (response.error?.localizedDescription)!)
                        
                    }
                    
                    

                    DispatchQueue.main.async{

                                    self.activityIndicator.isHidden = true
                                    self.activityIndicator.stopAnimating()

                    }  // end of dispatch

                } // end of outer dispatch

        }  // end of alamofire

   }  // end of method




    // code for pushing to next view controller on click
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        
        
        print(label.tag)
        if checkProfile  == true{
            
            let dictUserProfile = dataMutableArray[label.tag] as! NSDictionary
            let followerID = dictUserProfile.value(forKey: "follower_user_id") as! Int
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let userProfile = storyBoard.instantiateViewController(withIdentifier: "UserProfileVC") as! UserProfileVC
            userProfile.valuePerm = true
            userProfile.otherUserId = followerID
            self.navigationController?.pushViewController(userProfile, animated: true)
        }
        
        
        
      else if (userIdValueAgain == String(describing:otherFollowerId)) {
            
          let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let myProfile = storyboard?.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
            myProfile.valueKey = true
         //   userProfile.otherUserId = followerID
            self.navigationController?.pushViewController(myProfile, animated: true)
        
        }
        
        else{
            
            let dictUserProfile = dataOtherUserArray[label.tag] as! NSDictionary
            let followerID = dictUserProfile.value(forKey: "user_id") as! Int
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let userProfile = storyBoard.instantiateViewController(withIdentifier: "UserProfileVC") as! UserProfileVC
            userProfile.valuePerm = true
            userProfile.otherUserId = followerID
            self.navigationController?.pushViewController(userProfile, animated: true)
        }
    
    }
   // fetching followers user list
    func followersList(userId: String,authToken: String){
        
        DispatchQueue.main.async{
        
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
        
        }
       let json = ["user_id":userId,
                    "auth_token":authToken,
            ] as [String : Any]
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/politicsplay/api/web/v1/users/view_follower_list", method: .post, parameters:json, encoding: URLEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                DispatchQueue.main.async {
                    let json = JSON(data: response.data!)
                    let jsonDict = json.dictionaryObject
                    let descriptionStr = jsonDict?["message"] as! String
                   if  descriptionStr == "Followers List"{
                    
                    let dataArray = jsonDict?["data"] as! NSArray
                    print(dataArray)
                    self.dataMutableArray = dataArray.mutableCopy() as! NSMutableArray
                    print(self.dataMutableArray)
                   
                   }
                   
                   else if((response.error) != nil){
                    
                    self.alertView(title: "", message: (response.error?.localizedDescription)!)
                    
                    }
                   else{
                    
                    self.alertView(title: "", message: "No data found")
                    
                    }
                     DispatchQueue.main.async{
                    
                        self.activityIndicator.isHidden = true
                        self.activityIndicator.stopAnimating()
                        self.tableView.reloadData()
                        
                    } // end of dispatch
                    
                }  // end of outer dispatch
      
        }  // end of alamofire methd
        
  }  //  end of method
 
    
    
  // Mark: Alert View
    func alertView(title: String, message: String){
    
    
        let alertView = UIAlertController(title: title, message:message , preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
            
        })
        alertView.addAction(action)
        self.present(alertView, animated: true, completion: nil)
   }
    
    // Mark: Other follower list
    // fetching followers user list
    func otherFollowersList(userId: String,authToken: String){
        
      
        
        DispatchQueue.main.async{
            
             self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
            
        }
        let json = [
            "user_id":userId,
            "auth_token":authToken,
            "other_user_id": otherUserId
            ] as [String : Any]
        print(json)
        Alamofire.request("http://beta.brstdev.com/politicsplay/api/web/v1/users/get_list_of_followers", method: .post, parameters:json, encoding: URLEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                DispatchQueue.main.async {
                    let json = JSON(data: response.data!)
                    let jsonDict = json.dictionaryObject
                    let descriptionStr = jsonDict?["message"] as! String
                    if  descriptionStr == "List of followers"{
                     
                    let dataArray = jsonDict?["data"] as! NSArray
                    self.dataOtherUserArray = dataArray.mutableCopy() as! NSMutableArray
                    print(self.btnStatus)
                   
                    }
                    
                    else if descriptionStr == "No data found."{
                        
                     self.alertView(title: "", message: descriptionStr)
                        
                    }
                    
                    else if((response.error) != nil){
                        
                        self.alertView(title: "", message: (response.error?.localizedDescription)!)
                        
                    }
                    
                  DispatchQueue.main.async{
                        
                        self.activityIndicator.isHidden = true
                        self.activityIndicator.stopAnimating()
                        self.tableView.reloadData()
                        
                    } // end of dispatch
                    
                }  // end of outer dispatch
                
        }  // end of alamofire methd
        
    }  //  end of method
    
    
    
    
    // Mark: In case of my profile
    func btnFollowUnfollowCandidatesMethod(_ sender: UIButton)
    {
        
        let dictUserProfile = dataMutableArray[sender.tag] as! NSDictionary
        var followerID = dictUserProfile.value(forKey: "follower_user_id") as! Int
        var index = IndexPath()
        index = IndexPath.init(item: Int(sender.tag), section: 0)
        let cell = self.tableView.cellForRow(at: index) as! FollowerCellTableViewCell
        if let text =  cell.btnFollowStatus.titleLabel?.text {
        if text == "Following"{
            
               let alertView = UIAlertController(title: "", message:"Do you really want to unfollow?" , preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                  
                    self.followUnfollowUser(userId:self.userIdValueAgain, authToken:self.authTokenValue , status: 0, otherUsersId: followerID)
                })
            let action1 = UIAlertAction(title: "Cancel", style: .default, handler: { (alert) in
            })
                alertView.addAction(action)
                 alertView.addAction(action1)
                self.present(alertView, animated: true, completion: nil)
            
        }
     else{
        
          followUnfollowUser(userId:userIdValueAgain, authToken:authTokenValue , status: 1, otherUsersId: followerID)
        
         }

    }
   
    
    }

    // Mark: In case of other user profile
    func btnFollowUnfollowCandidatesUserProfileMethod(_ sender: UIButton)
    {
        
        print(sender.tag)
        let dictUserProfile = dataOtherUserArray[sender.tag] as! NSDictionary
        var followerID = dictUserProfile.value(forKey: "user_id") as! Int
        var index = IndexPath()
        index = IndexPath.init(item: Int(sender.tag), section: 0)
        let cell = self.tableView.cellForRow(at: index) as! FollowerCellTableViewCell
        if let text =  cell.btnFollowStatus.titleLabel?.text {
            if text == "Following"{
                
                let alertView = UIAlertController(title: "", message:"Do you really want to unfollow?" , preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                    
                    self.followUnfollowUser(userId:self.userIdValueAgain, authToken:self.authTokenValue , status: 0, otherUsersId: followerID)
                })
                let action1 = UIAlertAction(title: "Cancel", style: .default, handler: { (alert) in
                })
                alertView.addAction(action)
                alertView.addAction(action1)
                self.present(alertView, animated: true, completion: nil)
            }
            else{
                
                followUnfollowUser(userId:userIdValueAgain, authToken:authTokenValue , status: 1, otherUsersId: followerID)
                
            }
            
        }
        
        
    }
    
    
    
    
    


}
extension FolllowersViewController : UITableViewDataSource,UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if checkProfile == true{
        
            print(dataMutableArray.count)
            return  dataMutableArray.count
        }
        
        else{
        
            print(dataOtherUserArray.count)
            return  dataOtherUserArray.count
            
            
        }
   
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: "followersCell", for: indexPath) as! FollowerCellTableViewCell
        if checkProfile == true{
        
            
            // Mark: for my profile
            let dict = dataMutableArray[indexPath.row] as! NSDictionary
            print(dict)
            let dataDict = dict.value(forKey: "follower_data") as! NSDictionary
            followerUserId = dict.value(forKey: "follower_user_id") as! NSInteger
            let firstName = dataDict.value(forKey: "firstname") as! NSString
            let lastName = dataDict.value(forKey: "lastname") as! NSString
            concatenateString  = "\(firstName) \(lastName)"
            cell.lblFollowersName.text = concatenateString
            btnStatusMyProfile = dict.value(forKey: "is_following") as! NSInteger
            if  btnStatusMyProfile == 1{
                cell.btnFollowStatus.setTitle("Following",for: .normal)
                
            }
            else{
                
                cell.btnFollowStatus.setTitle("Follow",for: .normal)
                
            }
            
            cell.btnFollowStatus.tag = indexPath.row
            cell.btnFollowStatus.addTarget(self, action: #selector(btnFollowUnfollowCandidatesMethod), for: UIControlEvents.touchUpInside)
          
        }
       
        
        else{
        
            
            // Mark: for other user profile
            let dictUserProfile = dataOtherUserArray[indexPath.row] as! NSDictionary
            print(dictUserProfile)
            let firstName = dictUserProfile.value(forKey: "firstname") as! NSString
            let lastName = dictUserProfile.value(forKey: "lastname") as! NSString
             otherFollowerId = dictUserProfile.value(forKey: "user_id") as! NSInteger
            btnStatus = dictUserProfile.value(forKey: "is_following") as! NSInteger
             concatenateString = "\(firstName) \(lastName)"
            cell.lblFollowersName.text = concatenateString
            if btnStatus == 1{
                
                cell.btnFollowStatus.setTitle("Following",for: .normal)
              
            }
            else{
                
                cell.btnFollowStatus.setTitle("Follow",for: .normal)
             
                
            }
        
            cell.btnFollowStatus.tag = indexPath.row
            cell.btnFollowStatus.addTarget(self, action: #selector(btnFollowUnfollowCandidatesUserProfileMethod), for: UIControlEvents.touchUpInside)
            if  userIdValueAgain == String(describing:otherFollowerId){

                cell.btnFollowStatus.isHidden = true

            }
            else{

                cell.btnFollowStatus.isHidden = false

            }
            
        }


     if (indexPath.row % 2) == 0 {
          
            cell.btnFollowStatus.layer.borderColor = UIColor.init(red: 191/255.0, green: 0, blue: 0, alpha: 1.0).cgColor
            cell.btnFollowStatus.layer.borderWidth = 1;
            cell.btnFollowStatus.tintColor =  UIColor.init(red: 191/255.0, green: 0, blue: 0, alpha: 1.0)
            cell.btnFollowStatus.layer.cornerRadius = cell.btnFollowStatus.frame.height/2
            cell.btnFollowStatus.clipsToBounds = true
         ///   cell.btnFollowStatus.setTitle("Following",for: .normal)
            cell.contentView.backgroundColor = UIColor.init(red: 231/255.0, green: 236/255.0, blue: 242/255.0, alpha: 1.0)
        
        }
    
        else {
            
            cell.btnFollowStatus.backgroundColor =  UIColor.init(red: 191/255.0, green: 0, blue: 0, alpha: 1.0)
            cell.btnFollowStatus.tintColor = UIColor.white
            cell.btnFollowStatus.layer.cornerRadius =  cell.btnFollowStatus.frame.height/2
            cell.btnFollowStatus.clipsToBounds = true
       //     cell.btnFollowStatus.setTitle("Follow",for: .normal)
        }
        
        // code to make text underline
        let str : NSString = concatenateString as NSString
        cell.lblFollowersName.delegate = self
        cell.lblFollowersName.tag = indexPath.row
        cell.lblFollowersName.text = str as String
        let range : NSRange = str.range(of: str as String)
        cell.lblFollowersName.addLink(to: NSURL(string: "")! as URL!, with: range)
        cell.selectionStyle = UITableViewCellSelectionStyle.none;
        return cell
 
  }
    
    
    
    
    
    
    
    
    

}
