//
//  WhatsOnMindVC.swift
//  PoliticsPlay
//
//  Created by Brst981 on 31/10/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit
import AVFoundation
import MobileCoreServices
import AssetsLibrary
import Alamofire
import SwiftyJSON
import Photos


protocol WhatsOnMindVCDelegate:class{
    
     func getProfileDataMethd(userId: String,authToken: String)
     func postData(userId: String,authToken: String,page: String,completion: @escaping (_ result: Bool) -> Void)

}


class WhatsOnMindVC: UIViewController,TagFriendsDelegate,UITextViewDelegate {
    

    
    
    //outlets
    
    @IBOutlet var viewStatus: UIView!
    @IBOutlet var btnPost: UIButton!
    @IBOutlet var showPhotoImgView: UIImageView!
    @IBOutlet var showVideoImgView: UIImageView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var textView: UITextView!
    @IBOutlet var btnChoosePhoto: UIButton!
    @IBOutlet var btnChooseVideo: UIButton!
    @IBOutlet var btnTagFriends: UIButton!
    @IBOutlet var thumbnailIcon: UIImageView!
   // @IBOutlet var thumbnailIcon: UIImageView!
   @IBOutlet var thumbnailIconChooseVideo: UIImageView!
    // variables
    var videoURL: NSURL?
    let myPickerController = UIImagePickerController()
    var mediaBrowsedType = String()
    var selectedImage: UIImage?
    var userIdValueAgain = String()
    var authTokenValue  = String()
    var selectedVideoUrl = String()
    var followersIdData = NSInteger()
    var check = Bool()
    var checkEmpty = Bool()
    var tagCheck =  String()
    var rotatedImage = UIImage()
    var textViewCheck = Bool()
    var thumbnail = UIImage()
    var campValue = String()
    weak var delegate: WhatsOnMindVCDelegate?
    var type = String()


    override func viewDidLoad() {
        super.viewDidLoad()
        customizeView()

        // Do any additional setup after loading the view.
        myPickerController.delegate = self


        
        
        //tap gesture for dismissing the keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginVC.dismissKeyboard))
        view.addGestureRecognizer(tap)
        self.activityIndicator.isHidden = true
       
       self.textView.delegate = self
       self.textView.text = "Add description to your post"
        self.textView.textColor = UIColor.lightGray
        Singleton.sharedInstance.taggedPeople.removeAllObjects()
    
       
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
        // Dispose of any resources that can be recreated.
    }
    
    // Mark: Controlling text view
    func textViewDidBeginEditing(_ textView: UITextView) {
        if self.textView.textColor == UIColor.lightGray {
            self.textView.text = nil
            self.textView.textColor = UIColor.black
        }
    }
    
    
    // Mark: Controlling text view
    func textViewDidEndEditing(_ textView: UITextView) {
        if self.textView.text.isEmpty {
            self.textView.text = ""
            self.textView.textColor = UIColor.lightGray
        }
   }
    
 
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        // as @nhgrif suggested, we can skip the string manipulations if
        // the beginning of the textView.text is not touched.
        guard range.location == 0 else {
            return true
        }
        
        let newString = (textView.text as NSString).replacingCharacters(in: range, with: text) as NSString
        return newString.rangeOfCharacter(from: NSCharacterSet.whitespacesAndNewlines).location != 0
    }
    
    
    
    
   // for passing the tagged array
    func finishPassing(taggedFriends: NSMutableArray) {
        

        print(taggedFriends)
        Singleton.sharedInstance.taggedPeople = taggedFriends 
        print(Singleton.sharedInstance.taggedPeople)
      self.tagCheck = String(describing: Singleton.sharedInstance.taggedPeople)
    
    }

    func customizeView(){
        
      btnPost.layer.cornerRadius = btnPost.frame.height/2
      viewStatus.layer.shadowColor = UIColor.black.cgColor
      viewStatus.layer.shadowOpacity =  0.25
      viewStatus.layer.shadowRadius = 1.0
      viewStatus?.layer.shadowOffset = CGSize(width: 1, height: 1.0)

    }
    
    
    @IBAction func popBack(_ sender: Any) {
        
        if UserDefaults.standard.value(forKey: "userId") != nil{
            
           let userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as! String
            let authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
           // print(userIdValueAgain)
            print(authTokenValue)
            delegate?.getProfileDataMethd(userId: userIdValueAgain , authToken: authTokenValue )
//            delegate?.postData(userId: userIdValueAgain , authToken: authTokenValue , page: "1"){
//                (result:
//                Bool) in
//                print("got back: \(result)")
//
//            }
        }
        
        self.navigationController?.popViewController(animated: true)
        
       
        
        
        
   
    }

    
    //tap gesture
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    
    @IBAction func postToProfile(_ sender: Any) {
       
        
        
        if UserDefaults.standard.value(forKey: "userId") != nil{
            
            userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as! String
            authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
            print(userIdValueAgain)
            print(authTokenValue)
            print(checkEmpty)
           
         if checkEmpty == false && textView.text == "Add description to your post"{
                
                alertView(title: "", message: "You have to post atleast one thing")
                
            }
            else{
                
                addPostIntegration(userId: userIdValueAgain , authToken: authTokenValue)
            
            }
            
            
          
        }
        
    }
    
    
    
    
  
    
    
    //MARK: CheckCameraAuthorization
    
    func checkCameraAuthorization() {
        
        let status: AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        if status == .authorized {
            // authorized            picker.delegate = self
            let viewPickerController = UIImagePickerController()
            viewPickerController.allowsEditing = true
            viewPickerController.sourceType = .camera
            present(viewPickerController, animated: true, completion: { _ in })
        } else if status == .denied {
            // denied
            if AVCaptureDevice.responds(to: #selector(AVCaptureDevice.requestAccess(forMediaType:completionHandler:))) {
                AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {(_ granted: Bool) -> Void in
                    // Will get here on both iOS 7 & 8 even though camera permissions weren't required
                    // until iOS 8. So for iOS 7 permission will always be granted.
                    print("DENIED")
                    if granted {
                        // Permission has been granted. Use dispatch_async for any UI updating
                        // code because this block may be executed in a thread.
                        
                        DispatchQueue.main.async(execute: {() -> Void in
                            let picker = UIImagePickerController()
                            picker.delegate = self
                            picker.allowsEditing = true
                            picker.sourceType = .camera
                            self.present(picker, animated: true, completion: { _ in })
                        })
                        
                    }
                    else {
                        // Permission has been denied.
                        
                        
                        DispatchQueue.main.async(execute: {() -> Void in
                            self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
                        })
                        
                        
                    }
                })
            }
        } else if status == .restricted {
            // restricted
            DispatchQueue.main.async(execute: {() -> Void in
                self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
            })
        }
        else if status == .notDetermined {
            // not determined
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {(_ granted: Bool) -> Void in
                if granted {
                    // Access has been granted ..do something
                    DispatchQueue.main.async(execute: {() -> Void in
                        let picker = UIImagePickerController()
                        picker.delegate = self
                        picker.allowsEditing = true
                        picker.sourceType = .camera
                        self.present(picker, animated: true, completion: { _ in })
                        
                    })
                    
                    
                }
                else {
                    // Access denied ..do something
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
                    })
                    
                }
            })
        }
    }
    
    
    
    func accessMethod(_ message: String) {
        let alertController = UIAlertController(title: "Not Authorized", message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "OK", style: .default, handler: nil)
        let Settings = UIAlertAction(title: "Settings", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        })
        alertController.addAction(Settings)
        alertController.addAction(cancel)
        present(alertController, animated: true, completion: { _ in })
    }
    
    
    
    
    
    //MARK: CheckPhotoAuthorization
    
    func checkPhotoAuthorization()  {
        let status: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        if status == .authorized {
            let viewPickerController = UIImagePickerController()
            viewPickerController.delegate = self
            viewPickerController.allowsEditing = true
            viewPickerController.sourceType = .photoLibrary
            present(viewPickerController, animated: true, completion: { _ in })
        }
        else if status == .denied {
            DispatchQueue.main.async(execute: {() -> Void in
                self.accessMethod("Please go to Settings and enable the photos for this app to use this feature.")
            })
        } else if status == .notDetermined {
            // Access has not been determined.
            PHPhotoLibrary.requestAuthorization({(_ status: PHAuthorizationStatus) -> Void in
                if status == .authorized {
                    
                    DispatchQueue.main.async(execute: {() -> Void in
                        let viewPickerController = UIImagePickerController()
                        viewPickerController.delegate = self
                       viewPickerController.allowsEditing = true
                        viewPickerController.sourceType = .photoLibrary
                        self.present(viewPickerController, animated: true, completion: { _ in })
                    
                    })
                    
                }
                else {
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.accessMethod("Please go to Settings and enable the photos for this app to use this feature.")
                    })
                }
            })
        }
        else if status == .restricted {
            // Restricted access - normally won't happen.
        }
        
        
    }
    
    
    // code to show and  dismiss the list
    
    
    @IBAction func showFollowersList(_ sender: Any) {
   
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let tagViewController = storyBoard.instantiateViewController(withIdentifier:
            "TagFriendsVC") as! TagFriendsVC
       //self.navigationController?.pushViewController(tagViewController, animated: true)
        tagViewController.selectionArray =  Singleton.sharedInstance.taggedPeople
        self.present(tagViewController, animated: true, completion: nil)
        tagViewController.delegateFriends = self

    }
    
    // choose video and image from library
    @IBAction func viewLibrary(_ sender: Any) {
        
        
        let actionSheetControllerSecond = UIAlertController()
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            
       }
        
        actionSheetControllerSecond.addAction(cancelAction)
        let chooseVideoAction: UIAlertAction = UIAlertAction(title: "Select Video", style: .default) { action -> Void in
            // for video
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.mediaTypes = ["public.movie"]
            myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.present(myPickerController, animated: true, completion: nil)
            self.check = false
       }
        actionSheetControllerSecond.addAction(chooseVideoAction)
        
        
        
        let choosePictureAction: UIAlertAction = UIAlertAction(title: "Select Picture", style: .default) { action -> Void in
            // for picture
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.allowsEditing = true
            myPickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.present(myPickerController, animated: true, completion: nil)
        }
        actionSheetControllerSecond.addAction(choosePictureAction)
        
        self.present(actionSheetControllerSecond, animated: true, completion: nil)
        
        
    }
    
    // take picture and video from phone
    @IBAction func takeVideo(_ sender: Any) {
        
        // Mark: taking video from phone
        let actionSheetController = UIAlertController()
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            
        }
        actionSheetController.addAction(cancelAction)
        
        let takeVideoAction: UIAlertAction = UIAlertAction(title: "Take Video", style: .default) { action -> Void in
            // for video
            if(UIImagePickerController.isSourceTypeAvailable(.camera))
            {
                let imagePickerController = UIImagePickerController()
                imagePickerController.delegate = (self as UIImagePickerControllerDelegate & UINavigationControllerDelegate)
                imagePickerController.sourceType = .camera
                imagePickerController.mediaTypes = [kUTTypeMovie as NSString as String]
                imagePickerController.isEditing = true
                imagePickerController.videoMaximumDuration = 60.0
                self.present(imagePickerController, animated: true, completion: nil)
                self.check = true
            }
            else{
                
                self.alertView(title: "", message: "Camera is not available")

                
            }
            
        }
        
        actionSheetController.addAction(takeVideoAction)
        
        // for picture
        let takePictureAction: UIAlertAction = UIAlertAction(title: "Take Picture", style: .default) { action -> Void in
            // for picture
            if(UIImagePickerController.isSourceTypeAvailable(.camera))
            {
                let imagePickerController = UIImagePickerController()
                imagePickerController.delegate = self
                imagePickerController.sourceType = .camera
                imagePickerController.isEditing = true
                self.present(imagePickerController, animated: true, completion: nil)
            }
            else{
                
               self.alertView(title: "", message: "Camera is not available")
            }
        }
        
        actionSheetController.addAction(takePictureAction)
        self.present(actionSheetController, animated: true, completion: nil)
        
    }
    
    
    //Mark: alert view controller
    func alertView(title: String, message: String){
    
      let alertView = UIAlertController(title: title, message:message , preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
            
        })
        alertView.addAction(action)
        self.present(alertView, animated: true, completion: nil)
    }
    
    
    
    
    // Mark: add post api integration in case of image
    func addPostIntegration(userId: String,authToken: String){
        
//        // id from run for office
//        self.campValue = (UserDefaults.standard.value(forKey: "runForOfficeKey") as? String)!
//        if campValue != ""{
//
//            print(campValue)
//
//        }
//        else{
//
//            print("error")
//
//        }
        
        // id from run for office
        
        let valueId = UserDefaults.standard.value(forKey: "runForOfficeKey") as? String
        if valueId != nil{
            
            self.campValue = valueId!
            
        }
            
        else{
            
            self.campValue = ""
            
        }
        
        
        DispatchQueue.main.async {
            
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
            self.btnChoosePhoto.isEnabled = false
            self.btnChooseVideo.isEnabled = false
            self.btnTagFriends.isEnabled = false
            self.textView.isUserInteractionEnabled = false
            self.btnPost.isUserInteractionEnabled = false
        }
        
        
        if textView.text == "Add description to your post"{
        
          textView.text = ""
        
       }
        
      if mediaBrowsedType == "public.image"{
        
        // for converting uiimage to nsdata
        let dataImage = UIImagePNGRepresentation(selectedImage!) as NSData?
        let image : UIImage = UIImage(data: dataImage as! Data)!
            
            
            let parameters = ["user_id":userId,
                              "auth_token":authToken,
                              "text":textView.text,
                              "tag_friends":tagCheck,
                              "runforoffice_id":self.campValue
                
                ] as [String : Any]
            
            print(parameters)
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                multipartFormData.append(UIImagePNGRepresentation(image)!, withName: "media", fileName: "profilepic.jpg", mimeType: "image/jpg")
                for (key, value) in parameters {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            }, to:"http://beta.brstdev.com/politicsplay/api/web/v1/users/add_post")
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        
                    })
                    
                    upload.responseJSON { response in
                        //print response.result
                        print(response)
                        
                        if response.result.value != nil {
                            
                            DispatchQueue.main.async {
                                
                                let json = JSON(data: response.data! )
                                
                                if let descriptionStr = json["message"].string {
                                    print(descriptionStr)
                                    if descriptionStr == "Post Added Successfully"{
                                        
                                        let alertView = UIAlertController(title: "", message:descriptionStr , preferredStyle: .alert)
                                        let action = UIAlertAction(title: "OK", style: .default, handler:{ (alert) in
                                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                         let tabController = storyBoard.instantiateViewController(withIdentifier:
                                                "TabBarViewController") as! TabBarViewController
                                       tabController.selectedIndex1 = 4
                                      self.navigationController?.pushViewController(tabController, animated: true)
                                     })
                                        alertView.addAction(action)
                                        self.present(alertView, animated: true, completion: nil)
                                        
                                    }  // end of if
                           
                                }
                            }
                            
                            DispatchQueue.main.async{
                                
                                 self.activityIndicator.isHidden = true
                                 self.activityIndicator.stopAnimating()
                                self.btnChoosePhoto.isEnabled = true
                                self.btnChooseVideo.isEnabled = true
                                self.btnTagFriends.isEnabled = true
                                self.textView.isUserInteractionEnabled = true
                                self.btnPost.isUserInteractionEnabled = true

                            }
                        }
                    }
                    
                case .failure(let encodingError):
                    
                
//                    let alertView = UIAlertController(title: "", message:(response.error?.localizedDescription)! , preferredStyle: .alert)
//                    let action = UIAlertAction(title: "OK", style: .default, handler:{ (alert) in
//                    })
//                    alertView.addAction(action)
//                    self.present(alertView, animated: true, completion: nil)
                    break
                    //print encodingError.description
                    
                }
                
            }
  }  // end of image uploading method
        
        
        
     else if (mediaBrowsedType == "public.movie"){
            
        uploadVideo(userId: userIdValueAgain,authToken: authTokenValue)
    
    }  // end of video case
        
   
    // case for updating simple text
        
    else{
        
    
        DispatchQueue.main.async{
        
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
            self.btnChoosePhoto.isEnabled = false
            self.btnChooseVideo.isEnabled = false
            self.btnTagFriends.isEnabled = false
            self.textView.isUserInteractionEnabled = false
            self.btnPost.isUserInteractionEnabled = false

            
        }
        
       if textView.text == ""{
        
        
        self.alertView(title: "", message: "You cannot send blank description")
        
       }
      
       
        let json = ["user_id":userId,
                    "auth_token":authToken,
                    "text":textView.text,
                    "tag_friends":tagCheck,
                    "runforoffice_id":self.campValue
            ] as [String : Any]
        print(json)
        
        
        Alamofire.request("http://beta.brstdev.com/politicsplay/api/web/v1/users/add_post", method: .post, parameters:                                           json, encoding: URLEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                DispatchQueue.main.async {
                    let json = JSON(data: response.data! )
                    let descripStr = json["message"].string
                    if descripStr == "Post Added Successfully"{
                        
                      
                        let alertView = UIAlertController(title: "", message:descripStr , preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default, handler:{ (alert) in
                            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                            let tabController = storyBoard.instantiateViewController(withIdentifier:
                                "TabBarViewController") as! TabBarViewController
                            tabController.selectedIndex1 = 4
                            self.navigationController?.pushViewController(tabController, animated: true)
                        })
                        alertView.addAction(action)
                        self.present(alertView, animated: true, completion: nil)
    
                    }  // end of if
                    
                        
                    else if((response.error) != nil){
                        
                        let alertView = UIAlertController(title: "", message:(response.error?.localizedDescription)! , preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default, handler:{ (alert) in
                        })
                        alertView.addAction(action)
                        self.present(alertView, animated: true, completion: nil)
                        
                    }  // end of error if
                    
                    DispatchQueue.main.async {
                        
                        
                        self.btnChoosePhoto.isEnabled = true
                        self.btnChooseVideo.isEnabled = true
                        self.btnTagFriends.isEnabled = true
                        self.activityIndicator.isHidden = true
                        self.activityIndicator.stopAnimating()
                        self.textView.isUserInteractionEnabled = true
                        self.btnPost.isUserInteractionEnabled = true
                    }
                    
                }
                
        }
        
   }  // end of only updating text

}
    
    
    //Mark: To upoad video
    func uploadVideo(userId: String,authToken: String){
    
        
        DispatchQueue.main.async{
        
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
            self.btnChoosePhoto.isEnabled = false
            self.btnChooseVideo.isEnabled = false
            self.btnTagFriends.isEnabled = false
            self.textView.isUserInteractionEnabled = false
            self.btnPost.isUserInteractionEnabled = false
        }
        
        
        var videoData =  Data()
        do {
            videoData =  try NSData(contentsOfFile: (self.videoURL?.relativePath)!, options: NSData.ReadingOptions.alwaysMapped) as Data
            print(videoData)
        }
        catch {
            
        }
        
        print(videoData)
        
   let parameters = ["user_id":userId,
                      "auth_token":authToken,
                      "text":textView.text,
                      "tag_friends":tagCheck,
                      "runforoffice_id":self.campValue] as [String : Any]
        
        
        print(parameters)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(videoData, withName: "media", fileName: "video.mp4", mimeType: "video/mp4")
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
                        }, to:"http://beta.brstdev.com/politicsplay/api/web/v1/users/add_post")
                        { (result) in
                            switch result {
                            case .success(let upload, _, _):
        
                                upload.uploadProgress(closure: { (progress) in
                                    //Print progress
                                    //  print(progress)
        
                                })
        
                                upload.responseJSON { response in
                                    //print response.result
                                    print(response)
        
                                    if response.result.value != nil {
        
                                        DispatchQueue.main.async {
        
                                            let json = JSON(data: response.data! )
                                          let postId = json["post_id"].int
                                           print(postId!)
                                            self.uploadVideoAgain(userId: self.userIdValueAgain,authToken: self.authTokenValue,postIdValue : postId!)
                                           
        
                                            if let descriptionStr = json["message"].string {
                                                print(descriptionStr)
                                             if ((response.error) != nil){
                                                    let alertView = UIAlertController(title: "", message:(response.error?.localizedDescription)! , preferredStyle: .alert)
                                                    let action = UIAlertAction(title: "OK", style: .default, handler:nil)
                                                    alertView.addAction(action)
                                                    self.present(alertView, animated: true, completion: nil)
                                                
                                                }
                                                
                                            }
                                        }
        
                                        DispatchQueue.main.async{
                                            self.activityIndicator.isHidden = true
                                            self.activityIndicator.stopAnimating()
                                            self.btnChoosePhoto.isEnabled = true
                                            self.btnChooseVideo.isEnabled = true
                                            self.btnTagFriends.isEnabled = true
                                            self.textView.isUserInteractionEnabled = true
                                            self.btnPost.isUserInteractionEnabled = true
                                        } // end of outer DispatchQueue
                                   
                                    
                                    }
                              
                                
                                }
                                
                            case .failure(let encodingError):
                                
                              //  self.alertViewMethod(titleStr: "", messageStr: encodingError.localizedDescription)
                                
                                break
                                
                            }
                            
                        }
        }
    
    //Mark: To upoad video again
    func uploadVideoAgain(userId: String,authToken: String,postIdValue: Int){
        
        DispatchQueue.main.async{
            
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
            self.btnChoosePhoto.isEnabled = false
            self.btnChooseVideo.isEnabled = false
            self.btnTagFriends.isEnabled = false
            self.textView.isUserInteractionEnabled = false
            self.btnPost.isUserInteractionEnabled = false

        }
        do {
                // for creating thumbnail for video url
          let asset = AVURLAsset(url: videoURL! as URL , options: nil)
                    let imgGenerator = AVAssetImageGenerator(asset: asset)
                    imgGenerator.appliesPreferredTrackTransform = true
                    let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
                    let uiImage = UIImage(cgImage: cgImage)
                    thumbnail = uiImage
        }

        catch{
        
            print("*** Error generating thumbnail: \(error.localizedDescription)")
        }
        
        let dataImage = UIImagePNGRepresentation(thumbnail) as NSData?
        let image : UIImage = UIImage(data: dataImage! as Data)!
        
        
        
        let parameters = ["post_id":String(describing: postIdValue)] as  [String : Any]
        
        
        print(parameters)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(UIImagePNGRepresentation(image)!, withName: "thumbnail", fileName: "profilepic.jpg", mimeType: "image/jpg")
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:"http://beta.brstdev.com/politicsplay/api/web/v1/users/upload_thumb")
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                    //  print(progress)
                    
                })
                
                upload.responseJSON { response in
                    //print response.result
                    print(response)
                    
                    if response.result.value != nil {
                        
                        DispatchQueue.main.async {
                            
                            let json = JSON(data: response.data! )
                            
                            if let descriptionStr = json["message"].string {
                                print(descriptionStr)
                                
                                if descriptionStr == "Post Added Successfully"{
                                    
                                    let alertView = UIAlertController(title: "", message:descriptionStr , preferredStyle: .alert)
                                    let action = UIAlertAction(title: "OK", style: .default, handler:{ (alert) in
                                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                                        let tabController = storyBoard.instantiateViewController(withIdentifier:
                                            "TabBarViewController") as! TabBarViewController
                                        tabController.selectedIndex1 = 4
                                        self.navigationController?.pushViewController( tabController, animated: true)
                                        
                                    })
                                    alertView.addAction(action)
                                    self.present(alertView, animated: true, completion: nil)
                                }
                                
                                if ((response.error) != nil){
                                    let alertView = UIAlertController(title: "", message:(response.error?.localizedDescription)! , preferredStyle: .alert)
                                    let action = UIAlertAction(title: "OK", style: .default, handler:nil)
                                    alertView.addAction(action)
                                    self.present(alertView, animated: true, completion: nil)
                                    
                                }
                                
                            }
                        }
                        
                        DispatchQueue.main.async{
                            
                            self.activityIndicator.isHidden = true
                            self.activityIndicator.stopAnimating()
                            self.btnChoosePhoto.isEnabled = true
                            self.btnChooseVideo.isEnabled = true
                            self.btnTagFriends.isEnabled = true
                            self.textView.isUserInteractionEnabled = true
                            self.btnPost.isUserInteractionEnabled = true
                        }
                    }
                }
                
            case .failure(let encodingError):
                
                //  self.alertViewMethod(titleStr: "", messageStr: encodingError.localizedDescription)
                
                break
                
            }
            
        }
    }
    
    
    
    
    
//    // MARK: Text View Validations
//    func validate(textView: UITextView) {
//       
//        let text = textView.text
//            !(text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty)!
//        
//    }
    
    
    //Mark: For setting the orientation of image before sending it to server
    func imageOrientation(_ src:UIImage)->UIImage {
        if src.imageOrientation == UIImageOrientation.up {
            return src
        }
        var transform: CGAffineTransform = CGAffineTransform.identity
        switch src.imageOrientation {
        case UIImageOrientation.down, UIImageOrientation.downMirrored:
            transform = transform.translatedBy(x: src.size.width, y: src.size.height)
            transform = transform.rotated(by: CGFloat(M_PI))
            break
        case UIImageOrientation.left, UIImageOrientation.leftMirrored:
            transform = transform.translatedBy(x: src.size.width, y: 0)
            transform = transform.rotated(by: CGFloat(M_PI_2))
            break
        case UIImageOrientation.right, UIImageOrientation.rightMirrored:
            transform = transform.translatedBy(x: 0, y: src.size.height)
            transform = transform.rotated(by: CGFloat(-M_PI_2))
            break
        case UIImageOrientation.up, UIImageOrientation.upMirrored:
            break
        }
        
        switch src.imageOrientation {
        case UIImageOrientation.upMirrored, UIImageOrientation.downMirrored:
            transform.translatedBy(x: src.size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case UIImageOrientation.leftMirrored, UIImageOrientation.rightMirrored:
            transform.translatedBy(x: src.size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case UIImageOrientation.up, UIImageOrientation.down, UIImageOrientation.left, UIImageOrientation.right:
            break
        }
        
        let ctx:CGContext = CGContext(data: nil, width: Int(src.size.width), height: Int(src.size.height), bitsPerComponent: (src.cgImage)!.bitsPerComponent, bytesPerRow: 0, space: (src.cgImage)!.colorSpace!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        
        ctx.concatenate(transform)
        
        switch src.imageOrientation {
        case UIImageOrientation.left, UIImageOrientation.leftMirrored, UIImageOrientation.right, UIImageOrientation.rightMirrored:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.height, height: src.size.width))
            break
        default:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.width, height: src.size.height))
            break
        }
        
        let cgimg:CGImage = ctx.makeImage()!
        let img:UIImage = UIImage(cgImage: cgimg)
        
        return img
    }
    
    
    
    //MARK: Resize Image
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0,y: 0,width: newWidth,height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    
    }

  extension WhatsOnMindVC:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    //Mark: Image Picker Delegate Methods
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]){
        
        
        
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        
        if mediaType == kUTTypeMovie {
            
           videoURL  = info[UIImagePickerControllerMediaURL] as? NSURL
            mediaBrowsedType = "public.movie"
            do {
              // for creating thumbnail for video url
                if check == true{
                
                    let asset = AVURLAsset(url: videoURL! as URL , options: nil)
                    let imgGenerator = AVAssetImageGenerator(asset: asset)
                    imgGenerator.appliesPreferredTrackTransform = true
                    let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
                    let uiImage = UIImage(cgImage: cgImage)
                    self.showVideoImgView.image = uiImage
                    self.showPhotoImgView.image = UIImage(named:"img")
                    self.thumbnailIcon.image =  UIImage(named:"playbutton")
                    self.thumbnailIconChooseVideo.image =  UIImage(named:"")

                    checkEmpty = true
                }
               
                else{
                
                    let asset = AVURLAsset(url: videoURL! as URL , options: nil) 
                    let imgGenerator = AVAssetImageGenerator(asset: asset)
                    imgGenerator.appliesPreferredTrackTransform = true
                    let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
                    let uiImage = UIImage(cgImage: cgImage)
                    self.showPhotoImgView.image = uiImage
                    self.showVideoImgView.image = UIImage(named:"camera")
                    self.thumbnailIconChooseVideo.image =  UIImage(named:"playbutton")
                    self.thumbnailIcon.image =  UIImage(named:"")
                    checkEmpty = true
                }
                
            }  // end of do
            
            catch let error {
                
                print("*** Error generating thumbnail: \(error.localizedDescription)")
            
            }
            
            dismiss(animated: true, completion: nil)
       
        }
            
      // for showing image section
        else if((info["UIImagePickerControllerOriginalImage"] as? UIImage) != nil) {
            
           mediaBrowsedType = "public.image"
            
           if let editedImage = info["UIImagePickerControllerEditedImage"]  as? UIImage
           {
            selectedImage = resizeImage(image: editedImage, newWidth: 400)
            self.showPhotoImgView.image = editedImage
            self.showVideoImgView.image = UIImage(named:"camera") // for choosing one of the option only
            print(selectedImage)
            self.thumbnailIconChooseVideo.image =  UIImage(named:"")
            checkEmpty = true
        }
           else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            
            selectedImage = resizeImage(image: originalImage, newWidth: 400)
            self.rotatedImage = self.imageOrientation(selectedImage!)
            self.showVideoImgView.image = originalImage
            self.showPhotoImgView.image = UIImage(named: "img")  // for choosing one of the option only
                selectedImage = self.rotatedImage // sending this image to server
                  print(selectedImage)
           self.thumbnailIcon.image =  UIImage(named:"")
            checkEmpty = true
            }
            self.dismiss(animated: true, completion: nil)
        
        }
        
    }
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
        
    }


}






