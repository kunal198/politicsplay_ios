
//
//  AppDelegate.swift
//  PoliticsPlay
//
//  Created by Brst981 on 24/10/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import UserNotifications
import FBSDKLoginKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var nvc: UINavigationController!
    var tabBarControl:TabBarViewController!
    var check = String()


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
   
        
        // Push notification
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self

            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization( options: authOptions, completionHandler: {granted, error in
                
                if let error = error {
                    print("D'oh: \(error.localizedDescription)")
                }
                
                else{
                    
                 //   application.registerForRemoteNotifications()
                }
                
                
            })

        } else {
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }

       application.registerForRemoteNotifications()
        

        
        
       
        
        IQKeyboardManager.sharedManager().enable = true
        FBSDKSettings.setAppID("207033006521166")
        
        
        // Check Whether user is autologin
        
        if UserDefaults.standard.value(forKey: "authToken") != nil {

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            tabBarControl = storyboard.instantiateViewController(withIdentifier:
                "TabBarViewController") as! TabBarViewController
            
            tabBarControl.selectedIndex1 = 0
            
            let leftViewController = storyboard.instantiateViewController(withIdentifier:"LeftMenuVC") as! LeftMenuVC
            
            nvc = UINavigationController(rootViewController: tabBarControl)
            nvc.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            nvc.navigationBar.shadowImage = UIImage()
            nvc.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
            // leftViewController.findCasteeVC = nvc
            
            let slideMenuController =  ExSlideMenuController(mainViewController:nvc, leftMenuViewController: leftViewController )
            slideMenuController.automaticallyAdjustsScrollViewInsets = true
            //   slideMenuController.delegate = mainViewController
            
            let transition = CATransition()
            transition.duration = 0.5
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            transition.type = kCATransitionFade
            
            self.window?.layer.add(transition, forKey: nil)
            self.window?.rootViewController = slideMenuController
            self.window?.makeKeyAndVisible()
            
        }
        

        return true
}
    
    
    // code to signout from facebook once user terminate the application
    func applicationWillTerminate(application: UIApplication)
    {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
//        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
//        loginManager.logOut()

    }
    
    
    // Add method openURL, it is used to control flow once user successfully logged in through Facebook and returning back to demo application.
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool
    {
       
        
        
//        if LinkedinSwiftHelper.shouldHandleUrl(url) {
//            return LinkedinSwiftHelper.application(application,
//                                                   openURL: url,
//                                                   sourceApplication: sourceApplication,
//                                                   annotation: annotation
//            )
//        }

        
        if check == "LinkedIn"{
            
            if (LISDKCallbackHandler .shouldHandle(url)) {
                return LISDKCallbackHandler.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
            }
          
//          let linkenInDidHandel = LISDKCallbackHandler.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
//
//             return linkenInDidHandel
            
            return true
        }
       else  {
        
            let facebookDidHandle = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
            
                return facebookDidHandle
        
        }
        
    }
    
    
    //push notification delegate
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
      Singleton.sharedInstance.deviceTokenStr  = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
     print(Singleton.sharedInstance.deviceTokenStr)
    //  UserDefaults.standard.set(Singleton.sharedInstance.deviceTokenStr, forKey: "deviceTokenStr")
    //  UserDefaults.standard.synchronize()
     // print( Singleton.sharedInstance.deviceTokenStr)
        
        
    }
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print("i am not available in simulator \(error)")
        
    }
    
    
    
    
    //MARK: Menu
    func createMenu()  {
        
      let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
      let tabBarControl = storyboard.instantiateViewController(withIdentifier:
                   "TabBarViewController") as! TabBarViewController
        tabBarControl.selectedIndex1 = 0
        let leftViewController = storyboard.instantiateViewController(withIdentifier:"LeftMenuVC") as! LeftMenuVC
        
        let nvc = UINavigationController(rootViewController: tabBarControl)
        nvc.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        nvc.navigationBar.shadowImage = UIImage()
        nvc.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        // leftViewController.findCasteeVC = nvc
        
        let slideMenuController =  ExSlideMenuController(mainViewController:nvc, leftMenuViewController: leftViewController )
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
     //   slideMenuController.delegate = mainViewController
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        
        self.window?.layer.add(transition, forKey: nil)
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()
    
   

    
    }
    
    
    
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "PoliticsPlay")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

