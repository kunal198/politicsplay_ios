
//
//  CameraViewController.swift
//  Camera with AVFoundation
//
//  Created by Aaqib Hussain on 30/10/2016.
//  Copyright © 2016 Kode Snippets. All rights reserved.
//

import UIKit
import AVFoundation
import Photos




class CameraViewController: UIViewController, AVCaptureFileOutputRecordingDelegate {
    //Receive Key from Menu on which camera to start
    var keyFromMenu : String?
    //Holds the camera layer
    @IBOutlet weak var cameraOverlayView: UIView!
    //Camera Session
    var session: AVCaptureSession?
    //Capturing Image
    var stillImageOutput: AVCaptureStillImageOutput?
    //Capturing Video
    var videoOutput :  AVCaptureMovieFileOutput?
    //Shows preview
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    //Capturing Camera hardware
    var captureDevice:AVCaptureDevice! = nil
    //Capture button outlet for changing image of button
    @IBOutlet weak var captureOutlet: UIButton!
    //Switching to front/back camera
    var camera : Bool = true
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        if let title = self.keyFromMenu{
            
            self.title = title
            if title == "Capture Picture"{
                initiatePictureCamera()
            }
            else{
               
                initiateVideoCamera()
           
            }
            
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Camera Initialization functions
    func initiatePictureCamera(){
      
        print("Picture Camera is Running")
        session = AVCaptureSession()
        session!.sessionPreset = AVCaptureSessionPresetPhoto
        var input : AVCaptureDeviceInput?
        var error: NSError?
        
        if (camera == false) {
            let videoDevices = AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo)
            
            
            for device in videoDevices!{
                let device = device as! AVCaptureDevice
                if device.position == AVCaptureDevicePosition.front {
                    captureDevice = device
                    
                    break
                }
            }
        } else {
            captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        }
        
        
        do {
            input = try AVCaptureDeviceInput(device: captureDevice)
            
            if error == nil && session!.canAddInput(input) {
                session!.addInput(input)
                
                stillImageOutput = AVCaptureStillImageOutput()
                stillImageOutput!.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
                
                if session!.canAddOutput(stillImageOutput) {
                    session!.addOutput(stillImageOutput)
                    
                    videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session)
                    videoPreviewLayer!.videoGravity = AVLayerVideoGravityResizeAspectFill
                    videoPreviewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
                    videoPreviewLayer!.frame = cameraOverlayView.bounds
                    
                    cameraOverlayView.layer.addSublayer(videoPreviewLayer!)
                    
                    session!.startRunning()
                }
            }
            
        }
        catch let err as NSError {
            error = err
            input = nil
            print(error!.localizedDescription)
            
        }
        
    }
    func initiateVideoCamera(){
        
        
        
        
        print("Video Camera is Running")
       
        session = AVCaptureSession()
        session!.sessionPreset = AVCaptureSessionPresetHigh
        //var captureDevice:AVCaptureDevice! = nil
        var input : AVCaptureDeviceInput?
        var error: NSError?

        if (camera == false) {
            let videoDevices = AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo)
            
            
            for device in videoDevices!{
                let device = device as! AVCaptureDevice
                if device.position == AVCaptureDevicePosition.front {
                    captureDevice = device
                    break
                }
                
            }
        } else {
            captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        }
        
        
        do {
            
            input = try AVCaptureDeviceInput(device: captureDevice)
            
            if error == nil && session!.canAddInput(input) {
                session!.addInput(input)
                
                self.videoOutput = AVCaptureMovieFileOutput()
                
                if session!.canAddOutput(videoOutput) {
                    session!.addOutput(videoOutput)
                    
                    videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session)
                    videoPreviewLayer!.videoGravity = AVLayerVideoGravityResizeAspectFill
                    videoPreviewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
                    videoPreviewLayer!.frame = cameraOverlayView.bounds
                    cameraOverlayView.layer.addSublayer(videoPreviewLayer!)
                    
                    session!.startRunning()
                }
            }
            
        }
        catch let err as NSError {
            error = err
            input = nil
            print(error!.localizedDescription)
            
        }
    }
    //MARK: - Change Camera to Front or Back
    @IBAction func switchCamera(_ sender: UIBarButtonItem) {
  
        self.camera = !camera
        session!.stopRunning()
        videoPreviewLayer!.removeFromSuperlayer()
        initiatePictureCamera()
    
    }
    //MARK: - Capture Video or Picture
    @IBAction func capture(_ sender: AnyObject) {
        
        
        
        if let title = self.keyFromMenu{
        
        if title == "Capture Picture"{
            
        if let videoConnection = stillImageOutput!.connection(withMediaType: AVMediaTypeVideo) {
            
            stillImageOutput!.captureStillImageAsynchronously(from: videoConnection, completionHandler: { (sampleBuffer, error) -> Void in
                
                if sampleBuffer != nil {
                    
                    let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer)
                    let dataProvider = CGDataProvider(data: imageData as! CFData)
                    let cgImageRef = CGImage(jpegDataProviderSource: dataProvider!, decode: nil, shouldInterpolate: true, intent: CGColorRenderingIntent.defaultIntent)
                    let image = UIImage(cgImage: cgImageRef!, scale: 1.0, orientation: UIImageOrientation.right)
                    print(image)
                    UIImageWriteToSavedPhotosAlbum(image, self,#selector(CameraViewController.image(_:didFinishSavingWithError:contextInfo:)), nil)
                }
                
                
            })
            
            
            
            
        }
        }
            //If not title is Capture Video
        else{
            
            let fileName = "video.mp4";
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let filePath = documentsURL.appendingPathComponent(fileName)
            print(filePath)
            if self.videoOutput!.isRecording{
            //Change camera button for video recording
            self.videoOutput!.stopRecording()
                self.captureOutlet.setImage(UIImage(named: "capture"), for: UIControlState())
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
                
            return
            
            }
            else{
                
            //Change camera button for video recording
            self.captureOutlet.setImage(UIImage(named: "video_record"), for: UIControlState())
            //Start recording
            self.videoOutput!.startRecording(toOutputFileURL: filePath, recordingDelegate: self)
  

            }
            
            }
        }
        
    }
    
    //MARK: - Shows alert when image is saved
    func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafeRawPointer) {
        guard error == nil else {
            //Error saving image
            print(error)
            return
        }
        
        //Image saved successfully
        showAlert("Saved", message: "Image Saved to Photos")
        

    }
    //MARK: - Get completed video path
    func capture(_ captureOutput: AVCaptureFileOutput!, didFinishRecordingToOutputFileAt outputFileURL: URL!, fromConnections connections: [Any]!, error: Error!){
        
       captureOutput.maxRecordedDuration = CMTimeMake(30, 1)
//        let when = DispatchTime.now() + 2
//
//        DispatchQueue.main.asyncAfter(deadline: when){
//
//        captureOutput.stopRecording()
        
           // self.captureDevice?.stopRunning()
        
        
      //  }
        
        
        doVideoProcessing(outputFileURL)

        
    }
    
    func doVideoProcessing(_ outputPath:URL){
        
        
       print(outputPath)
        
        PHPhotoLibrary.shared().performChanges({
            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: outputPath)
            
            
            
        }) { (success, error) in
            
            
            
            if error == nil{
                
                print("Success:\(success)")
                DispatchQueue.main.async(execute: {
                    self.activityIndicator!.stopAnimating()
                    self.showAlert("Saved", message: "Video saved to Photos")
                })
                
                
            }
            else{
                
                DispatchQueue.main.async(execute: {
                    self.activityIndicator!.stopAnimating()

                    self.showAlert("Error", message: error!.localizedDescription)
                    
                })
                

                
            }
            
            
        }
        
    

        
   //  self.performSegue(withIdentifier: "recordedVideo", sender: nil)
//    let storyBoard : UIStoryboard = UIStoryboard(name:"Main", bundle: nil)
//       let playBackVC = storyBoard.instantiateViewController(withIdentifier: "PlayBackViewController") as! PlayBackViewController
//        playBackVC.urlString =  outputPath.relativePath
//      self.navigationController?.pushViewController(playBackVC, animated: true)
        
        
        
    }
   
    func showAlert(_ title: String, message : String)
    {
    
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let done = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(done)
        self.present(alert, animated: false, completion: nil)

    
    }

}
