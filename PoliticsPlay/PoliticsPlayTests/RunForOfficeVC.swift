//
//  RunForOfficeVC.swift
//  LayoutScreens
//
//  Created by Brst on 30/10/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class RunForOfficeVC: UIViewController,RunForOfficeSecVCDelegate,UITextFieldDelegate{
    @IBOutlet weak var scroll_View: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var btnYes: UIButton!
    @IBOutlet var btnNo: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //variables
    var arrSelectionParty = [String]()
    var tempIndex = Int()
    var selectedOffice = String()
    var subCategoryArr =  NSMutableArray()
    var selectedOfficeName = NSString()
    var userIdValueAgain  = String()
    var authTokenValue = String()
    var addedOffice = String()
    var idPassed = String()
    var otherAddedData = String()
    var selectedOfficeValue = Bool()
    var selectedPaperValue = Bool()
    var firstTextField  = UITextField()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrSelectionParty = ["President","U.S. Senatorial","Gubernatorial","U.S. House of Representative/Congressional","Lieutenant Gubernatorial","State Senatorial","State Assembly ","Mayoral ","City Council","County Supervisor","School Board","District Attorney","Other"]
       contentsBorder()
   
         //code to show the last selected option
//       if Singleton.sharedInstance.chooseOffice == ""{
//
//        }
//
//       else{
//
//        let indexOfOffice = arrSelectionParty.index(of: Singleton.sharedInstance.chooseOffice)
//        self.tempIndex = indexOfOffice!
//
//        }
        
      // code to show the last selected option
//        if  let selectedOption = UserDefaults.standard.value(forKey: "selectedKey") as? String{
//       if selectedOption == "1"{
//
//        btnYes.setImage(UIImage(named: "selected"), for: .normal)
//        btnNo.setImage(UIImage(named: "radio"), for: .normal)
//       }
//
//       else{
//
//        btnYes.setImage(UIImage(named: "radio"), for: .normal)
//        btnNo.setImage(UIImage(named: "selected"), for: .normal)
//
//        }
//
//    }
//     else{
//
//            btnYes.setImage(UIImage(named: "radio"), for: .normal)
//            btnNo.setImage(UIImage(named: "radio"), for: .normal)
//        }
        
        
        // Mark: delegate and notifications wrk
       //  data.delegate = self
        
        if UserDefaults.standard.value(forKey: "userId") != nil{
            
            self.userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as! String
            self.authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
           self.getStaticOfficesList(userId: userIdValueAgain,authToken: authTokenValue)
      
        }
        
        
       self.activityIndicator.isHidden = true
       tempIndex = -1
      
      
        // Mark: Validations
        selectedOfficeValue  = false
        selectedPaperValue = false
        
    
    }
    
    // Mark: notification wrk
    func submitData(imagePara: UIImage,linkText:String) {
        
       print(imagePara)
       print(linkText)
        if UserDefaults.standard.value(forKey: "userId") != nil{
            
            self.userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as! String
            print(self.userIdValueAgain)
            self.authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
             print(self.authTokenValue)
            self.submitOfficesList(userId: userIdValueAgain , authToken: authTokenValue , imageValue:imagePara,linkValue:linkText)
            
        }
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true
    } 
    
    func contentsBorder()
    {
        submitBtn.layer.cornerRadius = submitBtn.frame.height/2
    }
    override func viewWillLayoutSubviews() {
        scroll_View.contentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height+100)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // Mark: Get Static Run For Offices List
    func getStaticOfficesList(userId: String,authToken: String){
        
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        
        
        let parameters = ["user_id":userId,
                          "auth_token":authToken
                         
            ] as [String : Any]
        
        print(parameters)
        
        
        Alamofire.request("http://beta.brstdev.com/politicsplay/api/web/v1/users/getsubcat", method: .post, parameters:parameters, encoding: URLEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                DispatchQueue.main.async {
                    let json = JSON(data: response.data!)
                    let description = json["message"].string
                    if description == "subcategory found"{
                    let subCategoryArray = json["all_data"].arrayObject! as NSArray
                        self.subCategoryArr = (subCategoryArray.mutableCopy() as! NSMutableArray)
                    }
                    else if((response.error) != nil){
                        
                        self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                        
                    }
                    
                    DispatchQueue.main.async {
                        
                       self.activityIndicator.isHidden = true
                       self.activityIndicator.stopAnimating()
                        
                        
                    }
                    
                }
                
        }
        
    }
    
    
    // MARK: Text Field Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if textField == self.firstTextField {
            
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            
            if (newLength > 12) {
                
                self.alertViewMethod(titleStr: "", messageStr: "You cannot enter more than 12 characters" )
            }
            
            // checking for blank space
            let rawString: String = firstTextField.text!
            let whitespace = CharacterSet.whitespacesAndNewlines
            let range: NSRange = (rawString as NSString).rangeOfCharacter(from: whitespace)
            if range.location != NSNotFound {
                
                self.alertViewMethod(titleStr: "", messageStr: "You cannot enter blank space")
                
            }
            
            
            // checking that user only enters alphabets
            let aSet = NSCharacterSet(charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
            
        }
        
        
        return true

    
    
    }
    
    
    
    
    
// Mark: Submit Office list
    func submitOfficesList(userId:String,authToken:String,imageValue:UIImage,linkValue:String){
    
        if selectedOfficeValue == true{
            
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
            let statusValue = UserDefaults.standard.value(forKey:"selectedKey") as? String
            let parameters = ["user_id":userId,
                              "auth_token":authToken,
                              "officename":self.selectedOfficeName,
                              "status":statusValue!,
                              "custom_campaign":self.addedOffice,
                              "link":linkValue
                ] as [String : Any]
            
            print(parameters)
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                multipartFormData.append(UIImagePNGRepresentation(imageValue)!, withName:"image", fileName: "profilepic.jpg", mimeType: "image/jpg")
                for (key, value) in parameters {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            }, to:"http://beta.brstdev.com/politicsplay/api/web/v1/users/runforoffice")
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        //Print progress
                        //  print(progress)
                        
                    })
                    
                    upload.responseJSON { response in
                        //print response.result
                        print(response)
                        
                        DispatchQueue.main.async {
                            if response.result.value != nil {
                                let json = JSON(data: response.data!)
                                let data = json["data"].dictionaryObject! as NSDictionary
                                print(data)
                                self.idPassed = data.value(forKey: "id") as! String
                                print(self.idPassed)
                                UserDefaults.standard.set(self.idPassed, forKey: "runForOfficeKey")
                                UserDefaults.standard.synchronize()
                                let alertView = UIAlertController(title: "Congratulations you have submitted the data successfully", message:"" , preferredStyle: .alert)
                                let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                                   
                                    
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let tabBarControl = storyboard.instantiateViewController(withIdentifier:
                                        "TabBarViewController") as! TabBarViewController
                                    self.navigationController?.pushViewController(tabBarControl, animated: true)
                                    
                                })
                                alertView.addAction(action)
                                self.present(alertView, animated: true, completion: nil)
                                
                                
                                
                            } // dispatch end
                            
                        }
                        
                    }
                    
                    DispatchQueue.main.async{
                        
                        self.activityIndicator.isHidden = true
                        self.activityIndicator.stopAnimating()
                        self.tableView.reloadData()
                        
                    }
                    
                case .failure(let encodingError):
                    
                    //    self.alertViewMethod(titleStr: "", messageStr: encodingError.localizedDescription)
                    break
                    
                }
            }
    
        }
        
        else{
            

            let alertView = UIAlertController(title: "Congratulations you have submitted the data successfully", message:"" , preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
            
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let tabBarControl = storyboard.instantiateViewController(withIdentifier:
                    "TabBarViewController") as! TabBarViewController
                self.navigationController?.pushViewController(tabBarControl, animated: true)
                
            })
            alertView.addAction(action)
            self.present(alertView, animated: true, completion: nil)
            self.idPassed = ""
            UserDefaults.standard.set(self.idPassed, forKey: "runForOfficeKey")
            UserDefaults.standard.synchronize()
            
        }
        
    
    }
    
    
    
    
    
// MARK: Alert View
    func alertViewMethod(titleStr: String , messageStr: String){
        
        
        let alertView = UIAlertController(title: titleStr, message:messageStr , preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
            
            
        })
        alertView.addAction(action)
        self.present(alertView, animated: true, completion: nil)
    }
    

    
    
    @IBAction func toggleLeft(_ sender: Any) {
        
        slideMenuController()?.toggleLeft()
        
    }
    
   // push to run for office again
    
    
    @IBAction func pushToRunVC(_ sender: Any) {
        
        // Mark: If the user has selected office and also submitted paper then  it will shift to next screen
        if (selectedOfficeValue == true){
            
            if  (selectedPaperValue == true){
                
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let nextViewController = storyBoard.instantiateViewController(withIdentifier:
                    "RunForOfficeSecVC") as! RunForOfficeSecVC
                nextViewController.delegate = self
                self.navigationController?.pushViewController(nextViewController, animated: true)
                
            }
                
            else{
                
                self.alertViewMethod(titleStr: "Please tell us whether you have submit papers or not", messageStr: "")
                
                
            }
            
    
        }
      
            
    // Mark: If the user has not submitted any office then also we will move to next screen
            
        else{
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier:
                "RunForOfficeSecVC") as! RunForOfficeSecVC
            nextViewController.delegate = self
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
         }
        
      
   
    }
  
    @IBAction func closeAction(_ sender: Any) {
   
        if (tableView.isHidden){
            
          tableView.isHidden = false
        
        }
        else{
        
            tableView.isHidden = true
        }
    
    }
    
    
    @IBAction func clickYes(_ sender: Any) {
        
        selectedOffice = "1"
        selectedPaperValue = true
        btnYes.setImage(UIImage(named: "selected"), for: .normal)
        btnNo.setImage(UIImage(named: "radio"), for: .normal)
        UserDefaults.standard.set(selectedOffice, forKey: "selectedKey")
        UserDefaults.standard.synchronize()
    
    }
    
    
    
    @IBAction func clickNo(_ sender: Any) {
    
        selectedOffice = "0"
        selectedPaperValue = true
        btnYes.setImage(UIImage(named:"radio"), for: .normal)
        btnNo.setImage(UIImage(named:"selected"), for: .normal)
        UserDefaults.standard.set(selectedOffice, forKey:"selectedKey")
        UserDefaults.standard.synchronize()
    
   }
    
    
}


//MARK: SliderMenu Delegate Methods
extension RunForOfficeVC : SlideMenuControllerDelegate {
    
    func leftDidClose() {
        
        }
    
}

extension RunForOfficeVC:UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrSelectionParty.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"cellSelectionParty", for: indexPath ) as!
        RunForOffcCell
        cell.lblDescription.text = self.arrSelectionParty[indexPath.row]
        
        if indexPath.row == tempIndex{
            cell.btnPressed.setImage(UIImage(named:"selected"), for:UIControlState.normal)
            
        }else{
            
            cell.btnPressed.setImage(UIImage(named:"radio"), for:UIControlState.normal)
        }
        
        if indexPath.row % 2 == 0 {
            
            cell.contentView.backgroundColor = UIColor.white
        }
            
            else {
            cell.contentView.backgroundColor = UIColor.init(red: 231/255.0, green: 236/255.0, blue: 242/255.0, alpha: 1.0)
        }
        
        
        
        return cell
        
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       // tempIndex = 0
        
        selectedOfficeValue = true
        
        let dict = self.subCategoryArr[indexPath.row] as! NSDictionary
        self.selectedOfficeName = dict.value(forKey: "name") as! NSString
        print(self.selectedOfficeName)
        
        let lastRowIndex = tableView.numberOfRows(inSection: tableView.numberOfSections-1)

        if (indexPath.row == lastRowIndex - 1) {
            print("last row selected")
            let alertController = UIAlertController(title: "Add New Name", message: "", preferredStyle: UIAlertControllerStyle.alert)
            let saveAction = UIAlertAction(title: "Save", style: UIAlertActionStyle.default, handler: {
                alert -> Void in

                self.firstTextField = alertController.textFields![0] as UITextField!
                self.arrSelectionParty.remove(at: self.subCategoryArr.count - 1)
                self.arrSelectionParty.append((self.firstTextField.text!))
                self.addedOffice = (self.firstTextField.text!)
                print(self.arrSelectionParty)
                 tableView.reloadData()

            })

            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: {
                (action : UIAlertAction!) -> Void in

            })

            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "Enter Office Name"
            }


            alertController.addAction(saveAction)
            alertController.addAction(cancelAction)

            self.present(alertController, animated: true, completion: nil)

        }

          tempIndex = indexPath.row
          tableView.reloadData()
        
        
    }
    

    
    
    
  

}
   



