 //
//  SignUpViewController.swift
//  PoliticsPlay
//
//  Created by Brst981 on 03/11/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit
import RSKImageCropper
import Alamofire
import SwiftyJSON
import AVFoundation
import Photos

class SignUpViewController: UIViewController,UITextFieldDelegate {
    
    
    
    //Outlets
    @IBOutlet var btnSignUp: UIButton!
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var txtFieldFName: UITextField!
    @IBOutlet var txtFieldLName: UITextField!
    @IBOutlet var txtFieldEmail: UITextField!
    @IBOutlet var txtFieldCreatePass: UITextField!
    @IBOutlet var txtFieldConformPass: UITextField!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    
    
    //Variables
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let imagePicker = UIImagePickerController()
    let uniquePhotoNameStr = String()
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        imageView.layer.cornerRadius = imageView.frame.height/2
        imageView.clipsToBounds = true

        
        customizeTF()
        customizeView()
        
        txtFieldFName.delegate = self as? UITextFieldDelegate
        txtFieldLName.delegate = self as? UITextFieldDelegate
        txtFieldEmail.delegate = self as? UITextFieldDelegate
        txtFieldCreatePass.delegate = self as? UITextFieldDelegate
        txtFieldConformPass.delegate = self as? UITextFieldDelegate
        
       
    // code for setting delegate of image picker
        self.imagePicker.delegate = self
        
        self.activityIndicator.isHidden = true
        
       
        // internet connection check
        if Reachability.isConnectedToNetwork() {
            
            print("Internet connection available")
        }
        else{
            
            alertViewMethod(titleStr: "", messageStr: "No Internet connection available")
            
        }
    
    
    }
    
    @IBAction func pushToHome(_ sender: Any) {
    
    
       // registrationMethod()
        
        
        if txtFieldEmail.text == "" || txtFieldFName.text == "" || txtFieldCreatePass.text == "" || txtFieldConformPass.text == ""{
            alertViewMethod(titleStr: "", messageStr: "Please fill all mandatory fields.")
        }else if !isValidEmail(testStr: txtFieldEmail.text!) {
            alertViewMethod(titleStr: "", messageStr: "Please enter the valid email.")
        }
        else if !isPasswordValid(testStr: txtFieldCreatePass.text!) {
            alertViewMethod(titleStr: "", messageStr: "Password must contain minimum 6 characters with at least 1 number and 1 special character(@$!%*?&) with no space.")
        }
        else if (txtFieldCreatePass.text != txtFieldConformPass.text) {
            alertViewMethod(titleStr: "", messageStr: "Password does not match with confirm password.")
        }
        else{
            
            registrationMethod()
        }
}
    
    
    @IBAction func popBack(_ sender: Any) {
    
        self.navigationController?.popViewController(animated: true)

    }
    
    
    override func viewWillLayoutSubviews() {
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height+100)
    }
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func customizeTF(){
        
        let fullStringEmail = NSMutableAttributedString()
        let fullStringFName = NSMutableAttributedString()
        let fullStringLName = NSMutableAttributedString()
        let fullStringCreatePass = NSMutableAttributedString()
        let fullStringConformPass = NSMutableAttributedString()

        let image1Attachment = NSTextAttachment()
        let image2Attachment = NSTextAttachment()
        let image3Attachment = NSTextAttachment()
        let image4Attachment = NSTextAttachment()
        let image5Attachment = NSTextAttachment()

        let image1 = UIImage(named: "user")
        let image2 = UIImage(named: "user")
        let image3 = UIImage(named: "message")
        let image4 = UIImage(named: "padlock")
        let image5 = UIImage(named: "padlock")



        
        image1Attachment.image = image1
        image2Attachment.image = image2
        image3Attachment.image = image3
        image4Attachment.image = image4
        image5Attachment.image = image5

        
        let font = txtFieldEmail.font!
        let mid = font.descender + font.capHeight
        image1Attachment.bounds = CGRect(x: 0, y: font.descender - image1!.size.height / 2 + mid + 2, width: image1!.size.width, height:image1!.size.height).integral
        image2Attachment.bounds = CGRect(x: 0, y: font.descender - image2!.size.height / 2 + mid + 2, width: image2!.size.width, height:image2!.size.height).integral
        image3Attachment.bounds = CGRect(x: 0, y: font.descender - image3!.size.height / 2 + mid + 2, width: image3!.size.width, height:image2!.size.height).integral
         image4Attachment.bounds = CGRect(x: 0, y: font.descender - image4!.size.height / 2 + mid + 2, width: image4!.size.width, height:image2!.size.height).integral
         image5Attachment.bounds = CGRect(x: 0, y: font.descender - image5!.size.height / 2 + mid + 2, width: image5!.size.width, height:image5!.size.height).integral
        
        
        let image1String = NSAttributedString(attachment: image1Attachment)
        let image2String = NSAttributedString(attachment: image2Attachment)
        let image3String = NSAttributedString(attachment: image3Attachment)
        let image4String = NSAttributedString(attachment: image4Attachment)
        let image5String = NSAttributedString(attachment: image5Attachment)

       
        fullStringFName .append(image1String)
        fullStringLName .append(image2String)
        fullStringEmail .append(image3String)
        fullStringCreatePass .append(image4String)
        fullStringConformPass .append(image5String)

        
        
        fullStringEmail.append(NSAttributedString(string: "  Email*"))
        fullStringFName.append(NSAttributedString(string: "  First Name*"))
        fullStringLName.append(NSAttributedString(string: "  Last Name(Optional)"))
       fullStringCreatePass.append(NSAttributedString(string: "  Create Password*"))
        fullStringConformPass.append(NSAttributedString(string: "  Confirm Password*"))

        
        
        txtFieldEmail.attributedPlaceholder = fullStringEmail
        txtFieldFName.attributedPlaceholder = fullStringFName
        txtFieldLName.attributedPlaceholder = fullStringLName
        txtFieldCreatePass.attributedPlaceholder =  fullStringCreatePass
        txtFieldConformPass.attributedPlaceholder = fullStringConformPass

    }

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        txtFieldFName.resignFirstResponder()
        txtFieldLName.resignFirstResponder()
        txtFieldCreatePass.resignFirstResponder()
        txtFieldConformPass.resignFirstResponder()
        txtFieldEmail.resignFirstResponder()
        return true;
  }
    
    
    
    
    func customizeView(){
        
        btnSignUp.layer.cornerRadius = btnSignUp.frame.height/2;
        btnSignUp.clipsToBounds = true
    }
    
    
    
    //MARK: CheckCameraAuthorization
    
    func checkCameraAuthorization() {
        
        let status: AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        if status == .authorized {
            // authorized            picker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: { _ in })
        } else if status == .denied {
            // denied
            if AVCaptureDevice.responds(to: #selector(AVCaptureDevice.requestAccess(forMediaType:completionHandler:))) {
                AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {(_ granted: Bool) -> Void in
                    // Will get here on both iOS 7 & 8 even though camera permissions weren't required
                    // until iOS 8. So for iOS 7 permission will always be granted.
                    print("DENIED")
                    if granted {
                        // Permission has been granted. Use dispatch_async for any UI updating
                        // code because this block may be executed in a thread.
                        
                        DispatchQueue.main.async(execute: {() -> Void in
//                            let picker = UIImagePickerController()
                            self.imagePicker.delegate = self
                            self.imagePicker.allowsEditing = true
                            self.imagePicker.sourceType = .camera
                            self.present(self.imagePicker, animated: true, completion: { _ in })
                        })
                        
                    }
                    else {
                        // Permission has been denied.
                        
                        
                        DispatchQueue.main.async(execute: {() -> Void in
                            self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
                        })
                        
                        
                    }
                })
            }
        } else if status == .restricted {
            // restricted
            DispatchQueue.main.async(execute: {() -> Void in
                self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
            })
        }
        else if status == .notDetermined {
            // not determined
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {(_ granted: Bool) -> Void in
                if granted {
                    // Access has been granted ..do something
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.imagePicker.delegate = self
                        self.imagePicker.allowsEditing = true
                        self.imagePicker.sourceType = .camera
                        self.present(self.imagePicker, animated: true, completion: { _ in })
                        
                    })
                    
                    
                }
                else {
                    // Access denied ..do something
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
                    })
                    
                }
            })
        }
    }
    
    
    
    func accessMethod(_ message: String) {
        let alertController = UIAlertController(title: "Not Authorized", message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "OK", style: .default, handler: nil)
        let Settings = UIAlertAction(title: "Settings", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        })
        alertController.addAction(Settings)
        alertController.addAction(cancel)
        present(alertController, animated: true, completion: { _ in })
    }
    
    
    
    
    
    //MARK: CheckPhotoAuthorization
    
    func checkPhotoAuthorization()  {
        let status: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        if status == .authorized {
        
            self.imagePicker.delegate = self
             self.imagePicker.allowsEditing = true
            self.imagePicker.sourceType = .photoLibrary
            present(self.imagePicker, animated: true, completion: { _ in })
        }
        else if status == .denied {
            DispatchQueue.main.async(execute: {() -> Void in
                self.accessMethod("Please go to Settings and enable the photos for this app to use this feature.")
            })
        } else if status == .notDetermined {
            // Access has not been determined.
            PHPhotoLibrary.requestAuthorization({(_ status: PHAuthorizationStatus) -> Void in
                if status == .authorized {
                    
                    DispatchQueue.main.async(execute: {() -> Void in
                        let picker = UIImagePickerController()
                        picker.delegate = self
                        picker.allowsEditing = true
                        picker.sourceType = .photoLibrary
                        self.present(picker, animated: true, completion: { _ in })
                    })
                    
                }
                else {
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.accessMethod("Please go to Settings and enable the photos for this app to use this feature.")
                    })
                }
            })
        }
        else if status == .restricted {
            // Restricted access - normally won't happen.
        }
        
        
    }

    // code for picking image from image gallery
    
    @IBAction func pickingImageFromGallery(_ sender: Any) {
    
    
//        imagePicker.allowsEditing = true
//        imagePicker.sourceType = .photoLibrary
//        imagePicker.cameraCaptureMode = .photo
//        imagePicker.modalPresentationStyle = .fullScreen
//        present(imagePicker, animated: true, completion: nil)
        
        
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let camera = UIAlertAction(title: "Camera", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.checkCameraAuthorization()
        })
        let gallery = UIAlertAction(title: "Gallery", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.checkPhotoAuthorization()
        })
        alertController.addAction(gallery)
        alertController.addAction(camera)
        alertController.addAction(cancel)
        present(alertController, animated: true, completion: { _ in })
        
   }
    
    
    //MARK: Resize Image
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0,y: 0,width: newWidth,height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
   
    // Upload Photo
    func registrationMethod()  {
        
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        
       print(Singleton.sharedInstance.deviceTokenStr)
        
        
    let parameters = ["firstname":txtFieldFName.text!,
                    "lastname":txtFieldLName.text!,
                    "email":txtFieldEmail.text!,
                    "password":txtFieldCreatePass.text!,
                    "device_token":"1234"
                    
                    ] as [String : Any]
        
        print(parameters)
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(UIImagePNGRepresentation(self.imageView.image!)!, withName: "profile_pic", fileName: "profilepic.jpg", mimeType: "image/jpg")
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:"http://beta.brstdev.com/politicsplay/api/web/v1/users/register")
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                    //  print(progress)
                    
                })
                
                upload.responseJSON { response in
                    //print response.result
                    print(response)
                    
                    if response.result.value != nil {
                        
                        DispatchQueue.main.async {
                           
                        let json = JSON(data: response.data! )
                        
                        if let descriptionStr = json["message"].string {
                            print(descriptionStr)
                        if descriptionStr == "Email is already taken"{
                        
                            self.alertViewMethod(titleStr: "", messageStr: "Email id already exist. Please use another.")
                            self.txtFieldEmail.text = ""
                       }
                            
                    else if  descriptionStr ==  "Password must contain minimum 6 characters"{
                         
                    self.alertViewMethod(titleStr: "", messageStr: descriptionStr)
                    self.txtFieldCreatePass.text = ""
                     self.txtFieldConformPass.text = ""
                }
                            
                else if  descriptionStr == "Email format is not valid"{
                
                            self.alertViewMethod(titleStr: "", messageStr: descriptionStr)
                            self.txtFieldEmail.text = ""
                            self.txtFieldEmail.text = ""
                }
                        
                            
                else if  descriptionStr ==  "Register Successfully"{
                    
                    let userId = String(describing: json["data"]["user_id"].int!)
                     Singleton.sharedInstance.userIdStr = userId
                    let authToken = String(describing: json["data"]["auth_token"].string!)
                    Singleton.sharedInstance.auth_token = authToken

                   
                let alertView = UIAlertController(title: "", message:"Congratulation! You have registered successfully. Please check your email id and verify your account." , preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                    
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    
                    let signViewController = storyBoard.instantiateViewController(withIdentifier:
                        "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(signViewController, animated: true)
                })
                alertView.addAction(action)
                self.present(alertView, animated: true, completion: nil)
                
                            
               // code for saving the email id
//                UserDefaults.standard.set(self.txtFieldEmail.text, forKey:"savedEmail")
//                UserDefaults.standard.synchronize()
                Singleton.sharedInstance.savedEmail = self.txtFieldEmail.text!
                print(Singleton.sharedInstance.savedEmail)
                            
         
                            
                 //  self.btnSignUp.isEnabled = false
                    self.txtFieldConformPass.text = ""
                    self.txtFieldCreatePass.text = ""
                    self.txtFieldEmail.text = ""
                    self.txtFieldFName.text = ""
                    self.txtFieldLName.text = ""
                            
              }
                if ((response.error) != nil){
                    let alertView = UIAlertController(title: "", message:(response.error?.localizedDescription)! , preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default, handler:nil)
                    alertView.addAction(action)
                  self.present(alertView, animated: true, completion: nil)}
                            
                            
                            
                            
                    
            }
        }
                        
                        DispatchQueue.main.async{
                            
                            self.activityIndicator.isHidden = true
                           self.activityIndicator.stopAnimating()
                            
                        }
    }
  }
                
            case .failure(let encodingError):
                
                self.alertViewMethod(titleStr: "", messageStr: encodingError.localizedDescription)
                break
                //print encodingError.description
                
            }
            
        }
    }

   // MARK: Text Field Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool { // return NO to not change text
        
        
        if textField == txtFieldFName {
            
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
    
            if (newLength > 12) {
                
                self.alertViewMethod(titleStr: "", messageStr: "You cannot enter more than 12 characters" )
            }
            
           // checking for blank space
            let rawString: String = txtFieldFName.text!
            let whitespace = CharacterSet.whitespacesAndNewlines
            let range: NSRange = (rawString as NSString).rangeOfCharacter(from: whitespace)
            if range.location != NSNotFound {
               
                self.alertViewMethod(titleStr: "", messageStr: "You cannot enter blank space")
            
            }
            
            
            // checking that user only enters alphabets
            let aSet = NSCharacterSet(charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
            
        }
        
        if textField == txtFieldLName {
            
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
             if (newLength > 12) {
                
                self.alertViewMethod(titleStr: "", messageStr: "You cannot enter more than 12 characters" )
        }
            
            
            
            // checking for blank space
            let rawString: String = txtFieldFName.text!
            let whitespace = CharacterSet.whitespacesAndNewlines
            let range: NSRange = (rawString as NSString).rangeOfCharacter(from: whitespace)
            if range.location != NSNotFound {
                
                self.alertViewMethod(titleStr: "", messageStr: "You cannot enter blank space")
            }
            
            
            // checking that user only enters alphabets
            let aSet = NSCharacterSet(charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
            
            }
        
        
        
        if textField == txtFieldCreatePass {
            
            // checking for blank space
            let rawString: String = txtFieldFName.text!
            let whitespace = CharacterSet.whitespacesAndNewlines
            let range: NSRange = (rawString as NSString).rangeOfCharacter(from: whitespace)
            if range.location != NSNotFound {
                
                self.alertViewMethod(titleStr: "", messageStr: "You cannot enter blank space")
            }
            
     }
        
        return true
        
    }
    
    // MARK: AlertView
 
    func alertViewMethod(titleStr:String, messageStr:String) {
    
      let alertView = UIAlertController(title: titleStr, message:messageStr , preferredStyle: .alert)
      let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
        
      })
      alertView.addAction(action)
      self.present(alertView, animated: true, completion: nil)
    
  }
 
 
 // MARK: Email Validation
    func isValidEmail(testStr:String) -> Bool {
    // print("validate calendar: \(testStr)")
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: testStr)
    
    }
    
   //Password Validation
    func isPasswordValid(testStr:String) -> Bool{
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{6,}")
        return passwordTest.evaluate(with: testStr)
  }
    
    
    
    
 
}

 extension SignUpViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if (info[UIImagePickerControllerEditedImage] != nil)
        {
            var chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage
            chosenImage = resizeImage(image: chosenImage, newWidth: 400)
            imageView.clipsToBounds = true
           // imageView.contentMode = .scaleAspectFill
            imageView.layer.cornerRadius = imageView.frame.size.height/2
            imageView.image = chosenImage
          

            
//           // code for compressing image
//            let compressData = UIImageJPEGRepresentation(chosenImage, 0.5) //max value is 1.0 and minimum is 0.0
//            let compressedImage = UIImage(data: compressData!)
//        }
        dismiss(animated: true, completion: nil)
       
        
        
    
    
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}

 }
