//
//  DonorHistoryVC.swift
//  LayoutScreens
//
//  Created by Brst on 31/10/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class DonorHistoryVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var table_View: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        table_View.showsVerticalScrollIndicator = false
        table_View.separatorStyle = .none
        // Do any additional setup after loading the view.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath ) as! DonorHistoryTableViewCell
       // cell.label.text = "$ 25 to Mike Davis for state senate on may 1st 2017"

        cell.selectionStyle = .none
        if indexPath.row % 2 == 0 {
            cell.cellContentView.backgroundColor = UIColor.white
        }
        else {
            
            cell.cellContentView.backgroundColor = UIColor (colorLiteralRed: 231/255, green: 235/255, blue: 240/255, alpha: 1.0)
        }
        
        return cell
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func toggleLeft(_ sender: Any) {
        
        slideMenuController()?.toggleLeft()
        
    }
    
    
    
    


}

//MARK: SliderMenu Delegate Methods
extension DonorHistoryVC : SlideMenuControllerDelegate {
    
    func leftDidClose() {
        
        
        
    }
    
}





