//
//  RunForOfficeSecVC.swift
//  LayoutScreens
//
//  Created by Brst on 30/10/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import Alamofire
import SwiftyJSON


protocol RunForOfficeSecVCDelegate:class{

    func submitData(imagePara:UIImage,linkText:String)

}

  class RunForOfficeSecVC: UIViewController,UITextFieldDelegate {
    
 //outlets
    @IBOutlet weak var linkTxt: UITextField!
    @IBOutlet weak var takePhotoBtn: UIButton!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    
    
    // Variables
    let imagePicker = UIImagePickerController()
    var officeName = String()
    var chosenImage = UIImage()
    var imageUpload = Bool()
    var linkUpload = Bool()
    weak var delegate: RunForOfficeSecVCDelegate?

    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
         contentsBorder()
        
        linkTxt.delegate = self as? UITextFieldDelegate

        
      self.imageUpload = false
      self.linkUpload = false
        
        
    }
    
    func contentsBorder()
    {
        linkTxt.layer.cornerRadius = 20
        linkTxt.layer.borderColor = UIColor .lightGray .cgColor
        linkTxt.layer.borderWidth = 1.0
        
        takePhotoBtn.layer.cornerRadius = takePhotoBtn.frame.height/2
        takePhotoBtn.layer.borderColor = UIColor .red .cgColor
        takePhotoBtn.layer.borderWidth = 1.0
        
        submitBtn.layer.cornerRadius = submitBtn.frame.height/2
   
        }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    @IBAction func popBack(_ sender: Any) {
    
        self.navigationController?.popViewController(animated: true)
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func choosePicker(_ sender: Any) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let camera = UIAlertAction(title: "Camera", style: .default, handler: {(_ action: UIAlertAction) -> Void in
          self.checkCameraAuthorization()
        })
        let gallery = UIAlertAction(title: "Gallery", style: .default, handler: {(_ action: UIAlertAction) -> Void in
         self.checkPhotoAuthorization()
        })
        alertController.addAction(gallery)
        alertController.addAction(camera)
        alertController.addAction(cancel)
        present(alertController, animated: true, completion: { _ in })
    }
    
    //MARK: CheckPhotoAuthorization
    
    func checkPhotoAuthorization()  {
        let status: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        if status == .authorized {
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = .photoLibrary
            present(imagePicker, animated: true, completion: { _ in })
        }
        else if status == .denied {
            DispatchQueue.main.async(execute: {() -> Void in
                self.accessMethod("Please go to Settings and enable the photos for this app to use this feature.")
            })
        } else if status == .notDetermined {
            // Access has not been determined.
            PHPhotoLibrary.requestAuthorization({(_ status: PHAuthorizationStatus) -> Void in
                if status == .authorized {
                    
                    DispatchQueue.main.async(execute: {() -> Void in
                        
                        self.imagePicker.delegate = self
                        self.imagePicker.allowsEditing = true
                        self.imagePicker.sourceType = .photoLibrary
                        self.present(self.imagePicker, animated: true, completion: { _ in })
                    })
                    
                }
                else {
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.accessMethod("Please go to Settings and enable the photos for this app to use this feature.")
                    })
                }
            })
        }
        else if status == .restricted {
            // Restricted access - normally won't happen.
        }
        
        
    }
    
    
    //MARK: CheckCameraAuthorization
    
    func checkCameraAuthorization() {
        
        let status: AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        if status == .authorized {
            // authorized            picker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: { _ in })
        } else if status == .denied {
            // denied
            if AVCaptureDevice.responds(to: #selector(AVCaptureDevice.requestAccess(forMediaType:completionHandler:))) {
                AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {(_ granted: Bool) -> Void in
                    // Will get here on both iOS 7 & 8 even though camera permissions weren't required
                    // until iOS 8. So for iOS 7 permission will always be granted.
                    print("DENIED")
                    if granted {
                        // Permission has been granted. Use dispatch_async for any UI updating
                        // code because this block may be executed in a thread.
                        
                        DispatchQueue.main.async(execute: {() -> Void in
                            let picker = UIImagePickerController()
                            picker.delegate = self
                            picker.allowsEditing = true
                            picker.sourceType = .camera
                            self.present(picker, animated: true, completion: { _ in })
                        })
                        
                    }
                    else {
                        // Permission has been denied.
                        
                        
                        DispatchQueue.main.async(execute: {() -> Void in
                            self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
                        })
                        
                        
                    }
                })
            }
        } else if status == .restricted {
            // restricted
            DispatchQueue.main.async(execute: {() -> Void in
                self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
            })
        }
        else if status == .notDetermined {
            // not determined
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {(_ granted: Bool) -> Void in
                if granted {
                    // Access has been granted ..do something
                    DispatchQueue.main.async(execute: {() -> Void in
                        let picker = UIImagePickerController()
                        picker.delegate = self
                        picker.allowsEditing = true
                        picker.sourceType = .camera
                        self.present(picker, animated: true, completion: { _ in })
                        
                    })
                    
                    
                }
                else {
                    // Access denied ..do something
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
                    })
                    
                }
            })
        }
    }
    
    func accessMethod(_ message: String) {
        let alertController = UIAlertController(title: "Not Authorized", message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "OK", style: .default, handler: nil)
        let Settings = UIAlertAction(title: "Settings", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        })
        alertController.addAction(Settings)
        alertController.addAction(cancel)
        present(alertController, animated: true, completion: { _ in })
    }
    
    // MARK: Submit action
    @IBAction func submitData(_ sender: Any) {
        
        
        if self.imageUpload == true || self.linkUpload == true{
            
            delegate?.submitData(imagePara:imgView.image!,linkText:self.linkTxt.text!)
            self.navigationController?.popViewController(animated: true)
            
        }
        else{
            
         self.alertViewMethod(titleStr: "You need to upload atleast one document" , messageStr: "")
            
        }
        
        
        
    }
    
    
     func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        self.linkUpload = true
         return true
        
    }
    
// MARK: Alert View
    func alertViewMethod(titleStr: String , messageStr: String){
        
        
        let alertView = UIAlertController(title: titleStr, message:messageStr , preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
        })
        alertView.addAction(action)
        self.present(alertView, animated: true, completion: nil)
    }
    
    
    
    
    
    
    
}

extension RunForOfficeSecVC:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if (info[UIImagePickerControllerEditedImage] != nil)
        {
            self.imageUpload = true
            self.chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage
            print(chosenImage)
          //  chosenImage = resizeImage(image: chosenImage, newWidth: 400)
             imgView.clipsToBounds = true
             imgView.contentMode = .scaleAspectFill
             imgView.layer.cornerRadius = imgView.frame.size.height/2
             imgView.image = chosenImage
        }
        dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
   
    }
    
    
    
    
}




