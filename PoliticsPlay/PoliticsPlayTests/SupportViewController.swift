//
//  SupportViewController.swift
//  LayoutScreens
//
//  Created by Brst on 02/11/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class SupportViewController: UIViewController {
    @IBOutlet weak var donateButton: UIButton!
    @IBOutlet weak var nominateButton: UIButton!
    @IBOutlet weak var endorseButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

       customizeView()
    
    
    
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true
        // changing tab bar images as per screen sizes
//
//        let screenSize: CGRect = UIScreen.main.bounds
//
//        let screenWidth = screenSize.width
//
//        if  screenWidth == 320 {
//            let tabBarBackground = UIImage(named: "tabimage1xnew")
//            UITabBar.appearance().selectionIndicatorImage = tabBarBackground
//            UITabBar.appearance().tintColor = UIColor.white
//           // self.tabBarController?.tabBar.barTintColor = UIColor (colorLiteralRed: 13/255, green: 55/255, blue: 127/255, alpha: 1.0)
//        }
//        if  screenWidth == 375 {
//            let tabBarBackground = UIImage(named: "tabimage2x")
//            UITabBar.appearance().selectionIndicatorImage = tabBarBackground
//            UITabBar.appearance().tintColor = UIColor.white
//           // self.tabBarController?.tabBar.barTintColor = UIColor (colorLiteralRed: 13/255, green: 55/255, blue: 127/255, alpha: 1.0)
//        }
//
//        if  screenWidth == 414 {
//            let tabBarBackground = UIImage(named: "tabimage3xnew")
//            UITabBar.appearance().selectionIndicatorImage = tabBarBackground
//            UITabBar.appearance().tintColor = UIColor.white
//           // self.tabBarController?.tabBar.barTintColor = UIColor (colorLiteralRed: 13/255, green: 55/255, blue: 127/255, alpha: 1.0)
//        }
        
        
       // self.tabBar.tintColor = UIColor.white
        
        
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func customizeView(){
        
        
        donateButton.layer.cornerRadius = donateButton.frame.height/2
        donateButton.clipsToBounds = true
        
        nominateButton.layer.cornerRadius = nominateButton.frame.height/2
        donateButton.clipsToBounds = true
        nominateButton.layer.borderColor = UIColor .red .cgColor
        nominateButton.layer.borderWidth = 1
        
        endorseButton.layer.cornerRadius = nominateButton.frame.height/2
        endorseButton.clipsToBounds = true
        endorseButton.layer.borderColor = UIColor .red .cgColor
        endorseButton.layer.borderWidth = 1
        
        nominateButton.layer.borderColor = UIColor.init(colorLiteralRed: 191/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0).cgColor
 
        
        endorseButton.layer.borderColor = UIColor.init(colorLiteralRed: 191/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0).cgColor

   
    
    }
    
    
    
    
    
    @IBAction func toggleLeft(_ sender: Any) {
        
        slideMenuController()?.toggleLeft()
        
    }
    
    
   //push to donate to candidate
    
    @IBAction func pushDonateScreen(_ sender: Any) {
   
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier:
                 "DonateVC") as! ViewController
        nextViewController.isSlide = false
            self.navigationController?.pushViewController(nextViewController, animated: true)
  }
    
    
    @IBAction func pushNominateScreen(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier:
                "NominateFirstVC") as! NominateFirstVC
        nextViewController.isFromSlide = false
      self.navigationController?.pushViewController(nextViewController, animated: true)

    }
    
    
    @IBAction func pushEndroseScreen(_ sender: Any) {
    
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier:
                   "EndorseVC") as! EndorseVC
        nextViewController.isFromSlide = false
        self.navigationController?.pushViewController(nextViewController, animated: true)
  }
    

}

//MARK: SliderMenu Delegate Methods
extension SupportViewController : SlideMenuControllerDelegate {
    
    func leftDidClose() {
        
        
        
    }
    
}




