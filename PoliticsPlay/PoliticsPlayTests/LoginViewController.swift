//
//  LoginViewController.swift
//  PoliticsPlay
//
//  Created by Brst981 on 26/10/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    //Outlets
    @IBOutlet var btnLinkedIn: UIButton!
    @IBOutlet var btnFacebook: UIButton!
    @IBOutlet var btnLogIn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        modifyingButtonLayout()
    
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
   }
    
    
    func modifyingButtonLayout(){
    
        btnLinkedIn.layer.cornerRadius = 10.0
        btnLinkedIn.clipsToBounds = true
        btnFacebook.layer.cornerRadius = 10.0
        btnFacebook.clipsToBounds = true
    }

    // Push to Sign up Screen
   
    @IBAction func pushToSignUpScreen(_ sender: Any) {
    
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SignUpVC") as? SignUpVC
        navigationController?.pushViewController(vc!,
                                                 animated: true)
    
    
    }
    
   //Push to Location Screen
    
    @IBAction func pushToLocationScreen(_ sender: Any) {
    
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MyLocationVC") as? MyLocationVC
        navigationController?.pushViewController(vc!,
                                                 animated: true)
    }
    
    
    
    
    
    



}
