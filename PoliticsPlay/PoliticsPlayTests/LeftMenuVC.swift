//
//  LeftMenuVC.swift
//  Unlock your 10
//
//  Created by brst on 10/9/17.
//  Copyright © 2017 The Brihaspati Infotech. All rights reserved.
//

import UIKit
import FBSDKLoginKit

enum LeftMenu: Int {
    
    
    case HomeVC
    case MyProfileVC
    case MyStoryVC
    case MyTrendingVC
    case InboxVC
    case EndroseVC
    case NominateVC
    case RunOfficeVC
    case CompeditionVC
    case SearchVC
    case SupportVC
    case DonationVC
    case PrivacyVC
    case TermsOfUseVC
    case ContactVC
    case NationalVC
    case LoginVC
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}


class LeftMenuVC: UIViewController ,LeftMenuProtocol {

//OUTLETS
    
    @IBOutlet var tableView: UITableView!
    
    
//VARIABLES

    var menus = ["Home","My Profile","Your Story","Trending","Messages","Endrose","Nominate","Run For Office","See Compedition","Search","Support Your Campaign","My Donation History","Privacy","Terms of Service","Contact Us","Logout"]
    var images = ["homenew","profile","story","trending","messageicon","endorse","nominating","office","competition","searchicon","handshake","story","privacy","notepad","phoneicon","logout"]
    var homeVC: UIViewController!
    var myProfileVC: UIViewController!
    var yourStoryVC: UIViewController!
    var trendingVC : UIViewController!
    var inboxVC :UIViewController!
    var endroseVC : UIViewController!
    var nominateVC: UIViewController!
    var runForOfficeVC: UIViewController!
    var compeditionVC: UIViewController!
    var searchVC: UIViewController!
    var supportCandidateVC: UIViewController!
    var donationHistoryVC: UIViewController!
    var privacyVC: UIViewController!
    var termsOfUseVC: UIViewController!
    var contactVC: UIViewController!
   // var nationalVC: UIViewController!
    var loginVC: UIViewController!
    var selectedIndex = 0
  override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
        // Do any additional setup after loading the view.
    }

    // MARK: CustomizeUI
    func customizeUI()  {
      let storyboard = UIStoryboard(name: "Main", bundle: nil)
     homeVC = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
    self.homeVC = UINavigationController(rootViewController: homeVC)
    
    myProfileVC = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
    self.myProfileVC = UINavigationController(rootViewController: myProfileVC)
   
    yourStoryVC = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
       self.yourStoryVC = UINavigationController(rootViewController: yourStoryVC)
      
    trendingVC = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
        self.trendingVC = UINavigationController(rootViewController: trendingVC)

      inboxVC = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
        self.inboxVC = UINavigationController(rootViewController: inboxVC)

      endroseVC = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
        self.endroseVC = UINavigationController(rootViewController: endroseVC)

        
        
     let nominateVC = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
     self.nominateVC = UINavigationController(rootViewController: nominateVC)
     let runForOfficeVC = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
     self.runForOfficeVC = UINavigationController(rootViewController: runForOfficeVC)
    compeditionVC = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
       self.compeditionVC = UINavigationController(rootViewController: compeditionVC)

      searchVC = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
      self.searchVC = UINavigationController(rootViewController: searchVC)
 
      supportCandidateVC = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
        self.supportCandidateVC = UINavigationController(rootViewController: supportCandidateVC)

      donationHistoryVC = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
        self.donationHistoryVC = UINavigationController(rootViewController: donationHistoryVC)

    privacyVC = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
        self.privacyVC = UINavigationController(rootViewController: privacyVC)

    termsOfUseVC = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
        self.termsOfUseVC = UINavigationController(rootViewController: termsOfUseVC)

    contactVC = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
        self.contactVC = UINavigationController(rootViewController: contactVC)

 //  nationalVC = storyboard.instantiateViewController(withIdentifier: "NationalNewsCycleSecVC") as! NationalNewsCycleSecVC
      //  self.nationalVC = UINavigationController(rootViewController: nationalVC)


  
        
    loginVC = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC

        
    }
 //MARK: Button Action
    @IBAction func btnCrossAction(_ sender: Any) {
        self.slideMenuController()?.closeLeft()
    }
    
    func changeViewController(_ menu: LeftMenu) {
        
         let storyboard = UIStoryboard(name: "Main", bundle: nil)
        switch menu {
            
        
        case .HomeVC:
        self.slideMenuController()?.changeMainViewController(self.homeVC, close: true)
        
        case .MyProfileVC:
        let  homeVC = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
          homeVC.selectedIndex1 = 4
        let nav = UINavigationController(rootViewController: homeVC)
        self.slideMenuController()?.changeMainViewController(nav, close: true)
        
        case .MyStoryVC:
        
        let  homeVC = storyboard.instantiateViewController(withIdentifier: "CameraOverlay") as! CameraOverlay
        let nav = UINavigationController(rootViewController: homeVC)
        self.slideMenuController()?.changeMainViewController(nav, close: true)
        
        case .MyTrendingVC:
            
            let  homeVC = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
            homeVC.selectedIndex1 = 3
            let nav = UINavigationController(rootViewController: homeVC)
             self.slideMenuController()?.changeMainViewController(nav, close: true)
        
        case .InboxVC:
            
            let  homeVC = storyboard.instantiateViewController(withIdentifier: "InboxVC") as! InboxVC
            let nav = UINavigationController(rootViewController: homeVC)
            self.slideMenuController()?.changeMainViewController(nav, close: true)
        
        case .EndroseVC:
            
            let  homeVC = storyboard.instantiateViewController(withIdentifier: "EndorseVC") as! EndorseVC
            let nav = UINavigationController(rootViewController: homeVC)
            self.slideMenuController()?.changeMainViewController(nav, close: true)
        
        case .NominateVC:
            
            let  homeVC = storyboard.instantiateViewController(withIdentifier: "NominateFirstVC") as! NominateFirstVC
            let nav = UINavigationController(rootViewController: homeVC)
            self.slideMenuController()?.changeMainViewController(nav, close: true)
        
        case .RunOfficeVC:
            
            let  homeVC = storyboard.instantiateViewController(withIdentifier: "RunForOfficeVC") as! RunForOfficeVC
            let nav = UINavigationController(rootViewController: homeVC)
            self.slideMenuController()?.changeMainViewController(nav, close: true)
        
        case .CompeditionVC:
            
            let  homeVC = storyboard.instantiateViewController(withIdentifier: "CompetitonVC") as! CompetitonVC
            let nav = UINavigationController(rootViewController: homeVC)
            
            self.slideMenuController()?.changeMainViewController(nav, close: true)
        
        case .SearchVC:
            
            let  homeVC = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
            homeVC.selectedIndex1 = 1
            let nav = UINavigationController(rootViewController: homeVC)
           self.slideMenuController()?.changeMainViewController(nav, close: true)
        
        case .SupportVC:
            
            let  homeVC = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
            homeVC.selectedIndex1 = 2
            let nav = UINavigationController(rootViewController: homeVC)
            self.slideMenuController()?.changeMainViewController(nav, close: true)
        
        case .DonationVC:
            
            let  homeVC = storyboard.instantiateViewController(withIdentifier: "DonateVC") as! ViewController
            let nav = UINavigationController(rootViewController: homeVC)
            self.slideMenuController()?.changeMainViewController(nav, close: true)
        
        case .PrivacyVC:
            
            let  homeVC = storyboard.instantiateViewController(withIdentifier: "PrivacyVC") as! PrivacyVC
            let nav = UINavigationController(rootViewController: homeVC)
            self.slideMenuController()?.changeMainViewController(nav, close: true)
        
        case .TermsOfUseVC:
            
            let  homeVC = storyboard.instantiateViewController(withIdentifier: "TerrmsOfUseVC") as! TerrmsOfUseVC
            let nav = UINavigationController(rootViewController: homeVC)
            self.slideMenuController()?.changeMainViewController(nav, close: true)
        
        case .ContactVC:
            
            let  homeVC = storyboard.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
            let nav = UINavigationController(rootViewController: homeVC)
            self.slideMenuController()?.changeMainViewController(nav, close: true)
    
        //  case .NationalVC:
       //    self.slideMenuController()?.changeMainViewController(self.nationalVC, close: true)
        
        case .LoginVC:
            
        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
            loginManager.logOut()
         let loginviewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let navigationController = UINavigationController(rootViewController: loginviewController)
            UIApplication.shared.keyWindow?.rootViewController = navigationController
        let prefs = UserDefaults.standard
        prefs.removeObject(forKey:"userId")
        UserDefaults.standard.removeObject(forKey:"authToken")
        UserDefaults.standard.synchronize()
            
            //self.slideMenuController()?.closeLeft()
       
        default:
            print("F. You failed")
            let loginManager: FBSDKLoginManager = FBSDKLoginManager()
            loginManager.logOut()
            let loginviewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            let navigationController = UINavigationController(rootViewController: loginviewController)
            UIApplication.shared.keyWindow?.rootViewController = navigationController
            //Singleton.sharedInstance.userIdStr = ""
            let prefs = UserDefaults.standard
            prefs.removeObject(forKey:"userId")
            UserDefaults.standard.removeObject(forKey:"authToken")
            UserDefaults.standard.synchronize()

            
            
            
            
        }
  
    
    
    }

  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension LeftMenuVC : UITableViewDelegate {

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = LeftMenu(rawValue: indexPath.row) {
            self.changeViewController(menu)
        }
        
      selectedIndex = indexPath.row
      self.tableView.reloadData()
    
    
    }
 
}


extension LeftMenuVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! AmountRaisedTableViewCell
       // let lblMenu = cell.viewWithTag(10) as! UILabel
        cell.lblLeftMenu.text = menus[indexPath.row]
      cell.imageLeftMenu.image = UIImage(named: images[indexPath.row] )
        
        
        if selectedIndex == indexPath.row {
            print(selectedIndex)
            cell.backgroundColor = UIColor.lightText
            cell.viewSeprator.layer.isHidden = true
        
        }else{
            cell.backgroundColor = UIColor.clear
            print(selectedIndex)
             cell.viewSeprator.layer.isHidden = false
        }
        
        
        return cell
   
    
    }
    
}

