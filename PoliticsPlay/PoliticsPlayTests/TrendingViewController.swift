
//
//  TrendingViewController.swift
//  LayoutScreens
//
//  Created by Brst on 01/11/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AVFoundation
import AVKit

class TrendingViewController: UIViewController,TTTAttributedLabelDelegate,DetailPostDelegate {
    func postData(userId: String, authToken: String, page: String) {
        
        print("hello")
    }
    
    
    
   
    
    @IBOutlet weak var table_View: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    // Variables
    var trendingArr = NSMutableArray()
    var type = String()
    var img_url = String()
    var likedStatus = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        table_View.showsVerticalScrollIndicator = false
        table_View.separatorStyle = .none
        
        
        // changing tab bar images as per screen sizes
        UITabBar.appearance().tintColor = UIColor.white
        self.tabBarController?.tabBar.barTintColor = UIColor (colorLiteralRed: 13/255, green: 55/255, blue: 127/255, alpha: 1.0)
        
        if UserDefaults.standard.value(forKey: "userId") != nil{
            
            let userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as? String
            let authTokenValue = UserDefaults.standard.value(forKey: "authToken") as? String
            //self.trendingArr.removeAllObjects()
            self.trendingList(userId: userIdValueAgain! , authToken: authTokenValue!)
        }
        
        
       
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        super.viewWillAppear(true)
        // Mark: For hitting the trending api
        if UserDefaults.standard.value(forKey: "userId") != nil{
            
            let userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as? String
            let authTokenValue = UserDefaults.standard.value(forKey: "authToken") as? String
            self.trendingArr.removeAllObjects()
            self.trendingList(userId: userIdValueAgain! , authToken: authTokenValue!)
        }
        
        
    }
    
    @IBAction func toggleLeft(_ sender: Any) {
        
        slideMenuController()?.toggleLeft()
    }

   // Mark: Trending List Api
        func trendingList(userId:String , authToken: String){
        
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
            
            let json = [
                    "user_id":userId,
                    "auth_token":authToken,
                    ] as [String : Any]
          print(json)
        
            Alamofire.request("http://beta.brstdev.com/politicsplay/api/web/v1/users/highfollowerslist", method: .post, parameters:json, encoding: URLEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                DispatchQueue.main.async {
                    let json = JSON(data: response.data!)
                    if((response.error) != nil){
                        
                        self.alertViewMethod(titleStr: "", messageStr: (response.error?.localizedDescription)!)
                        
                    }
                    let descripStr = json["message"].string
                    if descripStr == "Posts found"{
                        
                     let mergedArray = json["all_data"].arrayObject! as NSArray
                     print(mergedArray)
                     self.trendingArr = mergedArray.mutableCopy() as! NSMutableArray
                     print(self.trendingArr)
                        
                     
                    }
                   
                  
                    
                    DispatchQueue.main.async {
                        
                        self.activityIndicator.isHidden = true
                        self.activityIndicator.stopAnimating()
                        self.table_View.reloadData()
                        
                  }  // end of inner dispatch
                    
                }  // end of outer dispatch
                
        } // end of alamofire
        
    }
    
    // MARK: AlertView
    
    func alertViewMethod(titleStr:String, messageStr:String) {
        
        let alertView = UIAlertController(title: titleStr, message:messageStr , preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
            
        })
        alertView.addAction(action)
        self.present(alertView, animated: true, completion: nil)
        
    }
    

 }


// Mark: Table View Methods

extension TrendingViewController:UITableViewDelegate, UITableViewDataSource{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
   
        return self.trendingArr.count

    }
   
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
   
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")as! TrendingTableViewCell
        let dict = self.trendingArr[indexPath.row] as! NSDictionary
        print(dict)
        self.type = dict.value(forKey: "type") as! String
        print(type)
        let postDate = dict.value(forKey: "post_date") as! String
        let postTime = dict.value(forKey: "post_time") as! String
        let image = dict.value(forKey: "image") as! String
        let thumbnail = dict.value(forKey: "thumbnail") as! String
        let likeCount = dict.value(forKey: "likes") as! String
        let commentCount = dict.value(forKey: "comments") as! String
        let textDescription = dict.value(forKey: "description") as! String
        let userName = dict.value(forKey: "username") as! String
        let profilePic = dict.value(forKey: "profile_pic") as! String
        self.likedStatus = dict.value(forKey: "is_likedbyuser") as! String
        
        
        if self.likedStatus == "yes"{
         
            if let image = UIImage(named: "heartlike") {
                
                cell.showLikeBtn.setImage(image, for: UIControlState.normal)
            }
            
            
        }
       
        
        else{
            
            if let image = UIImage(named: "heartunlike") {
                
                cell.showLikeBtn.setImage(image, for: UIControlState.normal)
            }
            
        }
        cell.readMore.isHidden = true
        if self.type == "post"{
            
            if textDescription == ""{
                
                cell.descriptionTxt.text = "No description added"
//                if cell.descriptionTxt.numberOfLines > 3{
//
//                    cell.readMore.isHidden = false
//
//                }
//
//                else{
//
//                    cell.readMore.isHidden = true
//                }
          }
            else{
                
//                if cell.descriptionTxt.numberOfLines > 3{
//
//                    cell.readMore.isHidden = false
//                }
//
//                else{
//
//                    cell.readMore.isHidden = true
//                }
                
                
            cell.descriptionTxt.text = textDescription
            
            
            }
        }
            
        else{
            
            cell.descriptionTxt.text = "No description added"
//            if cell.descriptionTxt.numberOfLines > 3{
//
//                cell.readMore.isHidden = false
//
//            }
//
//            else{
//
//                cell.readMore.isHidden = true
//            }
            
        }
        
      

        
        
      //Mark: showing data on cell
        cell.timeText.text = postTime
        cell.dateLabel.text = postDate
        cell.likeCount.text = likeCount
        cell.commentCount.text = commentCount
        cell.userName.text = userName
        cell.userProfilePic.sd_setImage(with: URL(string:profilePic), placeholderImage: UIImage(named: "pro"))
        cell.userProfilePic.layer.cornerRadius = cell.userProfilePic.layer.frame.size.height/2
        cell.userProfilePic.clipsToBounds = true
        // Mark: for image count
        let imgType = NSURL(string :image)?.pathExtension
        if imgType == "jpg" || imgType == "png" {
            
            cell.showImage.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "noimage"))
            cell.thumbnailIcn.image = UIImage(named: "")
            cell.outerImgView.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "noimage"))
            
            
        }
        else if(imgType == "mp4") {
            
            cell.showImage.sd_setImage(with: URL(string: thumbnail), placeholderImage: UIImage(named: "noimage"))
            cell.outerImgView.sd_setImage(with: URL(string: thumbnail ), placeholderImage: UIImage(named: "noimage"))
            cell.thumbnailIcn.image = UIImage(named: "playbutton")
        }
            
            // Mark: Work when image is not coming from backend
      else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "trendingCellData") as! TrendingSecondCell
            if (dict != nil){
              if self.likedStatus == "yes"{
                    
                    if let image = UIImage(named: "heartlike") {
                        
                        cell.showLikeButton.setImage(image, for: UIControlState.normal)
                    }
                    
                    
                }
                    
                    
                else{
                    
                    if let image = UIImage(named: "heartunlike") {
                        
                        cell.showLikeButton.setImage(image, for: UIControlState.normal)
                    }
                    
                }
                cell.readMore.isHidden = true
                

                //Mark: Showing data on cell
                if self.type == "post"{
                    
                    if textDescription == ""{
                        
                       cell.descriptionTxt.text = "No description added"
//                        if cell.descriptionTxt.numberOfLines > 3{
//
//                            cell.readMore.isHidden = false
//
//                        }
//
//                        else{
//
//
//                            cell.readMore.isHidden = true
//
//
//                        }
                        
                        
                    }
                    else{
                       
                        cell.descriptionTxt.text = textDescription
//                        if cell.descriptionTxt.numberOfLines > 3{
//
//                         cell.readMore.isHidden = false
//
//                        }
//
//                        else{
//
//                            cell.readMore.isHidden = true
//                        }
                        
                    }
                }
                else{
                    
                    cell.descriptionTxt.text = "No description added"
//                    if cell.descriptionTxt.numberOfLines > 3{
//                        
//                        cell.readMore.isHidden = false
//                        
//                    }
//                        
//                    else{
//                        
//                        cell.readMore.isHidden = true
//                        
//                    }
                    
                }
                
                cell.dateLabel.text = postDate
                cell.timeText.text =  postTime
                cell.userName.text = userName
                cell.likeCount.text = likeCount
                cell.commentCount.text = commentCount
               cell.userProfilePic.sd_setImage(with: URL(string:profilePic), placeholderImage: UIImage(named: "pro"))
                cell.userProfilePic.layer.cornerRadius = cell.userProfilePic.layer.frame.size.height/2
                cell.userProfilePic.clipsToBounds = true
            }
            
            cell.cellView.layer.cornerRadius = 4
            cell.cellView.layer.shadowColor = UIColor.black.cgColor
            cell.cellView.layer.shadowOffset = CGSize(width: 1,height: 1)
            cell.cellView.layer.shadowOpacity = 0.5
            cell.cellView.layer.shadowRadius = 1.0
            cell.selectionStyle = UITableViewCellSelectionStyle.none
         //   cell.readMore.tag = indexPath.row
          //  cell.readMore.addTarget(self, action: #selector(pushToDetailScreen), for:UIControlEvents.touchUpInside)
            return cell
        }
        
        cell.selectionStyle = .none
        cell.cellView.layer.cornerRadius = 4
        cell.cellView.layer.shadowColor = UIColor.black.cgColor
        cell.cellView.layer.shadowOffset = CGSize(width: 1,height: 1)
        cell.cellView.layer.shadowOpacity = 0.5
        cell.cellView.layer.shadowRadius = 1.0
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.btnPopUp.tag = indexPath.row
        cell.btnPopUp.addTarget(self, action: #selector(showPopup), for: UIControlEvents.touchUpInside)
        return cell
    
    }
   
    
    
    // Mark: Show image and video
    func showPopup(_ sender: UIButton) {
        
        
        let dict = trendingArr[sender.tag] as! NSDictionary
        print(dict)
        let img_Url  = dict.value(forKey:"image") as! String
        let valueVideo = NSURL(string : img_Url)?.pathExtension
        if valueVideo == "mp4"{
            
            let videoURL = URL(string:img_Url)
            let player = AVPlayer(url: videoURL!)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.view.addSubview(playerViewController.view)
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
                
            }
            
        }
            
        else{
            
            let valueImage = NSURL(string : self.img_url)?.pathExtension
            if valueImage == "jpg"{
                
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let detailController = storyBoard.instantiateViewController(withIdentifier:
                    "DetailImageVC") as! DetailImageVC
                detailController.postDictData = self.trendingArr[sender.tag] as! NSDictionary
                self.navigationController?.pushViewController(detailController, animated: true)
                
            }
            
        }
        
    }
    // Height for row
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        let dict = self.trendingArr[indexPath.row] as! NSDictionary
        self.img_url  = dict.value(forKey:"image") as! String
        let imgType = NSURL(string :img_url)?.pathExtension
        if imgType == "jpg" || imgType == "png" {
            
            return 300.0
            
            
        }
        else if(imgType == "mp4") {
            
            return 300.0
            
            
        }
            
        else{
            
            return 130.0
        }
        
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
     
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let postDetailViewController = storyBoard.instantiateViewController(withIdentifier:
            "DetailPost") as! DetailPost
        postDetailViewController.postType = "Trending"
        postDetailViewController.postTrendingData = self.trendingArr[indexPath.row] as! NSDictionary
        let dictData  = self.trendingArr[indexPath.row] as! NSDictionary
        postDetailViewController.postIDDetail = dictData.value(forKey: "id") as! Int
        postDetailViewController.likedByMe = dictData.value(forKey: "is_likedbyuser") as! String
        postDetailViewController.delegate = self
        self.navigationController?.pushViewController(postDetailViewController, animated: true)
        
        
    }
    
    
    
    
    
    
    

}
//MARK: SliderMenu Delegate Methods
extension TrendingViewController : SlideMenuControllerDelegate {
    
    func leftDidClose() {
        
        
        
    }
    
}
