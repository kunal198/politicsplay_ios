//
//  NationalNewsCycleSecVC.swift
//  LayoutScreens
//
//  Created by Brst on 31/10/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class NationalNewsCycleSecVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var lblNationalCycle: UILabel!
    @IBOutlet weak var table_View: UITableView!
    override func viewDidLoad() {
    super.viewDidLoad()
     table_View.showsVerticalScrollIndicator = false
    // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
   
         return 20
   
    
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath ) as! NationalNewsCycleSecCell
  
        
        cell.layoutIfNeeded()
        
        cell.imageViewNational.frame.size.width = cell.imageViewNational.frame.size.height
        cell.cellContentView.layer.shadowColor = UIColor.black.cgColor
        cell.cellContentView.layer.cornerRadius = 4.0
        cell.cellContentView.layer.borderWidth = 1.0
        cell.cellContentView.layer.borderColor = UIColor.clear.cgColor
        cell.cellContentView.layer.masksToBounds = true
        cell.cellContentView.layer.shadowColor = UIColor.black.cgColor
        cell.cellContentView.layer.shadowOffset = CGSize(width: 1, height: 1.0)
        cell.cellContentView.layer.shadowRadius = 1.0
        cell.cellContentView.layer.shadowOpacity = 0.50
        cell.cellContentView.layer.masksToBounds = false
        cell.imageViewNational.layer.cornerRadius = cell.imageViewNational.frame.height/2
        cell.imageViewNational.clipsToBounds = true
        
        if indexPath.row == 1{
            
          // cell.lblNationalCycle.text = "Eli Broad donated $27,00 to the comittee to elect Hillary Clinton for president I am with her"
            //cell.imageViewNational.isHidden = true
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 120
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func toggleLeft(_ sender: Any) {
        
        slideMenuController()?.toggleLeft()
        
    }
    
  
    
    

}

//MARK: SliderMenu Delegate Methods
extension NationalNewsCycleSecVC : SlideMenuControllerDelegate {
    
    func leftDidClose() {
        
        
        
    }
    
}


