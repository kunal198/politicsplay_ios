
//
//  UserProfileVC.swift
//  LayoutScreens
//
//  Created by Brst on 01/11/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import Alamofire
import SwiftyJSON
import SDWebImage


 @objc protocol DetailPostDelegate:class{
    
    func postData(userId: String,authToken: String,page: String)
    @objc optional func trendingList(userId:String , authToken: String)
  //  @objc optional func postData(userId: String,authToken: String,page: String,completion: @escaping (_ result: Bool) -> Void)
    
    
    
}




class DetailPost: UIViewController, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate{
    
    // Outlets
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var table_View: UITableView!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var profileImg: UIImageView!
    @IBOutlet var postImg: UIImageView!
    @IBOutlet var postDescription: UITextView!
    @IBOutlet var lblLikes: UILabel!
    @IBOutlet var imgComment: UIImageView!
    @IBOutlet var lblComments: UILabel!
    @IBOutlet var imgLikes: UIImageView!
    @IBOutlet var sepratorViewUpwards: UIView!
    @IBOutlet var sepratorViewDownwards: UIView!
    @IBOutlet var btnClickable: UIButton!
    @IBOutlet var thumbnailIcon: UIImageView!
    @IBOutlet weak var txtFieldText: UITextField!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var addCommentTF: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var lblNoComments: UILabel!
     @IBOutlet weak var showLikes: UILabel!
     @IBOutlet weak var showImageLike: UIButton!
    
    
    //Variables
    var postArray = Array<Any>()
    var postDict = NSDictionary()
    var img_Url = String()
    var profile_Pic = String()
    var checkuserProfile = Bool()
    var userProfileDict = NSDictionary()
    var userProfilePic = String()
    var noOfLikes = String()
    var noOfComments = String()
    var postId = Int()
    var postIDDetail = Int()
    var userIdValueAgain = String()
    var authTokenValue = String()
    var moreScroll = Bool()
    var currentIndex =  1
    var commentsArray = NSMutableArray()
    var indexingDict = [String: Any]()
    var postType = String()
    var postHomeData = NSDictionary()
    var postTrendingData = NSDictionary()
    var homePostId = String()
    var despStr = String()
    var likes = String()
    var commentCount = String()
    var dataArray = NSMutableArray()
    var likeKeyStatus = String()
    var likeDesp = String()
    var noOfCommentDict = [String:Any]()
    var postTypeDetail = String()
    var lblLikesCount = Int()
    var delegate : DetailPostDelegate?
    var imagePassed = UIImage()
    var totalLikes = Int()
    var likedByMe = String()
    var CommentAdded = Bool()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
       // table_View.showsVerticalScrollIndicator = false
        // Do any additional setup after loading the view.
        
        // code for showing data as per user profile and my profile
     
        
        print(self.imagePassed)
        
       CommentAdded = false
        
        
        // Mark: Showing data as per different screen scenarios
        showData()

        
        
     // code for making img view round
        profileImg.clipsToBounds = true
        profileImg.contentMode = .scaleAspectFill
        profileImg.layer.cornerRadius = profileImg.frame.size.height/2

        NotificationCenter.default.addObserver(self, selector: #selector(DetailPost.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(DetailPost.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        
        // Mark: Comments through pagination
        if UserDefaults.standard.value(forKey: "userId") != nil{
            
           userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as! String
           authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
            postData(userId: userIdValueAgain , authToken: authTokenValue , page: "1", postID:postIDDetail){
                (result:
                Bool) in
                print("got back: \(result)")
            }
            
        
        }
        
        
        self.lblNoComments.isHidden = true
        
       // self.txtFieldText.isUserInteractionEnabled = false
        
       // self.txtFieldText.delegate = self
       self.addCommentTF.delegate = self
        
        
        // Mark: For like unlike scenario
       self.likeBtn.addTarget(self, action: #selector(btnLikeUnlikeMethod), for: UIControlEvents.touchUpInside)
     
        
        
        
    }
    
    
    func keyboardWillShow(notification:NSNotification) {
       
    //  self.view.frame.origin.y =  -15
        
    }
    
    func keyboardWillHide(notification:NSNotification) {
    
      
    //  self.view.frame.origin.y = 0
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        
     //   self.txtFieldText.resignFirstResponder()
        self.addCommentTF.resignFirstResponder()
        return true;
        
    }
   
    
    @IBAction func showVideo(_ sender: Any) {
   
    
        if checkuserProfile == true{
            
            let valueVideo = NSURL(string : self.img_Url)?.pathExtension
            if valueVideo == "mp4"{
                
                let videoURL = URL(string:self.img_Url)
                let player = AVPlayer(url: videoURL!)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.view.addSubview(playerViewController.view)
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                    
                }
                
            }
            
            
            let valueImage = NSURL(string : self.img_Url)?.pathExtension
            if valueImage == "jpg"{
                
                self.btnClickable.isEnabled = false
                
            }
            
    }
        
        else{
            
            
            let valueVideo = NSURL(string : self.img_Url)?.pathExtension
            if valueVideo == "mp4"{
                
                let videoURL = URL(string:self.img_Url)
                let player = AVPlayer(url: videoURL!)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.view.addSubview(playerViewController.view)
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                    
                }
                
            }
            
            
            let valueImage = NSURL(string : self.img_Url)?.pathExtension
            if valueImage == "jpg"{
                
                self.btnClickable.isEnabled = false
                
            }
            
            
        }
   }
    // Mark: Like / Unlike Scenario
    func btnLikeUnlikeMethod(_ sender: UIButton){
        
        
        
        self.authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
        print(self.authTokenValue)
        self.userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as! String
        print(self.userIdValueAgain)
 
        
        
        let json = ["user_id":self.userIdValueAgain,
                    "auth_token":self.authTokenValue,
                    "post_id":self.postIDDetail
            
            ] as [String : Any]
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/politicsplay/api/web/v1/posts/likeunlikepost", method: .post, parameters:                                           json, encoding: URLEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                
                if((response.error) != nil){
                    
                    let alertView = UIAlertController(title: "", message:(response.error?.localizedDescription)! , preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                        
                    })
                    alertView.addAction(action)
                    self.present(alertView, animated: true, completion: nil)
                    
                    return
                }
                
                
                let json = JSON(data: response.data!)
                let descriptionStr = json["action"].string
                print(descriptionStr!)
                var image:UIImage?
                
                if descriptionStr == "unliked"{
                    
                    self.totalLikes -= 1
                     image = UIImage(named: "heartunlike") as UIImage?
                    
                } else {
                    
                    
                    self.totalLikes += 1
                     image = UIImage(named: "heartlike") as UIImage?
                
                }
                
                DispatchQueue.main.async {
                    self.showImageLike.setImage(image, for: UIControlState.normal)
                    self.lblLikes.text = String(describing:self.totalLikes)
                }
                
        
        }
        
    }
    
// Mark:  to show the data
    func showData(){
        
        // Mark: Setting the data on home screen
        if self.postType == "Home"{
            
            
            print(likedByMe)
            
            
            if likedByMe == "yes" {

                let image = UIImage(named: "heartlike") as UIImage?
                self.showImageLike.setImage(image, for: UIControlState.normal)


            } else {

                let image = UIImage(named: "heartunlike") as UIImage?
                self.showImageLike.setImage(image, for: UIControlState.normal)

            }
            
            
            self.dateLabel.text = postHomeData.value(forKey: "post_date") as? String
            print(self.dateLabel.text!)
            self.timeLabel.text = postHomeData.value(forKey: "post_time") as?  String
            print(self.timeLabel.text!)
            let imageProfilePic = postHomeData.value(forKey: "profile_pic") as?  String
            let text = postHomeData.value(forKey: "description") as? String
            self.lblLikes.text = postHomeData.value(forKey: "post_likes") as? String
            self.lblComments.text = postHomeData.value(forKey: "post_comments") as? String
            if text == ""{
                
                self.postDescription.text = "No description added"
                
            }
            else{
                
                self.postDescription.text = postHomeData.value(forKey: "description") as? String
            }
            
            print(self.postDescription.text)
            self.img_Url = (postHomeData.value(forKey: "image") as? String)!
            let thumbnailImage = (postHomeData.value(forKey: "thumbnail") as? String)!
            
            // code for setting profile picture
            self.profileImg.sd_setImage(with: URL(string: imageProfilePic!), placeholderImage: UIImage(named: "man.png"))
            
            // checking if it is a video or image
            let valueImage = NSURL(string : self.img_Url)?.pathExtension
            if valueImage == "jpg" || valueImage == "png"{
                
                self.postImg.sd_setImage(with: URL(string: self.img_Url), placeholderImage: UIImage(named: "noimage"))
                
            }
            else if(valueImage == "mp4"){
                
                self.postImg.sd_setImage(with: URL(string: thumbnailImage), placeholderImage: UIImage(named: "noimage"))
                self.thumbnailIcon.image = UIImage(named: "playbutton")
            }
                
            else{
                
                self.postDescription.frame = CGRect.init(x: self.postDescription.frame.origin.x, y: self.postImg.frame.origin.y, width: self.postDescription.frame.size.width, height: self.postDescription.frame.size.height)

                
                
            }
            
            totalLikes = Int((self.postHomeData.value(forKey: "post_likes") as? String)!)!
            
          }
            
  // Mark: Setting the data on trending screen
        if self.postType == "Trending"{
            
            
            
            if likedByMe == "yes" {
                
                let image = UIImage(named: "heartlike") as UIImage?
                self.showImageLike.setImage(image, for: UIControlState.normal)
                
                
            } else {
                
                let image = UIImage(named: "heartunlike") as UIImage?
                self.showImageLike.setImage(image, for: UIControlState.normal)
                
            }
            
            
            print(postTrendingData)
            
            self.lblLikes.text = postTrendingData.value(forKey: "likes") as? String
            self.lblComments.text = postTrendingData.value(forKey: "comments") as? String
            self.dateLabel.text = postTrendingData.value(forKey: "post_date") as? String
            print(self.dateLabel.text!)
            self.timeLabel.text = postTrendingData.value(forKey: "post_time") as?  String
            print(self.timeLabel.text!)
            let imageProfilePic = postTrendingData.value(forKey: "profile_pic") as?  String
            let text = postTrendingData.value(forKey: "description") as? String
           
            
            if text == ""{
                
                self.postDescription.text = "No description added"
                
            }
            else{
                
                self.postDescription.text = postTrendingData.value(forKey: "description") as? String
            }
            
            print(self.postDescription.text)
            self.img_Url = (postTrendingData.value(forKey: "image") as? String)!
            let thumbnailImage = (postTrendingData.value(forKey: "thumbnail") as? String)!
            
            // code for setting profile picture
            self.profileImg.sd_setImage(with: URL(string: imageProfilePic!), placeholderImage: UIImage(named: "man.png"))
            
            // checking if it is a video or image
            let valueImage = NSURL(string : self.img_Url)?.pathExtension
            if valueImage == "jpg" || valueImage == "png"{
                
                self.postImg.sd_setImage(with: URL(string: self.img_Url), placeholderImage: UIImage(named: "noimage"))
                
            }
            else if(valueImage == "mp4"){
                
                self.postImg.sd_setImage(with: URL(string: thumbnailImage), placeholderImage: UIImage(named: "noimage"))
                self.thumbnailIcon.image = UIImage(named: "playbutton")
            }
                
            else{
                                self.postDescription.frame = CGRect.init(x: self.postDescription.frame.origin.x, y: self.postImg.frame.origin.y, width: self.postDescription.frame.size.width, height:self.postDescription.frame.size.height)
      }
            
            totalLikes = Int((self.postTrendingData.value(forKey: "likes") as? String)!)!
        }
    
            //Mark: For other user profile
         if checkuserProfile == true{
            
            
            
            if likedByMe == "yes" {
                
                let image = UIImage(named: "heartlike") as UIImage?
                self.showImageLike.setImage(image, for: UIControlState.normal)
                
                
            } else {
                
                let image = UIImage(named: "heartunlike") as UIImage?
                self.showImageLike.setImage(image, for: UIControlState.normal)
                
            }
            
            self.dateLabel.text = userProfileDict.value(forKey: "post_date") as? String
            print(self.dateLabel.text!)
            self.timeLabel.text = userProfileDict.value(forKey: "post_time") as?  String
            self.noOfLikes = (userProfileDict.value(forKey: "post_likes") as?  String)!
            self.lblLikes.text = self.noOfLikes

            if  self.noOfComments == "" {

             //   self.lblLikes.text = "0"
                self.lblComments.text = "0"

            }
            else{

                self.lblComments.text = self.noOfComments
             }
            let text = userProfileDict.value(forKey: "description") as? String
            if text == ""{
                
                self.postDescription.text = "No description added"
                
            }
            else{
                
                self.postDescription.text = userProfileDict.value(forKey: "description") as? String
            }
            
            print(self.postDescription.text)
            self.img_Url = (userProfileDict.value(forKey: "image") as? String)!
            let thumbnailImage = (userProfileDict.value(forKey: "thumbnail") as? String)!
            
            // code for setting profile picture
            let profileImgUrl = userProfilePic
            print(profileImgUrl)
            self.profileImg.sd_setImage(with: URL(string: profileImgUrl), placeholderImage: UIImage(named: "man.png"))
            
            
            // checking if it is a video or image
            let valueImage = NSURL(string : self.img_Url)?.pathExtension
            if valueImage == "jpg" || valueImage == "png"{
                
                self.postImg.sd_setImage(with: URL(string: self.img_Url), placeholderImage: UIImage(named: "noimage"))
                
            }
            else if(valueImage == "mp4"){
                
                self.postImg.sd_setImage(with: URL(string: thumbnailImage), placeholderImage: UIImage(named: "noimage"))
                self.thumbnailIcon.image = UIImage(named: "playbutton")
            }
                
            else{
                
                                self.postDescription.frame = CGRect.init(x: self.postDescription.frame.origin.x, y: self.postImg.frame.origin.y, width: self.postDescription.frame.size.width, height: self.postDescription.frame.size.height)
              
            }
            
            
            totalLikes = Int((self.userProfileDict.value(forKey: "post_likes") as? String)!)!

            
            
        }
        
        
        if self.postTypeDetail == "Profile"{
            
            
            if likedByMe == "yes" {
                
                let image = UIImage(named: "heartlike") as UIImage?
                self.showImageLike.setImage(image, for: UIControlState.normal)
                
                
            } else {
                
                let image = UIImage(named: "heartunlike") as UIImage?
                self.showImageLike.setImage(image, for: UIControlState.normal)
                
            }
            
                    self.dateLabel.text = postDict.value(forKey: "post_date") as? String
                    self.timeLabel.text = postDict.value(forKey: "post_time") as?  String
                    self.noOfLikes = (postDict.value(forKey: "post_likes") as?  String)!
                    self.lblLikes.text = self.noOfLikes
                    if   self.noOfComments == "" {

                        self.lblComments.text = "0"
                    }
                     else{
            
                            self.lblComments.text = self.noOfComments
            
                      }
            
                        let text = postDict.value(forKey: "description") as? String
            
                        if text == ""{
            
                            self.postDescription.text = "No description added"
            
                        }
                        else{
            
                            self.postDescription.text = postDict.value(forKey: "description") as? String
                        }
            
                        print(self.postDescription.text)
                        self.img_Url = (postDict.value(forKey: "image") as? String)!
                        let thumbnailImage = (postDict.value(forKey: "thumbnail") as? String)!
            
                        // code for setting profile picture
                        let profileImgUrl = profile_Pic
                        print(profileImgUrl)
                        self.profileImg.sd_setImage(with: URL(string: profileImgUrl), placeholderImage: UIImage(named: "man.png"))
            
            
                        // checking if it is a video or image
                        let valueImage = NSURL(string : self.img_Url)?.pathExtension
                        if valueImage == "jpg" || valueImage == "png"{
            
                            self.postImg.sd_setImage(with: URL(string: self.img_Url), placeholderImage: UIImage(named: "noimage"))
            
                        }
                        else if(valueImage == "mp4"){
            
                            self.postImg.sd_setImage(with: URL(string: thumbnailImage), placeholderImage: UIImage(named: "noimage"))
                            self.thumbnailIcon.image = UIImage(named: "playbutton")
                        }
            
                        else{
                            
                       self.postDescription.frame = CGRect.init(x: self.postDescription.frame.origin.x, y: self.postImg.frame.origin.y, width: self.postDescription.frame.size.width, height: self.postDescription.frame.size.height)

            
                        }
            totalLikes = Int((self.postDict.value(forKey: "post_likes") as? String)!)!
        
        
        }
        
    
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        if self.despStr == "comments not found"{
          
          self.lblNoComments.isHidden = false
          //self.table_View.isHidden = true
          return 0

        }
        
        else{
            
            self.lblNoComments.isHidden = true
            return commentsArray.count
        }
        



       
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)as! UserProfileTableViewCell
            let dict = commentsArray[indexPath.row] as! NSDictionary
            let dictNew = self.noOfCommentDict as NSDictionary
            print(dictNew)
            self.noOfLikes = dictNew.value(forKey: "post_likes") as! String
            self.noOfComments = dictNew.value(forKey: "post_comments") as! String
          if self.noOfComments == "" {
            
           //  self.lblLikes.text = "0"
             self.lblComments.text = "0"
            
          }
          else{
            
          //  self.lblLikes.text = self.noOfLikes
            self.lblComments.text = self.noOfComments
         }
            let nameText = dict.value(forKey: "username") as! String
            let imageUrl = dict.value(forKey: "profile_pic") as! String
            let commentText = dict.value(forKey: "commment_text") as! String
            cell.cellContentView.layer.masksToBounds = false
            cell.cellContentView.layer.shadowColor = UIColor.black.cgColor
            cell.cellContentView.layer.shadowOpacity = 0.5
            cell.cellContentView.layer.shadowOffset = CGSize(width: -1, height: 1)
            cell.cellContentView.layer.shadowRadius = 1
            cell.cellContentView.layer.shadowPath = UIBezierPath(rect: cell.cellContentView.bounds).cgPath
            cell.cellContentView.layer.shouldRasterize = true
            cell.cellContentView.layer.rasterizationScale = UIScreen.main.scale
            cell.cellImg.layer.cornerRadius = cell.cellImg.frame.height/2
            cell.cellImg.clipsToBounds = true
            cell.cellImageDown.layer.cornerRadius = cell.cellImageDown.frame.height/2
            cell.cellImageDown.clipsToBounds = true
            cell.selectionStyle = UITableViewCellSelectionStyle.none;
            // Mark: For displaying comment data
            cell.userName.text = nameText
            cell.cellImageDown.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "noimage"))
            cell.textFieldText.text = commentText
            return cell
            
      //  }
        
       }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func popView(_ sender: Any) {
        
        // Mark: for shifting back to the trending screen and updating the api for likes and comment scenario
        if self.postType == "Trending"{
            
            if UserDefaults.standard.value(forKey: "userId") != nil{
            
                let userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as? String
                let authTokenValue = UserDefaults.standard.value(forKey: "authToken") as? String
                delegate?.trendingList!(userId: userIdValueAgain! , authToken: authTokenValue!)
                self.navigationController?.popViewController(animated: true)

            }
            
            
        
        }
        // Mark: For updating the likes and comments in case of my profile
        if  self.postTypeDetail == "Profile"{
            
//            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//            let profileVC = storyBoard.instantiateViewController(withIdentifier:
//                "TabBarViewController") as! TabBarViewController
//            profileVC.selectedIndex1 = 4
//            self.navigationController?.pushViewController(profileVC, animated: true)
            self.navigationController?.popViewController(animated: true)
            
        }
        
        if self.postType == "Home"{
            
            self.navigationController?.popViewController(animated: true)
            
            
        }

        if checkuserProfile == true{
            
            self.navigationController?.popViewController(animated: true)
            
        }

    }
    
  
    // Mark: Listing of all comments through pagination
    func postData(userId: String,authToken: String,page: String,postID: Int,completion: @escaping (_ result: Bool) -> Void){
        
       DispatchQueue.main.async {
            
           self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
            
        }
        let json = ["page":page,
                    "user_id":userId,
                    "auth_token":authToken,
                    "post_id":postID] as [String : Any]
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/politicsplay/api/web/v1/posts/getpostdetails", method: .post, parameters:  json, encoding: URLEncoding.default)
            .responseJSON { response in
                
                print(response.result)   // result of response serialization
                print(response)
                if((response.error) != nil){
                    
                    let alertView = UIAlertController(title: "", message:(response.error?.localizedDescription)! , preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                        
                    })
                    alertView.addAction(action)
                    self.present(alertView, animated: true, completion: nil)
                    
                }
                DispatchQueue.main.async {
                    
                    let json = JSON(data: response.data!)
                    let data = json["data"].dictionaryObject
                    self.noOfCommentDict = json["data"]["post_data"].dictionaryObject!
                    print(self.noOfCommentDict)
                   
                    let commentsArray = json["data"]["comments_list"].arrayObject
                    self.despStr = json["message"].string!
                     if ((data) != nil){

                        self.commentsArray.addObjects(from: commentsArray!)
                       // print(self.commentsArray)
                      //  self.commentCount = String(describing:self.commentsArray.count)
                        self.indexingDict = (json["data"]["pagination"].dictionaryObject)!
                        print(self.indexingDict)
                        let lastPageValue = self.indexingDict["page_count"] as! Int
                        print(lastPageValue)
                        if(self.currentIndex == lastPageValue ){
                            self.moreScroll = false
                        }
                        else{
                            self.currentIndex = self.currentIndex + 1
                            self.moreScroll = true
                        }
                    }
                    
                  
                    
                    DispatchQueue.main.async {
                       
                        self.activityIndicator.isHidden = true
                        self.activityIndicator.stopAnimating()
                         self.table_View.reloadData()
                   // Loading the last comment on last
                        if let commentsCount = commentsArray?.count
                        {
                            if commentsCount > 0 && self.CommentAdded == true
                            {
                                self.table_View.scrollToRow(at: IndexPath.init(row: commentsCount-1, section: 0) , at: UITableViewScrollPosition.bottom, animated: false)
                            }
                        }
                        
                        
                        

                    } // end of dispatch
                    
                    
                    
                } // end of one step ahead disaptch
                
                
                
        } // end of alamofire methd
        
    }
    
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (table_View.contentOffset.y >= (table_View.contentSize.height - table_View.frame.size.height) &&  self.moreScroll) {
            self.moreScroll = false
          
            let str = String(self.currentIndex)
            print(str)
         // postData(userId: userIdValueAgain , authToken: authTokenValue , page:str, postID:postIDDetail)
            
        }
        
    }
    
    
    // Mark: Add comment section
    @IBAction func btnAddComment(_ sender: Any){
   
      addComment()
    
    
    }
    
    
   // Mark: Add comment section
    func addComment(){
        
        DispatchQueue.main.async{
            
          self.activityIndicator.isHidden = false
          self.activityIndicator.startAnimating()

        }
        let userIdValue = UserDefaults.standard.value(forKey:"userId") as! String
        let authTokenValue = UserDefaults.standard.value(forKey:"authToken") as! String
         let json = ["user_id":userIdValue,
                    "auth_token":authTokenValue,
                    "post_id":String(describing:self.postIDDetail),
                    "commenttext":addCommentTF.text!
            ] as [String : Any]
        
        print(json)
        
        CommentAdded = true
        
        Alamofire.request("http://beta.brstdev.com/politicsplay/api/web/v1/posts/addpostcomment", method: .post, parameters:json, encoding: URLEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
            
                self.addCommentTF.text = ""
                DispatchQueue.main.async {
                  // code for showing post through pagination
                    if UserDefaults.standard.value(forKey: "userId") != nil{
                        
                        self.userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as! String
                        self.authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
                        self.commentsArray.removeAllObjects()
//                        self.postData(userId: self.userIdValueAgain, authToken:self.authTokenValue , page:"1", postID:self.postIDDetail)
                        self.postData(userId: self.userIdValueAgain , authToken: self.authTokenValue , page: "1", postID:self.postIDDetail){
                            (result:
                            Bool) in
                            print("got back: \(result)")
                            
                            
                        }
                        
                    }
                    
                   // self.activityIndicator.isHidden = false
                    //self.activityIndicator.startAnimating()
                    
                    
                }
                
                
                
                
        }
        
    }
    


}
