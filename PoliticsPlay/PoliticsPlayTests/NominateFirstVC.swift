//
//  NominateFirstVC.swift
//  LayoutScreens
//
//  Created by Brst on 30/10/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class NominateFirstVC: UIViewController,UITextFieldDelegate {
    
    
    
    // outlets
    @IBOutlet weak var serByNameTxt: UITextField!
    @IBOutlet weak var whichOfficeTxt: UITextField!
    @IBOutlet weak var nominateBtn: UIButton!
    var isFromSlide = true
    @IBOutlet var sideBtn: UIButton!
    @IBOutlet var viewName: UIView!
    @IBOutlet var viewOffice: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contentsBorder()
        
        
        // code for change the image
        if isFromSlide {
            sideBtn.setImage(UIImage(named: "tg-bar"), for: .normal)
            
        } else {
            sideBtn.setImage(UIImage(named: "back2"), for: .normal)
        }
        
       
        // code for setting delegate
        serByNameTxt.delegate = self
        whichOfficeTxt.delegate = self

        //tap gesture for dismissing the keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginVC.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    func contentsBorder()
    {
        nominateBtn.layer.cornerRadius = nominateBtn.frame.height/2
        nominateBtn.clipsToBounds = true
        
        viewName.layer.cornerRadius = nominateBtn.frame.height/2
        viewName.clipsToBounds = true
        viewName.layer.borderColor = UIColor .lightGray .cgColor
        viewName.layer.borderWidth = 1.0
       
        viewOffice.layer.cornerRadius = nominateBtn.frame.height/2
        viewOffice.clipsToBounds = true
        viewOffice.layer.borderColor = UIColor .lightGray .cgColor
        viewOffice.layer.borderWidth = 1.0

   
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func toggleLeft(_ sender: Any) {
  
    
        if isFromSlide {
            
            slideMenuController()?.toggleLeft()
            
            
            
            
        } else {
            
            self.navigationController?.popViewController(animated: true)
            
        }
            
    }
    
    
    
    @IBAction func pushToNominateScreen(_ sender: Any) {
        
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nominateVC = storyBoard.instantiateViewController(withIdentifier:
            "NominateSecVC") as! NominateSecVC
        self.navigationController?.pushViewController(nominateVC, animated: true)
        
}
    
    
    @IBAction func popBack(_ sender: Any) {
    
    self.navigationController?.popViewController(animated: true)
    
    }
    
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        serByNameTxt.resignFirstResponder()
        whichOfficeTxt.resignFirstResponder()
        return true;
    }
    
   
    //tap gesture
    func dismissKeyboard() {
      
        view.endEditing(true)
    }
    
    
   
    
}
//MARK: SliderMenu Delegate Methods
extension NominateFirstVC : SlideMenuControllerDelegate {
    
    func leftDidClose() {
        
        
        
    }
    
}





