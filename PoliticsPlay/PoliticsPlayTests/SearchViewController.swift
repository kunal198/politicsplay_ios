//
//  SearchViewController.swift
//  LayoutScreens
//
//  Created by Brst on 01/11/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import IQKeyboardManagerSwift


class SearchViewController: UIViewController,TTTAttributedLabelDelegate,UITextFieldDelegate,UICollectionViewDelegateFlowLayout,UserProfileVCDelegate   {
    
    // Outlets
    @IBOutlet weak var serach_View: UIView!
    @IBOutlet weak var ShowCollView: UIView!
    @IBOutlet weak var collection_View: UICollectionView!
    @IBOutlet weak var table_View: UITableView!
    @IBOutlet var txtFieldSearch: UITextField!
    @IBOutlet var activityIndicatr: UIActivityIndicatorView!
    
    @IBOutlet weak var searchCollectionView: UICollectionView!
    @IBOutlet weak var searchOuterView: UIView!
    // Variables
     let leftAndRightPaddings: CGFloat = 15.0
     let numberOfItemsPerRow: CGFloat = 3.0
     let heightAdjustment: CGFloat = 30.0
    var userIdValueAgain = String()
    var authTokenValue =  String()
    var searchArray = NSMutableArray()
    var searchRecentArray = NSMutableArray()

    var otherUserId = String()
    var dict = NSDictionary()
    var moreScroll = Bool()
    var currentIndex = Int()
    var searchData = NSMutableArray()
    var indexingDict = [String:Any]()
    var searchValue = Bool()
    @IBOutlet weak var searchLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

  
        // Mark: setting the outer collection view
        ShowCollView.layer.cornerRadius = 4
        ShowCollView.layer.shadowColor = UIColor.black.cgColor
        ShowCollView.layer.shadowOffset = CGSize(width: 1,height: 1)
        ShowCollView.layer.shadowOpacity = 0.5
        ShowCollView.layer.shadowRadius = 1.0
        
        searchOuterView.layer.cornerRadius = 4
        searchOuterView.layer.shadowColor = UIColor.black.cgColor
        searchOuterView.layer.shadowOffset = CGSize(width: 1,height: 1)
        searchOuterView.layer.shadowOpacity = 0.5
        searchOuterView.layer.shadowRadius = 1.0
        
        // Mark: Top view fixing
        txtFieldSearch.delegate = self as UITextFieldDelegate
      //  txtFieldSearch.addTarget(self, action: #selector(UITextFieldDelegate.textFieldShouldReturn(_:)), for: )
        serach_View.layer.cornerRadius = serach_View.frame.height/2
        serach_View.clipsToBounds = true
        
        self.activityIndicatr.isHidden = true
        
        // changing tab bar images as per screen sizes
       UITabBar.appearance().tintColor = UIColor.white
        self.tabBarController?.tabBar.barTintColor = UIColor (colorLiteralRed: 13/255, green: 55/255, blue: 127/255, alpha: 1.0)
        
         self.searchValue = false

        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        IQKeyboardManager.sharedManager().shouldShowToolbarPlaceholder = false
        
        self.searchLabel.isHidden = true
        
        
        dismissKeyboard()
        
        
        if UserDefaults.standard.value(forKey: "userId") != nil{
            
            userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as! String
            authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
            // self.searchArray.removeAllObjects()
            postData(userId: userIdValueAgain , authToken: authTokenValue) {
                (result:
                Bool) in
                print("got back: \(result)")
            }
            
        }
        
      }

 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //tap gesture
    func dismissKeyboard() {
        
        view.endEditing(true)
    }
    
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        // Mark: Recent Searches
        if UserDefaults.standard.value(forKey: "userId") != nil{
            
            userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as! String
            authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
           // self.searchArray.removeAllObjects()
            postData(userId: userIdValueAgain , authToken: authTokenValue) {
                (result:
                Bool) in
                print("got back: \(result)")
            }
            
        }
        
        dismissKeyboard()
       self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        
        if UserDefaults.standard.value(forKey: "userId") != nil{
            
            userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as! String
            authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
            searchMeth(userId: userIdValueAgain , authToken: authTokenValue)
            
        }
        
         textField.resignFirstResponder()
           return true;
   
    
    }
    
    
    @IBAction func toggleLeft(_ sender: Any) {
        
        slideMenuController()?.toggleLeft()
        
    }
    
    // Mark: Get searches thorugh pagination
    func postData(userId: String,authToken: String, completion: @escaping (_ result: Bool) -> Void){
        
       self.searchRecentArray.removeAllObjects()
        
        DispatchQueue.main.async {
            
            self.activityIndicatr.isHidden = false
            self.activityIndicatr.startAnimating()
            
        }
        let json = [
                    "user_id":userId,
                    "auth_token":authToken] as [String : Any]
        print(json)
        Alamofire.request("http://beta.brstdev.com/politicsplay/api/web/v1/users/getrecentsearches", method: .post, parameters:  json, encoding: URLEncoding.default)
            .responseJSON { response in
                
                print(response.result)   // result of response serialization
                print(response)
                if((response.error) != nil){
                    
                    let alertView = UIAlertController(title: "", message:(response.error?.localizedDescription)! , preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                        
                    })
                    alertView.addAction(action)
                    self.present(alertView, animated: true, completion: nil)
                    
                }
                DispatchQueue.main.async {
                    
                    let json = JSON(data: response.data!)
                    let data = json["data"].arrayObject
                    let descpStr = json["message"].string
                   if descpStr == "User id and authentication token not matched."{

                     let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                      let loginVC = storyBoard.instantiateViewController(withIdentifier:
                            "LoginVC") as! LoginVC
                     self.navigationController?.pushViewController(loginVC, animated: true)
                    
                    }
                    
                   else if descpStr == "searchshes not found"{
                        
                  //  self.alertView(title: "", messsage: "No searches found")
                    self.searchLabel.isHidden = false
                        
                        
                    }
                    
                    
           
                  else if ((data) != nil){

                    self.searchRecentArray.addObjects(from: data!)
                    print( self.searchRecentArray)
                        self.indexingDict = (json["pagination"].dictionaryObject)!
                        let lastPageValue = self.indexingDict["page_count"] as! Int
                        if(self.currentIndex == lastPageValue ){
                            self.moreScroll = false
                        }
                      else{
                            self.currentIndex = self.currentIndex + 1
                            self.moreScroll = true
                        }
                    completion(true)
                    self.reloadData()
                  }
                 
                  else{
                    
                    
                    let alertView = UIAlertController(title: "", message:(response.error?.localizedDescription)! , preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                        
                    })
                    alertView.addAction(action)
                    self.present(alertView, animated: true, completion: nil)
       }
                  // end of dispatch
                    
                    
                    
                } // end of one step ahead disaptch
                
                
                
        } // end of alamofire methd
        
    }
    
    
    func reloadData() {
        DispatchQueue.main.async {
            
            self.activityIndicatr.isHidden = true
            self.activityIndicatr.stopAnimating()
            self.collection_View.reloadData()
            
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
//        if (collection_View.contentOffset.x >= (collection_View.contentSize.width - collection_View.frame.size.width) && self.moreScroll) {
//            self.moreScroll = false
//            //let str = String(self.currentIndex)
//            postData(userId: userIdValueAgain , authToken: authTokenValue) {
//                (result: Bool) in
//                print("got back: \(result)")
//            }
//
//        }
        
    }
    
    // code for pushing to next view controller on click
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        
        print(label.tag)
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let userProfile = storyBoard.instantiateViewController(withIdentifier: "UserProfileVC") as! UserProfileVC
        userProfile.valuePerm = true
        self.navigationController?.pushViewController(userProfile, animated: true)
        
    }
    
   
    
    
// search action
    
    @IBAction func btnSearchAction(_ sender: Any) {
   
        if UserDefaults.standard.value(forKey: "userId") != nil{
            
            userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as! String
            authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
            searchMeth(userId: userIdValueAgain , authToken: authTokenValue)
        
        }
    
      self.txtFieldSearch.resignFirstResponder()
        
        
    
    }
    
    
    
    // api integration of search
    func searchMeth(userId: String, authToken: String){
        
        
        self.searchValue = true

        self.searchArray.removeAllObjects()
        
        
        self.activityIndicatr.isHidden = false
        self.activityIndicatr.startAnimating()
        
        
        let json = ["search_NameText":txtFieldSearch.text!,
                    "user_id":userId,
                    "auth_token":authToken
            ] as [String : Any]
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/politicsplay/api/web/v1/users/search", method: .post, parameters:                                           json, encoding: URLEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                self.txtFieldSearch.text = ""
                DispatchQueue.main.async {
                    let json = JSON(data: response.data! )
                    let jsonDict = json.dictionaryObject
                    let descripStr = json["message"].string
                    if descripStr == "Search Result"{
                    let data = jsonDict!["data"] as! NSArray
                    self.searchArray = data.mutableCopy() as! NSMutableArray
                    print(self.searchArray)
              }
               
                    else if (descripStr == "Userid and authentication token mismatched"){
                    
                        let alertView = UIAlertController(title: "", message: descripStr , preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                        
                            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                            let loginViewController = storyBoard.instantiateViewController(withIdentifier:
                                "LoginVC") as! LoginVC
                            self.navigationController?.pushViewController(loginViewController , animated: true)
                        
                        })
                        alertView.addAction(action)
                        self.present(alertView, animated: true, completion: nil)
                    }
                    
                    else if (descripStr == "Search Name Text cannot be empty."){
                        
                        self.alertView(title: "", messsage: descripStr!)}
                    
                    else if(descripStr == "No data found."){
                        
                      self.alertView(title: "", messsage: descripStr!)
                        
                        
                    }
                        
                   else if((response.error) != nil){

                        self.alertView(title: "", messsage: (response.error?.localizedDescription)!)}
                    
                    DispatchQueue.main.async {
                        
                        self.activityIndicatr.isHidden = true
                        self.activityIndicatr.stopAnimating()
                        self.searchCollectionView.reloadData()
                  }
                    
                }
                
        }
    
    }
    // code for alert view
    func alertView(title: String, messsage: String){
    
        let alertView = UIAlertController(title: title, message: messsage , preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
        })
        alertView.addAction(action)
        self.present(alertView, animated: true, completion: nil)
    }

}

extension SearchViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
       
        if collectionView == collection_View{
         
             return searchRecentArray.count
            
        }
        
        else{
            
            return searchArray.count
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
     
        
        if collection_View  == collectionView{
            
            
            self.searchLabel.isHidden = true

            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! SearchCollectionViewCell
            dict = searchRecentArray[indexPath.row] as! NSDictionary
            let imageFetched = dict.value(forKey: "profile_pic") as? String
            Singleton.sharedInstance.userFirstNameStr = (dict.value(forKey: "firstname") as? String)!
            Singleton.sharedInstance.userLastNameStr = (dict.value(forKey: "lastname") as? String)!
            cell.profileImage.frame.size.width = cell.profileImage.frame.size.height
            cell.profileImage.sd_setImage(with: URL(string: imageFetched!), placeholderImage: UIImage(named: "placeholder.png"))
            cell.profileImage.layer.cornerRadius = cell.profileImage.frame.height/2
            cell.profileImage.clipsToBounds = true
            cell.lblSearch.text = "\(Singleton.sharedInstance.userFirstNameStr) \(Singleton.sharedInstance.userLastNameStr)"

            return cell
        
        }
    
        else{
            
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "searchCell", for: indexPath) as! SecondSearchCell
            dict = searchArray[indexPath.row] as! NSDictionary
            let imageFetched = dict.value(forKey: "profile_pic") as? String
            Singleton.sharedInstance.userFirstNameStr = (dict.value(forKey: "firstname") as? String)!
            Singleton.sharedInstance.userLastNameStr = (dict.value(forKey: "lastname") as? String)!
           // cell.profileSearchImage.frame.size.width = cell.profileSearchImage.frame.size.height
            cell.profileSearchImage.sd_setImage(with: URL(string: imageFetched!), placeholderImage: UIImage(named: "placeholder.png"))
            cell.profileSearchImage.layer.cornerRadius = 23
            cell.profileSearchImage.clipsToBounds = true

            cell.lblSearchShow.text = "\(Singleton.sharedInstance.userFirstNameStr) \(Singleton.sharedInstance.userLastNameStr)"

            return cell
            
        }
        
//        if searchValue == true{
//
//
//
//        }
//
//        else{
//
//
//
//        }
       
       }
    // Mark: Did select
func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
    
    
    if searchCollectionView == collectionView{
        
        if UserDefaults.standard.value(forKey: "userId") != nil{
            
            userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as! String
            authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
           // postData(userId: userIdValueAgain , authToken: authTokenValue)
          //  postData(userId: userIdValueAgain , authToken: authTokenValue) {
              //  (result: Bool) in
               // print("got back: \(result)")
              //  if self.searchArray.count != 0 {
                  //  print(self.searchRecentArray)
                    let dict = self.searchArray[indexPath.row] as! NSDictionary
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileVC") as! UserProfileVC
                    print(dict)
                    vc.otherUserId = dict.value(forKey:"id") as! Int
                    vc.statusType = "yes"
                   vc.delegate = self
                    print(vc.otherUserId)
                    vc.valuePerm = true
                    self.navigationController?.pushViewController(vc,animated: true)
               // }
            }
           
        }
        
    else{
        
        let dict = searchRecentArray[indexPath.row] as! NSDictionary
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "UserProfileVC") as! UserProfileVC
        print(dict)
        vc.otherUserId = dict.value(forKey:"id") as! Int
        vc.statusType = "yes"
        print(vc.otherUserId)
        vc.valuePerm = true
        vc.delegate = self
     self.navigationController?.pushViewController(vc,animated: true)
        
        }
   
        
    
    }
//
    //MARK: flow layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

      //  if collectionView == collection_View{
            // setting the layout as per the design
            let bounds = UIScreen.main.bounds
            let width = (bounds.size.width - leftAndRightPaddings*(numberOfItemsPerRow+1)) / numberOfItemsPerRow
            let layout = collection_View.collectionViewLayout as! UICollectionViewFlowLayout
            layout.itemSize = CGSize(width: width,height: width)
            return CGSize(width: width, height: width)
            
        //}
        
    

       // else{
         
          // let boundsSearch = UIScreen.main.bounds
//            let widthSearch = (boundsSearch.size.width - leftAndRightPaddings*(numberOfItemsPerRow+1)) / numberOfItemsPerRow
//            let layoutSearch = searchCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
//            layoutSearch.itemSize = CGSize(width: widthSearch,height: widthSearch)
//            return CGSize(width: widthSearch, height: widthSearch)
            
      //  }
 



    }
    
    
    
   

}

//MARK: SliderMenu Delegate Methods
extension SearchViewController : SlideMenuControllerDelegate {
    
    func leftDidClose() {
        
        
        
    }
    
}








