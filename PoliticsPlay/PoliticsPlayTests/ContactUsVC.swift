//
//  ContactUsVC.swift
//  LayoutScreens
//
//  Created by Brst on 30/10/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class ContactUsVC: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var phoneTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var contactUsBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
       // contentsBorder()
        customizeTF()
   


        //tap gesture for dismissing the keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginVC.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
    }
    

    override func viewWillAppear(_ animated: Bool) {
 
      self.navigationController?.isNavigationBarHidden = true
        
        }

    
    
    
  

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    
    
    
    
    @IBAction func toggleLeft(_ sender: Any) {
        
        slideMenuController()?.toggleLeft()
        
    }
    
    
    //tap gesture
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    
    func customizeTF(){
        
        
        let fullStringName = NSMutableAttributedString()
        let fullStringPhone = NSMutableAttributedString()
        let fullStringEmail = NSMutableAttributedString()

        
        
        let image1Attachment = NSTextAttachment()
        let image2Attachment = NSTextAttachment()
        let image3Attachment = NSTextAttachment()

        
        
        
        let image = UIImage(named: "user")
        let image1 = UIImage(named: "phone")
        let image3 = UIImage(named: "message")

        
        image1Attachment.image = image
        image2Attachment.image = image1
        image3Attachment.image = image3

        
        let font = nameTxt.font!
        
        let mid = font.descender + font.capHeight
        image1Attachment.bounds = CGRect(x: 0, y: font.descender - image!.size.height / 2 + mid + 2, width: image!.size.width, height:image!.size.height).integral
        image2Attachment.bounds = CGRect(x: 0, y: font.descender - image!.size.height / 2 + mid + 2, width: image!.size.width, height:image!.size.height).integral
        
        
        let image1String = NSAttributedString(attachment: image1Attachment)
        let image2String = NSAttributedString(attachment: image2Attachment)
        let image3String = NSAttributedString(attachment: image3Attachment)

        
        fullStringName.append(image1String)
        fullStringPhone.append(image2String)
        fullStringEmail.append(image3String)

        fullStringName.append(NSAttributedString(string: "  Name"))
        fullStringPhone.append(NSAttributedString(string: "  Phone"))
        fullStringEmail.append(NSAttributedString(string: "  Email"))

        
        
        
        nameTxt.attributedPlaceholder = fullStringName
        phoneTxt.attributedPlaceholder = fullStringPhone
        emailTxt.attributedPlaceholder = fullStringEmail

        contactUsBtn.layer.cornerRadius = contactUsBtn.frame.height/2;
        contactUsBtn.clipsToBounds = true
   
    }
    
    
    
    
    
    
    
}



//MARK: SliderMenu Delegate Methods
extension ContactUsVC : SlideMenuControllerDelegate {
    
    func leftDidClose() {
        
        
        
    }
    
}
