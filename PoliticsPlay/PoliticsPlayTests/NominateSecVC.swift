//
//  NominateSecVC.swift
//  LayoutScreens
//
//  Created by Brst on 30/10/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class NominateSecVC: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var statementTxtView: UITextView!
    @IBOutlet weak var nominateBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        contentsBorder()
        
        
        //tap gesture for dismissing the keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginVC.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        
        
        // setting the text of placeholder
        statementTxtView.text = "Give a statement to support:"
        statementTxtView.textColor = UIColor.darkGray
        statementTxtView.delegate = self as? UITextViewDelegate

        
    }
    func contentsBorder()
    {
        statementTxtView.layer.cornerRadius = 10
        statementTxtView.layer.borderColor = UIColor .lightGray .cgColor
        statementTxtView.layer.borderWidth = 1.0
        
        nominateBtn.layer.cornerRadius = nominateBtn.frame.height/2
        nominateBtn.clipsToBounds = true
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
  self.navigationController?.isNavigationBarHidden = true
    
    }
    
    @IBAction func pushBack(_ sender: Any) {
    
    
    self.navigationController?.popViewController(animated: true)
    
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
    
       return true;
        
    }
    
    //tap gesture
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    
    //delegate methods of text view
    
     func textViewDidBeginEditing(_ textView: UITextView) {
        if statementTxtView.textColor == UIColor.lightGray {
            statementTxtView.text = nil
            statementTxtView.textColor = UIColor.black
        }
        
        statementTxtView.becomeFirstResponder()
        
        
    }
    
    
     func textViewDidEndEditing(_ textView: UITextView) {
        if statementTxtView.text.isEmpty {
            statementTxtView.text = "Placeholder"
            statementTxtView.textColor = UIColor.lightGray
      }
        
       statementTxtView.resignFirstResponder()
    }



}
