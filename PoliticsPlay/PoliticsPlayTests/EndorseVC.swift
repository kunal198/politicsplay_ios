//
//  EndorseVC.swift
//  LayoutScreens
//
//  Created by Brst on 30/10/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class EndorseVC: UIViewController,UITextFieldDelegate {
    
    
    // outlets
    @IBOutlet weak var serByNameTxt: UITextField!
    @IBOutlet weak var serByOfficeTxt: UITextField!
    @IBOutlet weak var endorseBtn: UIButton!
    @IBOutlet var sideBtn: UIButton!
    @IBOutlet var viewName: UIView!
    
    @IBOutlet var viewOffice: UIView!
    
    //variables
    var isFromSlide = true
     var padding = UIEdgeInsets()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contentsBorder()
        
        
        
        // code for changing image on button clck
        if isFromSlide {
            sideBtn.setImage(UIImage(named: "tg-bar"), for: .normal)

        } else {
            sideBtn.setImage(UIImage(named: "back2"), for: .normal)

        }
        
       // code for assigning delegate to text field
        serByNameTxt.delegate = self
        serByOfficeTxt.delegate = self
        
        serByNameTxt.setLeftPaddingPoints(5.0)
        serByOfficeTxt.setLeftPaddingPoints(5.0)

  }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
      self.navigationController?.isNavigationBarHidden = true
        
    }
    
    
    func contentsBorder()
    {      
        endorseBtn.layer.cornerRadius = endorseBtn.frame.height/2
     
        viewName.layer.cornerRadius = viewName.frame.height/2
        viewName.clipsToBounds = true
       
        viewOffice.layer.cornerRadius = viewOffice.frame.height/2
        viewOffice.clipsToBounds = true
        
   
    }
    
    
    
 
    //code for toggling left
    @IBAction func toggleLeft(_ sender: Any) {
        
        if isFromSlide {
            
            slideMenuController()?.toggleLeft()
            
        } else {
            
            self.navigationController?.popViewController(animated: true)
        }
        
}
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        
        serByNameTxt.resignFirstResponder()
        serByOfficeTxt.resignFirstResponder()
        return true;
    }
    

    
    
}

//MARK: SliderMenu Delegate Methods
extension EndorseVC : SlideMenuControllerDelegate {
    
    func leftDidClose() {
        
        
        
    }
    
}


extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        //self.leftViewMode = .always
}

}






