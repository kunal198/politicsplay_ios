//
//  CameraOverlay.swift
//  Ludi
//
//  Created by Mrinal Khullar on 07/12/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
//import MBProgressHUD
import PhotosUI
import SDWebImage
//import PhotoEditorSDK
import Alamofire
import AVFoundation
import SwiftyJSON

class CameraOverlay: UIViewController, UIVideoEditorControllerDelegate, UITextViewDelegate, UIGestureRecognizerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
   
   // Outlets
    @IBOutlet var crossBtnCamera: UIButton!
    @IBOutlet var galleryBtn: UIButton!
    @IBOutlet var captureBtn: UIButton!
    @IBOutlet var capturedImage: UIImageView!
    @IBOutlet var sendBtnCameraView: UIButton!
    @IBOutlet var downloadBtnCameraView: UIButton!
    @IBOutlet var playBtn: UIButton!
    @IBOutlet weak var timeCountLbl: UILabel!
    @IBOutlet var cameraView: UIView!
    @IBOutlet weak var flashButton: UIButton!
    @IBOutlet weak var cameraFlipButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    
    // Variables
    var cameraController = CameraController()
    var capturedTime = Float()
    var timer: Timer!
    var videoPlayer = AVPlayer()
    var progressCircle = CAShapeLayer()
    var media_uploadedFrom = String()
    var crossCount = Int()
    var videoPath = NSURL()
    var video_duration = Float()
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    var mediaType = String()
    var parentClass = UIViewController()
    var thumbnail = UIImage()
    var campValue = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customLayer(layer: sendBtnCameraView.layer)
        self.initCameraOverlay()
        
        self.crossCount = 1
        self.mediaType = "public.image"
        self.playBtn.isHidden = true
        self.timeCountLbl.isHidden = true
        
        self.checkCameraAuthorization()
        
        PHPhotoLibrary.requestAuthorization({(newStatus) in })
        try! AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: [])
        var prefersStatusBarHidden: Bool { return true }
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(self.longPress(guesture:)))
        self.captureBtn.addGestureRecognizer(longPress)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(DoubleTap(sender:)))
        tap.numberOfTapsRequired = 2
        self.cameraView.isUserInteractionEnabled = true
        self.cameraView.addGestureRecognizer(tap)
        self.cameraView.backgroundColor = UIColor.black
        
        self.capturedImage.backgroundColor = UIColor.black
        self.capturedImage.contentMode = .scaleAspectFit
        self.capturedImage.frame = CGRect(x: 0, y: UIScreen.main.bounds.height/10, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width)
        
        self.activityIndicator.isHidden = true
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.videoPlayer.pause()
        self.videoPlayer.isMuted = true
        NotificationCenter.default.removeObserver(self, name: .AVPlayerItemDidPlayToEndTime, object: self.videoPlayer)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    //MARK: Custom Camera functions
    
    func startTimer() {
        
        if self.timer == nil {
            capturedTime = 0
            
            self.timeCountLbl.text = "00:0\(Int(capturedTime))"
            self.timeCountLbl.isHidden = false
            
            self.timer = Timer.scheduledTimer(timeInterval: 0.25, target: self, selector: #selector(self.runTimedCode), userInfo: nil, repeats: true)
        }
        
    }
    
    func stopTimer() {
        print("1")
        self.timeCountLbl.isHidden = true
        
        if self.timer != nil {
            self.timer?.invalidate()
            self.timer = nil
            print("2")
        }
    }
    
    func runTimedCode() {
        print("timmerEnd")
        
        if capturedTime < 29.5{
            capturedTime += 0.25
            
            if capturedTime < 10 {
                self.timeCountLbl.text = "00:0\(Int(capturedTime) + 1)"
            }
            else{
                self.timeCountLbl.text = "00:\(Int(capturedTime) + 1)"
            }
            
            return
        }
        
        capturedTime += 0.25
        self.timeCountLbl.text = "00:\(Int(capturedTime))"
        self.cameraController.stopRecording()
       self.crossCount = 0
        self.progressCircle.removeFromSuperlayer()
        self.captureBtn.setImage(UIImage(named: "circle2"), for: .normal)
        self.stopTimer()
    }
    
    func configureCameraController() {
        
        cameraController.prepare {(error) in
            
            if let error = error {
                print(error)
            }
            
            try? self.cameraController.displayPreview(on: self.cameraView)
            
        }
        
    }
    
    func initCameraOverlay()  {
        
        self.playBtn.isHidden = true
      //  self.downloadBtnCameraView.isHidden = true
        self.flashButton.isHidden = false
     ///   self.editButton.isHidden = true
        self.cameraFlipButton.isHidden = false
        self.sendBtnCameraView.isHidden = true
        self.capturedImage.isHidden = true
        self.galleryBtn.isHidden = false
        self.captureBtn.isHidden = false
        self.progressCircle.removeFromSuperlayer()
        self.captureBtn.setImage(UIImage(named: "circle2"), for: .normal)
        
        self.videoPlayer.pause()
        self.videoPlayer.isMuted = true
        self.playBtn.isSelected = false
        
        self.capturedImage.layer.sublayers?.forEach {
            $0.removeFromSuperlayer()
        }
        NotificationCenter.default.removeObserver(self, name: .AVPlayerItemDidPlayToEndTime, object: self.videoPlayer)
        
    }
    
    func finalizeCameraOverlay()  {
        
        self.galleryBtn.isHidden = true
        self.capturedImage.isHidden = false
       // self.downloadBtnCameraView.isHidden = false
        self.flashButton.isHidden = true
        
        if self.mediaType == "public.image"{
           //self.editButton.isHidden = false
        }
        
        self.cameraFlipButton.isHidden = true
        self.sendBtnCameraView.isHidden = false
        self.captureBtn.isHidden = true
        
    }
    
    
    //MARK: Gesture Recognizers
    
    func DoubleTap(sender: UITapGestureRecognizer?) {
        
        do {
            try cameraController.switchCameras()
        }
        catch{
            
        }
        
    }
    
    
    func longPress(guesture: UILongPressGestureRecognizer) {
        
        if guesture.state == UIGestureRecognizerState.began {
            
            cameraController.startVideoRecord(completion: { (url, error) in
                
                if((error) != nil){
                    print(error as Any)
                }
                else{
                    self.mediaType = "public.movie"
                    
                    let asset : AVURLAsset = AVURLAsset(url: url!)
                    self.video_duration = (Float(asset.duration.seconds))
                    print("Selected video_duration-> \(self.video_duration)")
                    self.videoPath = asset.url as NSURL
                    self.playBtn.isHidden = false
                    
                    self.videoPlayer = AVPlayer(url: self.videoPath as URL)
                    let playerLayer = AVPlayerLayer(player: self.videoPlayer)
                    playerLayer.frame = self.capturedImage.bounds
                    self.videoPlayer.play()
                    self.videoPlayer.isMuted = false
                    
                    self.capturedImage.image = nil
                    self.capturedImage.backgroundColor = UIColor.black
                    self.capturedImage.layer.addSublayer(playerLayer)
                    self.capturedImage.layoutIfNeeded()
                    
                    NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.videoPlayer.currentItem, queue: nil, using: { (_) in
                        DispatchQueue.main.async {
                            self.videoPlayer.seek(to: kCMTimeZero)
                            self.videoPlayer.play()
                            self.videoPlayer.isMuted = false
                            self.playBtn.isSelected  = true
                        }
                    })
                    
                    self.media_uploadedFrom = "Camera"
                    self.finalizeCameraOverlay()
                    
                }
            })
            
            
            let lineWidth:CGFloat = 3
            let rectFofOval = CGRect(x: lineWidth / 2, y: lineWidth / 2 , width: captureBtn.bounds.width - lineWidth, height: captureBtn.bounds.height - lineWidth)
            let circlePath = UIBezierPath(ovalIn: rectFofOval)
            
            self.progressCircle.path = circlePath.cgPath
            self.progressCircle.strokeColor = UIColor.red.cgColor
            self.progressCircle.fillColor = UIColor.clear.cgColor
            self.progressCircle.lineWidth = 4.0
            self.progressCircle.frame = view.bounds
            self.progressCircle.lineCap = "round"
            
            self.captureBtn.layer.addSublayer(self.progressCircle)
            self.captureBtn.setImage(UIImage(named: "greenDot"), for: .normal)
            self.captureBtn.transform = CGAffineTransform(rotationAngle: -90)
            
            let animation = CABasicAnimation(keyPath: "strokeEnd")
            animation.fromValue = 0
            animation.toValue = (1)
            animation.duration = 32
            animation.repeatCount = 1
            animation.fillMode = kCAFillModeForwards
            animation.isRemovedOnCompletion = false
            animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            
            self.progressCircle.add(animation, forKey: nil)
            self.flashButton.isUserInteractionEnabled = false
            self.cameraFlipButton.isUserInteractionEnabled = false
           // self.crossBtnCamera.isUserInteractionEnabled = false
            self.galleryBtn.isUserInteractionEnabled = false
            
            self.startTimer()
            
        }
        
        if guesture.state == UIGestureRecognizerState.ended {
            
            self.cameraController.stopRecording()
            
           self.crossCount = 0
            self.progressCircle.removeFromSuperlayer()
            self.captureBtn.setImage(UIImage(named: "circle2"), for: .normal)
            self.stopTimer()
            
            self.flashButton.isUserInteractionEnabled = true
            self.cameraFlipButton.isUserInteractionEnabled = true
         //   self.crossBtnCamera.isUserInteractionEnabled = true
            self.galleryBtn.isUserInteractionEnabled = true
            
        }
        
        if guesture.state == UIGestureRecognizerState.failed {
            
            self.crossCount = 0
            self.progressCircle.removeFromSuperlayer()
            self.captureBtn.setImage(UIImage(named: "circle2"), for: .normal)
            self.stopTimer()
            
            self.flashButton.isUserInteractionEnabled = true
            self.cameraFlipButton.isUserInteractionEnabled = true
          //  self.crossBtnCamera.isUserInteractionEnabled = true
            self.galleryBtn.isUserInteractionEnabled = true
            
        }
        
        
    }
    
    
    
    @IBAction func cancelBtnFromKeyboardClicked(sender: Any) {
        view.endEditing(true)
    }
    
    
    // MARK: Camera Buttons
    
    @IBAction func turnflashOn(_ sender: Any) {
        
        if cameraController.flashMode == .on {
            cameraController.flashMode = .off
            self.flashButton.setImage(#imageLiteral(resourceName: "flashOff"), for: .normal)
        }
        else if cameraController.flashMode == .off{
            self.flashButton.setImage(#imageLiteral(resourceName: "flashAuto"), for: .normal)
            cameraController.flashMode = .auto
            
        }
        else{
            self.flashButton.setImage(#imageLiteral(resourceName: "flashIcon"), for: .normal)
            cameraController.flashMode = .on
        }
        
    }
    
    @IBAction func flipCamera(_ sender: Any) {
        
        do {
            try cameraController.switchCameras()
        }
        catch{
            
        }
        
    }
    
    
    @IBAction func imageCaptureAction(_ sender: Any) {
        
        
        cameraController.captureImage { (image, error) in
            
            self.mediaType = "public.image"
            self.capturedImage.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
            self.playBtn.isHidden = true
            self.capturedImage.image = image
            
            self.crossCount = 0
            self.media_uploadedFrom = "Camera"
            self.finalizeCameraOverlay()
            
            /**
             Present Photo Editor
             */
            
          //  self.presentPhotoEditorViewController(image: image!)
            
        }
        
    }
    
    @IBAction func openGallery(_ sender: Any) {
        
        self.checkPhotoAuthorization()
        self.crossCount = 0
    }
    
    
    @IBAction func saveImageInGallery(_ sender: Any) {
        
        //Singleton.albumName = "Ludi"
        
        if mediaType  == "public.image" {
          //  Singleton.sharedInstance.save(image: self.capturedImage.image!)
            
        }
        
        if mediaType == "public.movie" {
            self.videoPlayer.pause()
            self.videoPlayer.isMuted = true
            self.playBtn.isSelected = false
            
        //    Singleton.sharedInstance.saveVideo(videoPath: self.videoPath as URL)
        }
    }
    
    func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(ac, animated: true)
        } else {
            
          //  self.downloadBtnCameraView.isHidden = true
            
            let ac = UIAlertController(title: "Saved!", message: "Captured image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(ac, animated: true)
        }
    }
    
    @IBAction func cameraCrossAction(_ sender: Any) {
        
        crossCount = crossCount + 1

        if(crossCount == 2){
            dismiss(animated: true, completion: nil)
        }
        else{
            initCameraOverlay()
            dismiss(animated: true, completion: nil)
        }
        
        
      // dismiss(animated: true, completion: nil)
        //self.cameraView.isHidden = true
        
        
    }
    
    @IBAction func sendActionCameraView(_ sender: Any) {
        
        self.crossCount = 0
        self.videoPlayer.pause()
        self.videoPlayer.isMuted = true
        self.playBtn.isSelected = false
        
      //  DataManager.submissionPreUploadedMediaUrl = nil
      //  DataManager.campaignPreUploadedMediaUrl = nil
        

        if self.capturedImage.image != nil {
          let image = self.capturedImage.image!
        }

        
        let alertView = UIAlertController(title: "", message:"Do you want to add the story?", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
            
            
            self.submissionPreUploadProcess()
        
        })
        let action1 = UIAlertAction(title: "Cancel", style: .default, handler: { (alert) in
          
             self.initCameraOverlay()
            
         })
        
        alertView.addAction(action)
        alertView.addAction(action1)
        self.present(alertView, animated: true, completion: nil)
        
        
        
        
        
//
//        if self.parentClass is CreateCampaign {
//
//            var value = ["media_uploadedFrom": self.media_uploadedFrom, "mediaType":self.mediaType,  "videoUrl":self.videoPath] as [String : Any]
//
//            if self.capturedImage.image != nil{
//                value.updateValue(self.capturedImage.image! , forKey: "capturedImage")
//            }
//
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "mediaCapturedforCreateCampaign"), object: value)
//            self.campaignPreUploadProcess()
//            self.dismiss(animated:true, completion:nil)
//        }
//        else {
//
//            let typeLocal : String
//            if self.mediaType == "public.image"{
//                typeLocal = "Photos"
//            } else {
//                typeLocal = "Videos"
//            }
//
//            /**
//               If user is on Campaign Profile Screen
//            */
//
//            if shouldAutoSelectCampForSubmission == true && currentOpenedCampaign.value(forKey: "campaign_id") as! String != "" && currentOpenedCampaign.value(forKey: "submission_type") as! String != typeLocal{
//
//                let alert = UIAlertController(title: "Wrong submission type", message: "This campaign does not accept \(typeLocal). Go back to take another one or continue to submit this one to a different campaign.", preferredStyle: UIAlertControllerStyle.alert)
//
//                let OKAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
//
//                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                    let vc = storyboard.instantiateViewController(withIdentifier: "CreateSubmission") as! CreateSubmission
//
//                    vc.mediaType = self.mediaType
//                    if self.capturedImage.image != nil {
//                        vc.capturedImage = self.capturedImage.image!
//                    }
//
//                    vc.videoPath = self.videoPath
//                    vc.media_uploadedFrom =  self.media_uploadedFrom
//                    vc.video_duration = self.video_duration
//                    vc.IsWrongSubmission = true
//
//                    self.submissionPreUploadProcess()
//                    self.navigationController?.pushViewController(vc, animated: true)
//
//                })
//                alert.addAction(OKAction)
//                present(alert, animated: true)
//
//            }
//            else{
//
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let vc = storyboard.instantiateViewController(withIdentifier: "CreateSubmission") as! CreateSubmission
//
//                vc.mediaType = self.mediaType
//                if self.capturedImage.image != nil {
//                    vc.capturedImage = self.capturedImage.image!
//                }
//                vc.videoPath = self.videoPath
//                vc.media_uploadedFrom =  self.media_uploadedFrom
//                vc.video_duration = self.video_duration
//                vc.IsWrongSubmission = false
//
//                self.submissionPreUploadProcess()
//                self.navigationController?.pushViewController(vc, animated: true)
//
//            }
//
//        }
//
        
    }
    
    @IBAction func playVideo(_ sender: Any){
        
        if(playBtn.isSelected){
            self.videoPlayer.pause()
            self.videoPlayer.isMuted = true
            self.playBtn.isSelected = false
        }
        else{
            self.playBtn.isSelected = true
            self.videoPlayer.play()
            self.videoPlayer.isMuted = false
        }
        
    }
    
    
    //MARK: Edit Image Action
    
    @IBAction func editImage(_ sender: Any) {
        
        if self.capturedImage.image != nil {
          //  self.presentPhotoEditorViewController(image: self.capturedImage.image!)
        }
        
        
    }
    
    
    //MARK: Present Photo Editor
 /*
    func presentPhotoEditorViewController(image:UIImage) {
        
        let orient = image.imageOrientation
        print(orient)
        
        let configuration = Configuration() { builder in
            
            builder.configurePhotoEditorViewController { options in
                
                options.discardButtonConfigurationClosure  = { options in
                    
                    (options as UIButton).isHidden = true
                }
                
                options.applyButtonConfigurationClosure  = { options in
                    
                    (options as UIButton).setImage(nil, for: .normal)
                    (options as UIButton).setTitle("Done", for: .normal)
                    (options as UIButton).setTitleColor(UIColor.white, for: .normal)
                    
                }
                
                options.titleViewConfigurationClosure = { options in
                    (options as! UILabel).text = ""
                    
                }
                
                options.actionButtonConfigurationClosure = { cell, menuItem in
                    switch menuItem {
                    case .tool(let toolMenuItem):
                        if toolMenuItem.toolControllerClass == TransformToolController.self {
                            
                        }
                    default:
                        break
                    }
                    
                }
                
            }
            
        }
        
        if self.media_uploadedFrom == "Camera" && orient == UIImageOrientation.leftMirrored{
            
            let p = self.cameraView.takeSnapshot()
            let photoEditViewController = PhotoEditViewController(photo: p, configuration: configuration)
            photoEditViewController.delegate = self
            self.present(photoEditViewController, animated: true, completion: nil)
            
        }
        else{
            
            let photoEditViewController = PhotoEditViewController(photo: image, configuration: configuration)
            photoEditViewController.delegate = self
            self.present(photoEditViewController, animated: true, completion: nil)
            
        }
        
    }
    */
    
    //MARK: Camera Authorization
    
    
    func checkCameraAuthorization() {
        
        let status: AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        
        if status == .authorized {
            
            self.configureCameraController()
            
        } else if status == .denied {
            
            if AVCaptureDevice.responds(to: #selector(AVCaptureDevice.requestAccess(forMediaType:completionHandler:))) {
                AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {(_ granted: Bool) -> Void in
                    
                    if granted {
                        DispatchQueue.main.async(execute: {() -> Void in
                            self.configureCameraController()
                        })
                    }
                    else {
                        DispatchQueue.main.async(execute: {() -> Void in
                            self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
                        })
                        
                    }
                })
            }
        } else if status == .restricted {
            
            DispatchQueue.main.async(execute: {() -> Void in
                self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
            })
        }
        else if status == .notDetermined {
            
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {(_ granted: Bool) -> Void in
                if granted {
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.configureCameraController()
                        
                    })
                }
                else {
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
                    })
                    
                }
            })
        }
    }
    
    
    func accessMethod(_ message: String) {
        let alertController = UIAlertController(title: "Not Authorized", message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Ok", style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        let Settings = UIAlertAction(title: "Settings", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: ["":""], completionHandler: nil)
        })
        alertController.addAction(Settings)
        alertController.addAction(cancel)
        present(alertController, animated: true, completion: { _ in })
    }
    
    
    
    func checkPhotoAuthorization()  {
        let status: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        if status == .authorized {
            let localPicker = UIImagePickerController()
            localPicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            localPicker.videoMaximumDuration = 15.0
            localPicker.allowsEditing = true
            localPicker.delegate = self
            self.present(localPicker, animated: true, completion: { _ in })
            
        }
        else if status == .denied {
            DispatchQueue.main.async(execute: {() -> Void in
                self.accessMethodPhotos("Please go to Settings and enable the photos for this app to use this feature.")
            })
        } else if status == .notDetermined {
            PHPhotoLibrary.requestAuthorization({(_ status: PHAuthorizationStatus) -> Void in
                if status == .authorized {
                    
                    DispatchQueue.main.async(execute: {() -> Void in
                        let localPicker = UIImagePickerController()
                        localPicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
                        localPicker.videoMaximumDuration = 15.0
                        localPicker.allowsEditing = true
                        localPicker.delegate = self
                        self.present(localPicker, animated: true, completion: { _ in })
                    })
                    
                }
                else {
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.accessMethodPhotos("Please go to Settings and enable the photos for this app to use this feature.")
                    })
                }
            })
        }
        else if status == .restricted {
        }
        
        
    }
    
    func accessMethodPhotos(_ message: String) {
        let alertController = UIAlertController(title: "Not Authorized", message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Ok", style: .default) { (action) in
            
        }
        let Settings = UIAlertAction(title: "Settings", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: ["":""], completionHandler: nil)
        })
        alertController.addAction(Settings)
        alertController.addAction(cancel)
        present(alertController, animated: true, completion: { _ in })
    }
    
    //MARK: UIImagePickerControllerDelegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let mediaType = info[UIImagePickerControllerMediaType] as? String {
            
            self.mediaType = mediaType
            
            capturedImage.layer.sublayers?.forEach {
                $0.removeFromSuperlayer()
            }
            
            if mediaType  == "public.image" {
                print("Image Selected")
                
                if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
                    
                    self.playBtn.isHidden = true
                    self.capturedImage.image = image
                    
                }
            }
            
            if mediaType == "public.movie" {
                print("Video Selected")
                
                if let videoURL = info[UIImagePickerControllerMediaURL] as? NSURL {
                    
                    //  self.manageCroppingToSquare(filePath: videoURL as URL, completion: { (cropedVideoUrl) in
                    //     if cropedVideoUrl != nil{
                    //       }
                    //  })
                    
                    let asset : AVURLAsset = AVURLAsset(url: videoURL as URL)
                    self.video_duration = (Float(asset.duration.seconds))
                    print("Selected video_duration-> \(self.video_duration)")
                    
                    self.videoPath = asset.url as NSURL
                    self.playBtn.isHidden = false
                    
                    self.videoPlayer = AVPlayer(url: videoURL as URL)
                    let playerLayer = AVPlayerLayer(player: self.videoPlayer)
                    playerLayer.frame = self.capturedImage.bounds
                    self.videoPlayer.play()
                    self.videoPlayer.isMuted = false
                    
                    self.capturedImage.image = nil
                    self.capturedImage.backgroundColor = UIColor.black
                    self.capturedImage.layer.addSublayer(playerLayer)
                    
                    NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.videoPlayer.currentItem, queue: nil, using: { (_) in
                        DispatchQueue.main.async {
                            self.videoPlayer.seek(to: kCMTimeZero)
                            self.videoPlayer.play()
                            self.videoPlayer.isMuted = false
                            self.playBtn.isSelected  = true
                        }
                    })
                    
                }
            }
            
            self.finalizeCameraOverlay()
        }
        
        if(picker.sourceType == .photoLibrary){
            
            self.media_uploadedFrom = "Gallery"
            picker.dismiss(animated: false) {
                if self.mediaType  == "public.image" {
                    if self.capturedImage.image != nil{
                     //   self.presentPhotoEditorViewController(image: self.capturedImage.image!)
                    }
                }
            }
        }
        
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
   
        
        self.crossCount = 1
        picker.dismiss(animated: false, completion: nil)
        
    }
    
    
    //MARK: PhotoEdit ViewController Delegate.
   /*
    func photoEditViewController(_ photoEditViewController: PhotoEditViewController, didSave image: UIImage, and data: Data) {
        
        //  let pp =  photoEditViewController.view.pb_takeSnapshot(size: CGSize(width:  self.capturedImage.frame.width, height: self.capturedImage.frame.width))
        
        photoEditViewController.dismiss(animated: true) {
            
            let orient = image.imageOrientation
            print(orient)
            
        //  let image2 = UIImage(cgImage: image.cgImage!, scale: 1, orientation: UIImageOrientation.down)
            
            self.capturedImage.image = image
            let orient2 = self.capturedImage.image?.imageOrientation
            
        }
        
    }
    
    func photoEditViewControllerDidFailToGeneratePhoto(_ photoEditViewController: PhotoEditViewController) {
        
        photoEditViewController.dismiss(animated: true) {
            
        }
    }
    
    func photoEditViewControllerDidCancel(_ photoEditViewController: PhotoEditViewController) {
        
        photoEditViewController.dismiss(animated: true) {
            
        }
        
    }
 
 */
    
    //MARK: Upload Methods.
    
    
//    func campaignPreUploadProcess(){
//
//        var mimeType = String()
//        var fileName = String()
//        var imageData = Data()
//
//        if mediaType  == "public.movie" {
//            mimeType = "video/mp4"
//            fileName = "video.mp4"
//
//            do{
//                imageData =  try NSData(contentsOfFile: (self.videoPath.relativePath)!, options: NSData.ReadingOptions.alwaysMapped) as Data
//            }
//            catch{
//            }
//        }
//        else if mediaType  == "public.image"{
//            mimeType = "image/jpg"
//            fileName = "file.jpg"
//            imageData = UIImageJPEGRepresentation(self.capturedImage.image!,1)!
//
//        }
//
//
//        let parameters: Parameters =
//            ["auth_token":UserDefaults.standard.value(forKey:"authToken")!,
//             "user_id":UserDefaults.standard.value(forKey: "userId")!
//        ]
//
//        Alamofire.upload(multipartFormData: { multipartFormData in
//            multipartFormData.append(imageData, withName: "media",fileName:fileName ,mimeType:mimeType )
//            for (key, value) in parameters {
//                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
//            }
//        },to:"http://beta.brstdev.com/politicsplay/api/web/v1/users/add_story")
//        { (result) in
//            switch result {
//            case .success(let upload, _, _):
//
//                upload.uploadProgress(closure: { (progress) in
//                    print("Upload Progress: \(progress.fractionCompleted)")
//                })
//
//                upload.responseString(completionHandler: { (respo) in
//                    print(respo)
//                })
//
//                upload.responseJSON { response in
//
//                    print(response)
//                    if let json = response.result.value as? NSDictionary {
//
//                        if(json.value(forKey: "status") as! String == "200"){
//                          //  DataManager.campaignPreUploadedMediaUrl = json.value(forKey: "data") as? String
//                        self.alertView(title: "", message: "Story Added Successfully")
//
//                        }
//                        else{
//                          //  DataManager.campaignPreUploadedMediaUrl = nil
//                        }
//
//                    }
//
//                    if response.result.isFailure {
//
//                    }
//
//                }
//
//            case .failure(let encodingError):
//                print(encodingError)
//
//
//            }
//
//        }
//
//
//    }
    
    
    func alertView(title: String, message: String){
        
        let alertView = UIAlertController(title: title, message:message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
            
        })
        alertView.addAction(action)
        self.present(alertView, animated: true, completion: nil)
    
    }
    
    func submissionPreUploadProcess(){
        
        
        var mimeType = String()
        var fileName = String()
        var imageData = Data()
        var videoData = Data()
        
        if mediaType  == "public.movie" {
            mimeType = "video/mp4"
            fileName = "video.mp4"
            
            do{
                videoData =  try NSData(contentsOfFile: (self.videoPath.relativePath)!, options: NSData.ReadingOptions.alwaysMapped) as Data
                self.uploadMediaForSubmission( videoData: videoData , fileName: fileName, mimeType: mimeType)
            }
            catch{
                //    self.AlertMessage(title: "Alert", message: "Unable to process the selected video. Please try again.", buttonTitle: "Try Again")
            }
        }
        else if mediaType  == "public.image"{
            
            mimeType = "image/jpg"
            fileName = "file.jpg"
            
            imageData = UIImageJPEGRepresentation(self.capturedImage.image!,1)!
            self.uploadMediaForSubmission( imageData: imageData, fileName: fileName, mimeType: mimeType)
            
        }
        
    }
    
   
    // Mark: Upload video
    func uploadMediaForSubmission(videoData:Data  , fileName:String  , mimeType:String ) {
        
        
        
//        let value = UserDefaults.standard.value(forKey: "runForOfficeKey") as? String
//
//        // id from run for office
//        if value == ""{
//
//            self.campValue = ""
//
//        }
//        else{
//
//        self.campValue = (UserDefaults.standard.value(forKey: "runForOfficeKey") as? String)!
//
//        }
        
        
        let valueId = UserDefaults.standard.value(forKey: "runForOfficeKey") as? String
        if valueId != nil{
            
            self.campValue = valueId!
            
        }
            
        else{
            
            self.campValue = ""
            
        }
      
        DispatchQueue.main.async {
            
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
            self.sendBtnCameraView.isUserInteractionEnabled = false
        }
        
        
        let parameters: Parameters =
            ["auth_token":UserDefaults.standard.value(forKey:"authToken")!,
             "user_id":UserDefaults.standard.value(forKey: "userId")!,
             "runforoffice_id":self.campValue
        ]
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(videoData, withName: "media",fileName:fileName ,mimeType:mimeType )
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },to:"http://beta.brstdev.com/politicsplay/api/web/v1/users/add_story")
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseString(completionHandler: { (respo) in
                    print(respo)
                })
                
                upload.responseJSON { response in
                    
                    print(response)
                    if let json = response.result.value as? NSDictionary {
                        
                        if(json.value(forKey: "success") as! String == "200"){
                            
                            let storyId = json.value(forKey: "story_id") as! Int
                            let userId = UserDefaults.standard.value(forKey: "userId") as! String
                            let authToken = UserDefaults.standard.value(forKey: "authToken") as! String
                            
                            self.uploadVideoAgain(userId: userId,authToken: authToken,storyIdValue: storyId)
                            
                            
                
                            //  DataManager.submissionPreUploadedMediaUrl = json.value(forKey: "data") as? String
//                            self.alertView(title: "", message: "Story Added Successfully")
//                            self.activityIndicator.isHidden = true
//                            self.activityIndicator.stopAnimating()
//                            self.initCameraOverlay()
//                            self.sendBtnCameraView.isUserInteractionEnabled = true
                        }
                        else{
                            //   DataManager.submissionPreUploadedMediaUrl = nil
                            
//                            self.alertView(title: "", message: (response.error?.localizedDescription)!)
//                            self.activityIndicator.isHidden = true
//                            self.activityIndicator.stopAnimating()
//                            self.sendBtnCameraView.isUserInteractionEnabled = true
                        }
                        
                    }
                    
                    if response.result.isFailure {
                        
                        
                        self.alertView(title: "", message: (response.error?.localizedDescription)!)
                        self.activityIndicator.isHidden = true
                        self.activityIndicator.stopAnimating()
                        self.sendBtnCameraView.isUserInteractionEnabled = true
                    }
                    
                }
                
            case .failure(let encodingError):
                print(encodingError)
            }
            
        }
    }
    
    
//    // Mark: Upload Thumbnail
    func uploadVideoAgain(userId: String,authToken: String,storyIdValue: Int){

        
        
        DispatchQueue.main.async{

            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()


        }
        do {
            // for creating thumbnail for video url
            let asset = AVURLAsset(url: videoPath as URL , options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
            let uiImage = UIImage(cgImage: cgImage)
            thumbnail = uiImage
        }

        catch{

            print("*** Error generating thumbnail: \(error.localizedDescription)")
        }

        let dataImage = UIImagePNGRepresentation(thumbnail) as NSData?
        let image : UIImage = UIImage(data: dataImage! as Data)!



        let parameters = ["post_id":String(describing: storyIdValue)] as  [String : Any]


        print(parameters)

        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(UIImagePNGRepresentation(image)!, withName: "thumbnail", fileName: "profilepic.jpg", mimeType: "image/jpg")
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:"http://beta.brstdev.com/politicsplay/api/web/v1/users/upload_thumb")
        { (result) in
            switch result {
            case .success(let upload, _, _):

                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                    //  print(progress)

                })

                upload.responseJSON { response in
                    //print response.result
                    print(response)

                    if response.result.value != nil {

                        DispatchQueue.main.async {

                            let json = JSON(data: response.data! )

                            if let descriptionStr = json["message"].string {
                                print(descriptionStr)

                        if descriptionStr == "Post Added Successfully"{
                                    
                              ///   DataManager.submissionPreUploadedMediaUrl = json.value(forKey: "data") as? String
                                    
                                self.alertView(title: "", message: "Story Added Successfully")
                                self.activityIndicator.isHidden = true
                                self.activityIndicator.stopAnimating()
                                self.initCameraOverlay()
                               self.sendBtnCameraView.isUserInteractionEnabled = true
                                    
                            }
                                
                        else{
                            
                            //   DataManager.submissionPreUploadedMediaUrl = nil
                            
                                self.alertView(title: "", message: (response.error?.localizedDescription)!)
                               self.activityIndicator.isHidden = true
                               self.activityIndicator.stopAnimating()
                               self.sendBtnCameraView.isUserInteractionEnabled = true
                           }
                                
                          if ((response.error) != nil){
                                    let alertView = UIAlertController(title: "", message:(response.error?.localizedDescription)! , preferredStyle: .alert)
                                    let action = UIAlertAction(title: "OK", style: .default, handler:nil)
                                    alertView.addAction(action)
                                    self.present(alertView, animated: true, completion: nil)

                                }

                            }
                        }

                        DispatchQueue.main.async{

                            self.activityIndicator.isHidden = true
                            self.activityIndicator.stopAnimating()
                           
                        }
                    }
                }

            case .failure(let encodingError):

                //  self.alertViewMethod(titleStr: "", messageStr: encodingError.localizedDescription)

                break

            }

        }
    }
    
    
    
    // Mark: Upload image
    func uploadMediaForSubmission(imageData:Data  , fileName:String  , mimeType:String ) {
        
//        self.campValue = (UserDefaults.standard.value(forKey: "runForOfficeKey") as? String)!
//        // id from run for office
//        if campValue != ""{
//
//            print(campValue)
//
//        }
//        else{
//
//            print("error")
//
//        }
//
        // id from run for office
        
        let valueId = UserDefaults.standard.value(forKey: "runForOfficeKey") as? String
        if valueId != nil{
            
            self.campValue = valueId!
            
        }
            
        else{
            
            self.campValue = ""
            
        }
        
        
        DispatchQueue.main.async {
         
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
            self.sendBtnCameraView.isUserInteractionEnabled = false
        }
        
        
        let parameters: Parameters =
            ["auth_token":UserDefaults.standard.value(forKey:"authToken")!,
             "user_id":UserDefaults.standard.value(forKey: "userId")!,
             "runforoffice_id":self.campValue
        ]
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imageData, withName: "media",fileName:fileName ,mimeType:mimeType )
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },to:"http://beta.brstdev.com/politicsplay/api/web/v1/users/add_story")
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseString(completionHandler: { (respo) in
                    print(respo)
                })
                
                upload.responseJSON { response in
                    
                    print(response)
                    if let json = response.result.value as? NSDictionary {
                        
                        if(json.value(forKey: "success") as! String == "200"){
                         //  DataManager.submissionPreUploadedMediaUrl = json.value(forKey: "data") as? String
                           self.alertView(title: "", message: "Story Added Successfully")
                            self.activityIndicator.isHidden = true
                            self.activityIndicator.stopAnimating()
                            self.initCameraOverlay()
                             self.sendBtnCameraView.isUserInteractionEnabled = true
                        }
                        else{
                         //   DataManager.submissionPreUploadedMediaUrl = nil
                            
                             self.alertView(title: "", message: (response.error?.localizedDescription)!)
                             self.activityIndicator.isHidden = true
                             self.activityIndicator.stopAnimating()
                             self.sendBtnCameraView.isUserInteractionEnabled = true
                        }
                    
                    }
                    
                    if response.result.isFailure {
                        
                        
                        self.alertView(title: "", message: (response.error?.localizedDescription)!)
                        self.activityIndicator.isHidden = true
                        self.activityIndicator.stopAnimating()
                        self.sendBtnCameraView.isUserInteractionEnabled = true
                    }
                    
                }
                
            case .failure(let encodingError):
                print(encodingError)
              }
          
         }
    }
    
    
    //MARK: Common Methods.
    
    func customLayer(layer:CALayer){
        
        layer.cornerRadius = 8
        layer.masksToBounds = false;
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowRadius = 3
        layer.shadowOffset = CGSize(width: 1, height: 1)
        
    }
    
    
    
    
    @IBAction func toggleLeft(_ sender: Any) {
        
        slideMenuController()?.toggleLeft()
        
    }
    
    
    
}

extension UIView {
    
    func takeSnapshot() -> UIImage {
        
        print(self.bounds)
        print(bounds.size)
        print(UIScreen.main.scale)
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: (previewLayer?.frame.size.width)!, height: (previewLayer?.frame.size.width)!), true, UIScreen.main.scale)
        drawHierarchy(in: CGRect(origin: CGPoint(x: 0, y: -UIScreen.main.bounds.height/10 ), size: self.bounds.size), afterScreenUpdates: true)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        
        return image
    }
}
//MARK: SliderMenu Delegate Methods
extension CameraOverlay : SlideMenuControllerDelegate {
    
    func leftDidClose() {
        
        
        
    }
    
}
