//
//  CompetitonVC.swift
//  LayoutScreens
//
//  Created by Brst on 31/10/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class CompetitonVC: UIViewController{
    
   
    //Outlets
    @IBOutlet weak var table_View: UITableView!
    
    //Variables
    var arrNames = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
         table_View.showsVerticalScrollIndicator = false
        // Do any additional setup after loading the view.
        
        arrNames = ["James Anderson","David Warner","Christopher White","George Willanstan","Taylor Swift"]
    }
  
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = false
    
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func toggleLeft(_ sender: Any) {
        
        slideMenuController()?.toggleLeft()
        
    }
    


}

extension CompetitonVC:UITableViewDelegate, UITableViewDataSource{

   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return arrNames.count
   }
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath ) as! CompetitonTableViewCell
    cell.lblName.text = arrNames[indexPath.row]
    
    // assignig corner radius to image
    cell.cellImage.layer.cornerRadius = cell.cellImage.frame.height/2
    cell.cellImage.clipsToBounds = true
    
    
    //setting button follow and unfollow
    if (indexPath.row % 2) == 0 {
        
        cell.btnFollowStatus.layer.borderColor = UIColor.init(red: 191/255.0, green: 0, blue: 0, alpha: 1.0).cgColor
        cell.btnFollowStatus.layer.borderWidth = 1;
        cell.btnFollowStatus.tintColor =  UIColor.init(red: 191/255.0, green: 0, blue: 0, alpha: 1.0)
        cell.btnFollowStatus.layer.cornerRadius = cell.btnFollowStatus.frame.height/2
        cell.btnFollowStatus.clipsToBounds = true
        cell.btnFollowStatus.setTitle("Following",for: .normal)
        cell.contentView.backgroundColor = UIColor.init(red: 231/255.0, green: 236/255.0, blue: 242/255.0, alpha: 1.0)
        
    }
        
    else {
        
        cell.btnFollowStatus.backgroundColor =  UIColor.init(red: 191/255.0, green: 0, blue: 0, alpha: 1.0)
        cell.btnFollowStatus.tintColor = UIColor.white
        cell.btnFollowStatus.layer.cornerRadius =  cell.btnFollowStatus.frame.height/2
        cell.btnFollowStatus.clipsToBounds = true
        
     }
    
    // setting cell selection style to none
    cell.selectionStyle = UITableViewCellSelectionStyle.none
    
    
    
    
    return cell
    
    }
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
   
       return 70
   
   }

}




//MARK: SliderMenu Delegate Methods
extension CompetitonVC : SlideMenuControllerDelegate {
    
    func leftDidClose() {
        
        
        
    }
    
}




