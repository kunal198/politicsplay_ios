//
//  ViewController.swift
//  LayoutScreens
//
//  Created by Brst on 30/10/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITextFieldDelegate {
    
    
    
    // outlets
    @IBOutlet weak var serByNameTxt: UITextField!
    @IBOutlet weak var serByOfficeTxt: UITextField!
    @IBOutlet weak var costTxt: UITextField!
    @IBOutlet weak var donateBtn: UIButton!
    @IBOutlet var viewName: UIView!
    @IBOutlet var viewOffice: UIView!
    @IBOutlet var sideBtn: UIButton!
    
    // variables
    var isSlide = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        contentsBorder()
        
        serByNameTxt.delegate = self
        serByOfficeTxt.delegate = self
        
        
        //tap gesture for dismissing the keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginVC.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        
        // code for assigning number pad
        costTxt.keyboardType = UIKeyboardType.numberPad
   
       //code for updating image
        if isSlide{
        sideBtn.setImage(UIImage(named: "tg-bar"), for: .normal)
        }
        else{
        sideBtn.setImage(UIImage(named: "back2"), for: .normal)
   
        }
        

    
    }
   
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.isNavigationBarHidden = true
    
    }
    
    
    func contentsBorder()
    {
        donateBtn.layer.cornerRadius = donateBtn.frame.height/2
        donateBtn.clipsToBounds = true
        donateBtn.layer.borderColor = UIColor.lightGray.cgColor
        donateBtn.layer.borderWidth = 1.0
        
        viewName.layer.cornerRadius = donateBtn.frame.height/2
        viewName.clipsToBounds = true
        viewName.layer.borderColor = UIColor.lightGray.cgColor
        viewName.layer.borderWidth = 1.0
        

        
        viewOffice.layer.cornerRadius = donateBtn.frame.height/2
        viewOffice.clipsToBounds = true
        viewOffice.layer.borderColor = UIColor.lightGray.cgColor
        viewOffice.layer.borderWidth = 1.0
        

        costTxt.layer.cornerRadius = donateBtn.frame.height/2
        costTxt.clipsToBounds = true
        costTxt.layer.borderColor = UIColor.lightGray.cgColor
        costTxt.layer.borderWidth = 1.0
    
    
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func toggleLeft(_ sender: Any) {
        
        print(isSlide)
        if isSlide{
            
            slideMenuController()?.toggleLeft()
       
        }
        else{
            
            self.navigationController?.popViewController(animated: true)
        }
      
        
    }
    
    
    @IBAction func pushDonatePaymt(_ sender: Any) {
    
    
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let donateViewController = storyBoard.instantiateViewController(withIdentifier:
            "DonateCompaignVC") as! DonateCompaignVC
        self.navigationController?.pushViewController(donateViewController, animated: true)
    
    }
    
    
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        
        serByNameTxt.resignFirstResponder()
        serByOfficeTxt.resignFirstResponder()
        return true;
    }
    
    
    
    //tap gesture
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    
    
}

//MARK: SliderMenu Delegate Methods
extension ViewController : SlideMenuControllerDelegate {
    
    func leftDidClose() {
        
        
        
    }
    
}




