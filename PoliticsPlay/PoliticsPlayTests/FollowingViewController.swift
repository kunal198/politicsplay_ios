//
//  FollowingViewController.swift
//  PoliticsPlay
//
//  Created by Brst981 on 31/10/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


protocol FollowingViewControllerDelegate:class{
    
    func getProfileDataMethd(userId: String,authToken: String)
    
    
}


class FollowingViewController: UIViewController,TTTAttributedLabelDelegate {
    
    // Variables
    var arrFollowersStatus = [String]()
    var authTokenValue = String()
    var userIdValue = String()
    var arrDataListing = NSMutableArray()
    var status = Int()
    var followerUserId = Int()
    var otherUserId = Int()
    var checkUserProfile = Bool()
    var checkProfile = Bool()
    var dataOtherUserProfileArray = NSMutableArray()
    var concatenateString = String()
    var btnStatusUserProfile = NSInteger()
    var btnStatusMyProfile = NSInteger()
    var otherFollowerId = NSInteger()
    weak var delegate: FollowingViewControllerDelegate?

    //Outlets
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var tableView: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
  
        
        if UserDefaults.standard.value(forKey: "userId") != nil{

                userIdValue = UserDefaults.standard.value(forKey: "userId") as! String
                authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
                print(userIdValue)
                print(authTokenValue)
                self.otherFollowingList(userId: userIdValue, authToken: authTokenValue)
            
        }
        
}


    
   override func viewWillAppear(_ animated: Bool) {
        
   if UserDefaults.standard.value(forKey: "userId") != nil{
            
            if checkProfile == true{
                
                userIdValue = UserDefaults.standard.value(forKey: "userId") as! String
                authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
                print(userIdValue)
                print(authTokenValue)
                self.followingList(userId: userIdValue, authToken: authTokenValue)
                
                
            }
            
        }
    }
        
        override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func popBack(_ sender: Any) {
   
    
        
        if UserDefaults.standard.value(forKey: "userId") != nil{
            
           let userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as! String
           let authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
            delegate?.getProfileDataMethd(userId: userIdValueAgain , authToken: authTokenValue )
            
        }
        
        self.navigationController?.popViewController(animated: true)
    
    }
    
    
    //code to push the view controller on button click
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        
        if checkProfile == true{
            
            
            print(label.tag)
            let dictUserProfile = arrDataListing[label.tag] as! NSDictionary
            let followerID = dictUserProfile.value(forKey: "following_user_id") as! Int
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let userProfile = storyBoard.instantiateViewController(withIdentifier: "UserProfileVC") as! UserProfileVC
            userProfile.valuePerm = true
            userProfile.statusType = "yes"
            userProfile.otherUserId = followerID
            self.navigationController?.pushViewController(userProfile, animated: true)
            
        }
        
            
            
        else if (userIdValue == String(describing:otherFollowerId)) {

            let myProfile = storyboard?.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
             myProfile.valueKey = true
            self.navigationController?.pushViewController(myProfile, animated: true)

        }
            
        else{
            
            
            print(label.tag)
            let dictUserProfile = dataOtherUserProfileArray[label.tag] as! NSDictionary
            let followerID = dictUserProfile.value(forKey: "user_id") as! Int
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let userProfile = storyBoard.instantiateViewController(withIdentifier: "UserProfileVC") as! UserProfileVC
            userProfile.valuePerm = true
             userProfile.statusType = "yes"
            userProfile.otherUserId = followerID
            self.navigationController?.pushViewController(userProfile, animated: true)
    }
       
        
    }
    
    //Mark: Fetch following list
    func followingList(userId: String,authToken: String){
        
        
        
        DispatchQueue.main.async {
            
         self.activityIndicator.isHidden = false
         self.activityIndicator.startAnimating()
      }
        
     let json = [
            "user_id":userId,
            "auth_token":authToken
            ] as [String : Any]
        print(json)
        
        
        Alamofire.request("http://beta.brstdev.com/politicsplay/api/web/v1/users/view_following_list", method: .post, parameters:                                           json, encoding: URLEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if((response.error) != nil){
                    
                    let alertView = UIAlertController(title: "", message:(response.error?.localizedDescription)! , preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                        
                    })
                    alertView.addAction(action)
                    self.present(alertView, animated: true, completion: nil)
                    
                }
                DispatchQueue.main.async {
                    let json = JSON(data: response.data! )
                        let jsonDict = json.dictionaryObject
                    let descriptionStr = jsonDict!["message"] as? NSString
                       if descriptionStr == "No data found"{
                       
                        self.alertView(title: "", message: descriptionStr! as String)
                     
                       }
                        
                       else if((response.error) != nil){
                        
                        self.alertView(title: "", message: (response.error?.localizedDescription)!)
                        
                       }
                     
                        
                      else{
                        
                        let dataListing = jsonDict!["data"] as! NSArray
                        self.arrDataListing = dataListing.mutableCopy() as! NSMutableArray
                        print(self.arrDataListing)
                        
                    }
                   
                   
                    DispatchQueue.main.async{
                        
                                self.activityIndicator.isHidden = true
                                self.activityIndicator.stopAnimating()
                                self.tableView.reloadData()
                        }
             }
                
        }
        
    }
    
    
    //Mark: Fetch other user following list
    func otherFollowingList(userId: String,authToken: String){
        
        
        
        DispatchQueue.main.async {
            
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
        }
        
        let json = [
            "user_id":userId,
            "auth_token":authToken,
            "other_user_id":otherUserId
            ] as [String : Any]
        print(json)
        
        
        Alamofire.request("http://beta.brstdev.com/politicsplay/api/web/v1/users/get_list_of_following", method: .post, parameters:                                           json, encoding: URLEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                 if((response.error) != nil){
                    
                    self.alertView(title: "", message: (response.error?.localizedDescription)!)
                    
                }
                DispatchQueue.main.async {
                    let json = JSON(data: response.data! )
                    let jsonDict = json.dictionaryObject
                    let descriptionStr = jsonDict?["message"] as! String
                    if  descriptionStr == "List of followings"{
                     
                    let dataArray = jsonDict?["data"] as! NSArray
                    self.dataOtherUserProfileArray = dataArray.mutableCopy() as! NSMutableArray
                        
                    }
                        
                    else if descriptionStr == "No data found."{
                        
                       self.alertView(title: "", message: descriptionStr)
                        
                    }
                   
                }
                
                DispatchQueue.main.async{
                    
                    self.activityIndicator.isHidden = true
                    self.activityIndicator.stopAnimating()
                    self.tableView.reloadData()
                }
                
        }
        
    }

    // code to follow/unfollow any person
    
    @IBAction func btnFollowUnfollowCandidates(_ sender: UIButton) {
        
        //   followUnfollowUser(userId:userIdValue, authToken:authTokenValue)
        
        
    }
    
    
    // code for follow/unfollow people
    func followUnfollowUser(userId: String, authToken: String, status: NSInteger, otherUsersId: Int){
        
        
        DispatchQueue.main.async {
            
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
            
        }  // end of dispatch
        
        let json = [
            "user_id":userId,
            "auth_token":authToken,
            "other_user_id":otherUsersId,
            "status":status
            ] as [String : Any]
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/politicsplay/api/web/v1/users/follow_user", method: .post, parameters:                                           json, encoding: URLEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                DispatchQueue.main.async {
                    let json = JSON(data: response.data! )
                    let jsonDict = json.dictionaryObject
                    let descriptionStr = jsonDict?["message"] as! NSString
                     if ((descriptionStr == "Unfollow Successfully") || (descriptionStr == "Follow Successfully"))
                    {
                        if UserDefaults.standard.value(forKey: "userId") != nil{
                            
                            
                            if self.checkProfile == true{
                                
                               self.userIdValue = UserDefaults.standard.value(forKey: "userId") as! String
                                self.authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
                                self.followingList(userId: self.userIdValue, authToken: self.authTokenValue)
                                
                                
                            }
                            else {
                                
                                
                                self.userIdValue = UserDefaults.standard.value(forKey: "userId") as! String
                                self.self.authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
                                self.otherFollowingList(userId: self.userIdValue, authToken: self.authTokenValue)
                                
                            }
                                
                       }
                  }
                    
                    else if(descriptionStr == "No follower found to unfollow"){
                        
                           self.alertView(title: "", message: descriptionStr as String)
                        
                    }
                        
                     else if(descriptionStr == "No data found"){
                        
                 
                        if UserDefaults.standard.value(forKey: "userId") != nil{
                            
                            
                            if self.checkProfile == true{
                                
                                self.userIdValue = UserDefaults.standard.value(forKey: "userId") as! String
                                self.authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
                                self.followingList(userId: self.userIdValue, authToken: self.authTokenValue)
                                
                                
                            }
                            else {
                                
                                
                                self.userIdValue = UserDefaults.standard.value(forKey: "userId") as! String
                                self.self.authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
                                self.otherFollowingList(userId: self.userIdValue, authToken: self.authTokenValue)
                                
                            }
                            
                        }
                        
                    }
                        
                     else if(descriptionStr == "Already followed to this user"){
                        
                        self.alertView(title: "", message: descriptionStr as String)
                        
                    }
                     else if((response.error) != nil){
                        
                        self.alertView(title: "", message: (response.error?.localizedDescription)!)
                        
                    }
                  
                    DispatchQueue.main.async{
                        
                        self.activityIndicator.isHidden = true
                        self.activityIndicator.stopAnimating()
                     //   self.tableView.reloadData()
                        
                    }  // end of dispatch
                    
                } // end of outer dispatch
                
        }  // end of alamofire
        
    }  // end of method
    
    
    
   //Mark: Alert view
    func alertView(title: String, message: String){
    
    
        let alertView = UIAlertController(title: title, message:message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
        })
        alertView.addAction(action)
        self.present(alertView, animated: true, completion: nil)
    }
    
    
    //Mark: Follow Unfollow My profile scenario
    
    func followUnfollowMyProfile(_ sender: UIButton){
        
        print(sender.tag)
        let dictUserProfile = arrDataListing[sender.tag] as! NSDictionary
        let followerID = dictUserProfile.value(forKey: "following_user_id") as! Int
        var index = IndexPath()
        index = IndexPath.init(item: Int(sender.tag), section: 0)
        let cell = self.tableView.cellForRow(at: index) as! FollowingCell
        if let text =  cell.btnFollowStatus.titleLabel?.text {
        if text == "Following"{
                
                
                let alertView = UIAlertController(title: "", message:"Do you really want to unfollow?", preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                  
                    self.followUnfollowUser(userId: self.userIdValue, authToken:self.authTokenValue, status: 0, otherUsersId: followerID)
                
                })
                let action2 = UIAlertAction(title: "Cancel", style: .default, handler: { (alert) in
                })
                alertView.addAction(action)
                alertView.addAction(action2)
                self.present(alertView, animated: true, completion: nil)
                
            }
            else{
                
                followUnfollowUser(userId:userIdValue, authToken:authTokenValue, status: 1, otherUsersId: followerID)
                
            }
    }
        
}
    
    // Mark: Follow Unfollow other profile scenario
    func followUnfollowUserProfile(_ sender: UIButton){
        
        print(sender.tag)
        
        let dictUserProfile = dataOtherUserProfileArray[sender.tag] as! NSDictionary
        let followerID = dictUserProfile.value(forKey: "user_id") as! Int
        var index = IndexPath()
        index = IndexPath.init(item: Int(sender.tag), section: 0)
        let cell = self.tableView.cellForRow(at: index) as! FollowingCell
        if let text =  cell.btnFollowStatus.titleLabel?.text {
            if text == "Following"{
                
                let alertView = UIAlertController(title: "", message:"Do you really want to unfollow?", preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                    
                    self.followUnfollowUser(userId: self.userIdValue, authToken:self.authTokenValue , status: 0, otherUsersId: followerID)
                    
                })
                let action2 = UIAlertAction(title: "Cancel", style: .default, handler: { (alert) in
                })
                alertView.addAction(action)
                alertView.addAction(action2)
                self.present(alertView, animated: true, completion: nil)
        }
            else{
                
                followUnfollowUser(userId:userIdValue, authToken:authTokenValue , status: 1, otherUsersId: followerID)
                
            }
            
            
            
            
        }
        
    
    }
    
    
    
    
    
    
    
}
extension FollowingViewController : UITableViewDataSource,UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return  arrFollowersName.count
        
        if checkProfile == true{
        
            print(arrDataListing.count)
            return arrDataListing.count
        
        }
        
        else{
        
            print(dataOtherUserProfileArray.count)
            return dataOtherUserProfileArray.count
        }
        
    
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
      let cell  = tableView.dequeueReusableCell(withIdentifier: "followingCell", for: indexPath) as! FollowingCell
        if checkProfile == true{
            // Mark: for my profile
            let dict = arrDataListing[indexPath.row] as! NSDictionary
            let followingData = dict.value(forKey: "following_data") as! NSDictionary
            print(followingData)
            followerUserId = dict.value(forKey: "following_user_id") as! NSInteger
            let firstName = followingData.value(forKey: "firstname") as! NSString
            let lastName = followingData.value(forKey: "lastname") as! NSString
            self.concatenateString = "\(firstName) \(lastName)"
            btnStatusMyProfile = dict.value(forKey: "is_follower") as! NSInteger
            cell.lblNameDesp.text = concatenateString
            cell.btnFollowStatus.setTitle("Following",for: .normal)
            cell.btnFollowStatus.tag = indexPath.row
            cell.btnFollowStatus.addTarget(self, action: #selector(followUnfollowMyProfile), for: UIControlEvents.touchUpInside)
        }
        
        else{
        
            
            // Mark: for other user profile
            let dictUserProfile = dataOtherUserProfileArray[indexPath.row] as! NSDictionary
            let firstName = dictUserProfile.value(forKey: "firstname") as! NSString
            let lastName = dictUserProfile.value(forKey: "lastname") as! NSString
            self.concatenateString  = "\(firstName) \(lastName)"
            cell.lblNameDesp.text = concatenateString
            btnStatusUserProfile = dictUserProfile.value(forKey: "is_follower") as! NSInteger
            otherFollowerId = dictUserProfile.value(forKey: "user_id") as! NSInteger
            cell.btnFollowStatus.setTitle("Following",for: .normal)
            cell.btnFollowStatus.tag = indexPath.row
            cell.btnFollowStatus.addTarget(self, action: #selector(followUnfollowUserProfile), for: UIControlEvents.touchUpInside)
            if  userIdValue == String(describing:otherFollowerId){
                
                cell.btnFollowStatus.isHidden = true
                
            }
            else{
                
                cell.btnFollowStatus.isHidden = false
                
            }
        
        }
     
        // Mark: for desining refinement
        if (indexPath.row % 2) == 0 {
            
            cell.btnFollowStatus.layer.borderColor = UIColor.init(red: 191/255.0, green: 0, blue: 0, alpha: 1.0).cgColor
            cell.btnFollowStatus.layer.borderWidth = 1;
            cell.btnFollowStatus.tintColor =  UIColor.init(red: 191/255.0, green: 0, blue: 0, alpha: 1.0)
            cell.btnFollowStatus.layer.cornerRadius = cell.btnFollowStatus.frame.height/2
            cell.btnFollowStatus.clipsToBounds = true
            cell.contentView.backgroundColor = UIColor.init(red: 231/255.0, green: 236/255.0, blue: 242/255.0, alpha: 1.0)
            
        }
            
        else {
            
            cell.btnFollowStatus.backgroundColor =  UIColor.init(red: 191/255.0, green: 0, blue: 0, alpha: 1.0)
            cell.btnFollowStatus.tintColor = UIColor.white
            cell.btnFollowStatus.layer.cornerRadius =  cell.btnFollowStatus.frame.height/2
            cell.btnFollowStatus.clipsToBounds = true
        }
        
        // code to make text underline
        let str : NSString = concatenateString as NSString
        cell.lblNameDesp.delegate = self
        cell.lblNameDesp.tag = indexPath.row
        cell.lblNameDesp.text = str as String
        let range : NSRange = str.range(of: str as String)
        cell.lblNameDesp.addLink(to: NSURL(string: "")! as URL!, with: range)
        cell.selectionStyle = UITableViewCellSelectionStyle.none;
        return cell
   }
    

}
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

