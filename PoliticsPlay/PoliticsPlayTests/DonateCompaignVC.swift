//
//  DonateCompaignVC.swift
//  LayoutScreens
//
//  Created by Brst on 30/10/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class DonateCompaignVC: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var scroll_View: UIScrollView!
   
    @IBOutlet var creditNumberTF: UITextField!
    @IBOutlet weak var cvvNumTxt: UITextField!
    @IBOutlet weak var expirationTxt: UITextField!
     @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet var donateBtnRed: UIButton!
    @IBOutlet var monthBtn: UIButton!
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
 
        // setting delegate
        creditNumberTF.delegate = self
        cvvNumTxt.delegate = self
        expirationTxt.delegate = self
        emailTxt.delegate = self
        
        
        contentsBorder()
        
        // code for assigning number pad to keyboard type.
        creditNumberTF.keyboardType = UIKeyboardType.numberPad
        cvvNumTxt.keyboardType = UIKeyboardType.numberPad
        expirationTxt.keyboardType = UIKeyboardType.numberPad
        
        
        //tap gesture for dismissing the keyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginVC.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
    }
    
    
    
    
    
    
    func contentsBorder()
    {
        setFieldBorder(textField: creditNumberTF)
        setFieldBorder(textField: cvvNumTxt)
        setFieldBorder(textField: expirationTxt)
        setFieldBorder(textField: emailTxt)
        donateBtnRed.layer.cornerRadius = donateBtnRed.frame.height/2
//        monthBtn.layer.masksToBounds = false
//        monthBtn.layer.shadowColor = UIColor.black.cgColor
//        monthBtn.layer.shadowOpacity = 1.0
//        monthBtn.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
//        monthBtn.layer.shadowRadius = 3.0
    }

    func setFieldBorder(textField: UITextField)
    {
        
       textField.layer.masksToBounds = false
       textField.layer.shadowColor = UIColor.black.cgColor
       textField.layer.shadowOpacity = 0.8
       textField.layer.shadowOffset = CGSize(width: 1, height: 1)
       textField.layer.shadowRadius = 2.0
       //textField.layer.cornerRadius = 20.0
   }
 
    
    override func viewWillLayoutSubviews() {
        scroll_View.contentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height+100)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    
    
    }
    
    
    
    
    //code for pushing to donate screen
    
    @IBAction func pushDonateScreen(_ sender: Any) {
    
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let donateController = storyBoard.instantiateViewController(withIdentifier:
            "DonateCompaignSecVC") as! DonateCompaignSecVC
        self.navigationController?.pushViewController(donateController, animated: true)
    
    }
    
    
    
    
    @IBAction func popBack(_ sender: Any) {
    
    self.navigationController?.popViewController(animated: true)
    
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        creditNumberTF.resignFirstResponder()
        cvvNumTxt.resignFirstResponder()
        expirationTxt.resignFirstResponder()
        emailTxt.resignFirstResponder()
        return true;
        
    }
    
    
    //tap gesture
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
}


//extension UIView {
//    
//    // OUTPUT 1
//    func dropShadow(scale: Bool = true) {
//        self.layer.masksToBounds = false
//        self.layer.shadowColor = UIColor.black.cgColor
//        self.layer.shadowOpacity = 0.5
//        self.layer.shadowOffset = CGSize(width: -1, height: 1)
//        self.layer.shadowRadius = 1
//        
//        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
//        self.layer.shouldRasterize = true
//        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
//}
//

//}





