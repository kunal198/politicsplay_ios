//
//  CandidateSupportVC.swift
//  LayoutScreens
//
//  Created by Brst on 01/11/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class CandidateSupportVC: UIViewController,TTTAttributedLabelDelegate{
    @IBOutlet weak var table_View: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        table_View.showsVerticalScrollIndicator = false
        table_View.separatorStyle = .none
    }
 

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func toggleLeft(_ sender: Any) {
        
        slideMenuController()?.toggleLeft()
        
    }
    
    
    
    
    @IBAction func popBack(_ sender: Any) {
    
    
    self.navigationController?.popViewController(animated: true)
    
    
    
    }
    
    
    //code for pushing forward to view controller on press of link
    
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        
        
        print(label.tag)
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let userProfile = storyBoard.instantiateViewController(withIdentifier: "UserProfileVC") as! UserProfileVC
        userProfile.valuePerm = true
        self.navigationController?.pushViewController(userProfile, animated: true)
        
    }
    
    
    
    
    

}

extension CandidateSupportVC:UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)as! CandidateSupportTableViewCell
        
        cell.selectionStyle = .none
        if indexPath.row % 2 == 0 {
            cell.cellContentView.backgroundColor = UIColor.white
        }
        else {
            
            cell.cellContentView.backgroundColor = UIColor (colorLiteralRed: 231/255, green: 235/255, blue: 240/255, alpha: 1.0)
        }
        
        
        
        // code to make text underline
        let str : NSString = "Isadore Hall enorsed kamala Harries for president"
        cell.cellLabel.delegate = self
        cell.cellLabel.tag = indexPath.row
        cell.cellLabel.text = str as String
        let range : NSRange = str.range(of: "Isadore Hall")
        cell.cellLabel.addLink(to: NSURL(string: "")! as URL!, with: range)
        
        return cell
    }
    
}
//MARK: SliderMenu Delegate Methods
extension CandidateSupportVC : SlideMenuControllerDelegate {
    
    func leftDidClose() {
        
        
        
    }
    
}








