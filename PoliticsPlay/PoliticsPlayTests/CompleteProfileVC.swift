//
//  CompleteProfileVC.swift
//  PoliticsPlay
//
//  Created by Brst981 on 05/12/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AVFoundation
import Photos
class CompleteProfileVC: UIViewController {
    
    
    
    //Outlets
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var txtFieldFName: UITextField!
    @IBOutlet var txtFieldLName: UITextField!
    @IBOutlet var txtFieldEmail: UITextField!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    
    
    //Variables
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()


        
       self.activityIndicator.isHidden = true
        
        
        
        imageView.layer.cornerRadius = imageView.frame.height/2
        imageView.clipsToBounds = true
        
        
        customizeTF()
        customizeView()
        
        txtFieldFName.delegate = self as? UITextFieldDelegate
        txtFieldLName.delegate = self as? UITextFieldDelegate
        txtFieldEmail.delegate = self as? UITextFieldDelegate
        
        
        // code for setting delegate of image picker
        self.imagePicker.delegate = self
        
        
        
        // fetching all the info from facebook and showing in this screen text field
        self.txtFieldEmail.text =  Singleton.sharedInstance.emailStr
        self.txtFieldLName.text = Singleton.sharedInstance.lastNameStr
        self.txtFieldFName.text = Singleton.sharedInstance.firstNameStr
    //    self.imageView.image = Singleton.sharedInstance.userImageStr
        
        
        // code for showing no of follwers  and no of supporters
        
        print(Singleton.sharedInstance.userIdStr)
        print(Singleton.sharedInstance.auth_token)

        
        
   }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btnSubmit(_ sender: Any) {
        
        
        
        
        if txtFieldEmail.text == "" || txtFieldFName.text == ""{
            alertViewMethod(titleStr: "", messageStr: "Please enter all fields.")
        }else if !isValidEmail(testStr: txtFieldEmail.text!) {
            alertViewMethod(titleStr: "", messageStr: "Please enter the valid email.")
        } 
        else{
            
            if UserDefaults.standard.value(forKey: "userId") != nil{
                
                let userIdValue = UserDefaults.standard.value(forKey: "userId")
                let authTokenValue = UserDefaults.standard.value(forKey: "authToken")
                print(userIdValue)
                print(authTokenValue)
                completeProfile(userId: userIdValue as! String, authToken: authTokenValue as! String )
                
            }
    
        }
        
        
    }

    override func viewWillLayoutSubviews() {
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height+100)
    }
    
    
    func customizeTF(){
        
        let fullStringEmail = NSMutableAttributedString()
        let fullStringFName = NSMutableAttributedString()
        let fullStringLName = NSMutableAttributedString()
        
        let image1Attachment = NSTextAttachment()
        let image2Attachment = NSTextAttachment()
        let image3Attachment = NSTextAttachment()
        
        
        let image1 = UIImage(named: "user")
        let image2 = UIImage(named: "user")
        let image3 = UIImage(named: "message")
       
        
        
        
        
        image1Attachment.image = image1
        image2Attachment.image = image2
        image3Attachment.image = image3
        
        let font = txtFieldEmail.font!
        let mid = font.descender + font.capHeight
        image1Attachment.bounds = CGRect(x: 0, y: font.descender - image1!.size.height / 2 + mid + 2, width: image1!.size.width, height:image1!.size.height).integral
        image2Attachment.bounds = CGRect(x: 0, y: font.descender - image2!.size.height / 2 + mid + 2, width: image2!.size.width, height:image2!.size.height).integral
        image3Attachment.bounds = CGRect(x: 0, y: font.descender - image3!.size.height / 2 + mid + 2, width: image3!.size.width, height:image2!.size.height).integral
        
        
        let image1String = NSAttributedString(attachment: image1Attachment)
        let image2String = NSAttributedString(attachment: image2Attachment)
        let image3String = NSAttributedString(attachment: image3Attachment)
        
        
        fullStringFName .append(image1String)
        fullStringLName .append(image2String)
        fullStringEmail .append(image3String)
        
        
        
        fullStringEmail.append(NSAttributedString(string: "  Email*"))
        fullStringFName.append(NSAttributedString(string: "  First Name*"))
        fullStringLName.append(NSAttributedString(string: "  Last Name(Optional)"))
        
        
        txtFieldEmail.attributedPlaceholder = fullStringEmail
        txtFieldFName.attributedPlaceholder = fullStringFName
        txtFieldLName.attributedPlaceholder = fullStringLName
        
    }
    
    
    
    func textFieldShouldReturn(textField: UITextField!) -> Bool
    {
        txtFieldFName.resignFirstResponder()
        txtFieldLName.resignFirstResponder()
        txtFieldEmail.resignFirstResponder()
        return true;
        
        
    }
    
    
    
    func customizeView(){
        
        btnSubmit.layer.cornerRadius = btnSubmit.frame.height/2;
        btnSubmit.clipsToBounds = true
        
        
    }
    
    
    // code for picking image from image gallery
    
    @IBAction func pickingImageFromGallery(_ sender: Any) {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let camera = UIAlertAction(title: "Camera", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.checkCameraAuthorization()
        })
        let gallery = UIAlertAction(title: "Gallery", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.checkPhotoAuthorization()
        })
        alertController.addAction(gallery)
        alertController.addAction(camera)
        alertController.addAction(cancel)
        present(alertController, animated: true, completion: { _ in })
        
        
    }
    
    
    
    //MARK: CheckCameraAuthorization
    
    func checkCameraAuthorization() {
        
        let status: AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        if status == .authorized {
            // authorized            picker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: { _ in })
        } else if status == .denied {
            // denied
            if AVCaptureDevice.responds(to: #selector(AVCaptureDevice.requestAccess(forMediaType:completionHandler:))) {
                AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {(_ granted: Bool) -> Void in
                    // Will get here on both iOS 7 & 8 even though camera permissions weren't required
                    // until iOS 8. So for iOS 7 permission will always be granted.
                    print("DENIED")
                    if granted {
                        // Permission has been granted. Use dispatch_async for any UI updating
                        // code because this block may be executed in a thread.
                        
                        DispatchQueue.main.async(execute: {() -> Void in
                            let picker = UIImagePickerController()
                            picker.delegate = self
                            picker.allowsEditing = true
                            picker.sourceType = .camera
                            self.present(picker, animated: true, completion: { _ in })
                        })
                        
                    }
                    else {
                        // Permission has been denied.
                        
                        
                        DispatchQueue.main.async(execute: {() -> Void in
                            self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
                        })
                        
                        
                    }
                })
            }
        } else if status == .restricted {
            // restricted
            DispatchQueue.main.async(execute: {() -> Void in
                self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
            })
        }
        else if status == .notDetermined {
            // not determined
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: {(_ granted: Bool) -> Void in
                if granted {
                    // Access has been granted ..do something
                    DispatchQueue.main.async(execute: {() -> Void in
                        let picker = UIImagePickerController()
                        picker.delegate = self
                        picker.allowsEditing = true
                        picker.sourceType = .camera
                        self.present(picker, animated: true, completion: { _ in })
                        
                    })
                    
                    
                }
                else {
                    // Access denied ..do something
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.accessMethod("Please go to Settings and enable the camera for this app to use this feature.")
                    })
                    
                }
            })
        }
    }
    
    
    
    func accessMethod(_ message: String) {
        let alertController = UIAlertController(title: "Not Authorized", message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "OK", style: .default, handler: nil)
        let Settings = UIAlertAction(title: "Settings", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
        })
        alertController.addAction(Settings)
        alertController.addAction(cancel)
        present(alertController, animated: true, completion: { _ in })
    }
    
    
    //MARK: Resize Image
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0,y: 0,width: newWidth,height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    
    //MARK: CheckPhotoAuthorization
    
    func checkPhotoAuthorization()  {
        let status: PHAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        if status == .authorized {
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = .photoLibrary
            present(imagePicker, animated: true, completion: { _ in })
        }
        else if status == .denied {
            DispatchQueue.main.async(execute: {() -> Void in
                self.accessMethod("Please go to Settings and enable the photos for this app to use this feature.")
            })
        } else if status == .notDetermined {
            // Access has not been determined.
            PHPhotoLibrary.requestAuthorization({(_ status: PHAuthorizationStatus) -> Void in
                if status == .authorized {
                    
                    DispatchQueue.main.async(execute: {() -> Void in
                        
                        self.imagePicker.delegate = self
                        self.imagePicker.allowsEditing = true
                        self.imagePicker.sourceType = .photoLibrary
                        self.present(self.imagePicker, animated: true, completion: { _ in })
                    })
                    
                }
                else {
                    DispatchQueue.main.async(execute: {() -> Void in
                        self.accessMethod("Please go to Settings and enable the photos for this app to use this feature.")
                    })
                }
            })
        }
        else if status == .restricted {
            // Restricted access - normally won't happen.
        }
        
        
    }

    
// api integration of complete profile
    func completeProfile(userId: String,authToken:String){
        
        
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
        self.txtFieldFName.isUserInteractionEnabled = false
        self.txtFieldEmail.isUserInteractionEnabled = false
        self.txtFieldLName.isUserInteractionEnabled = false
        
        let parameters = ["user_id":userId,
                    "auth_token":authToken,
                    "firstname":txtFieldFName.text!,
                    "lastname":txtFieldLName.text!,
                    "email":txtFieldEmail.text!
            ] as [String : Any]
      
        
        print(parameters)
        
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(UIImagePNGRepresentation(self.imageView.image!)!, withName: "profile_pic", fileName: "profilepic.jpg", mimeType: "image/jpg")
            for (key, value) in parameters {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:"http://beta.brstdev.com/politicsplay/api/web/v1/users/complete_profile")
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                    //  print(progress)
                    
                })
                
                upload.responseJSON { response in
                    //print response.result
                    print(response)
                    
                    if response.result.value != nil {
                        
                        DispatchQueue.main.async {
                            
                            
                            let json = JSON(data: response.data! )
                            if let descriptionStr = json["message"].string {
                                print(descriptionStr)
                                if descriptionStr == "Email is already taken"{
                                    
                                    self.alertViewMethod(titleStr: "", messageStr: descriptionStr)
                                }
                                    
                                else if  descriptionStr ==  "Password must contain minimum 6 characters"{
                                    
                                    self.alertViewMethod(titleStr: "", messageStr: descriptionStr)
                                    
                                }
                                    
                                else if  descriptionStr ==  "Profile Updated successfully"{
                                    
                                    let userId = String(describing: json["data"]["user_id"].int!)
                                    Singleton.sharedInstance.userIdStr = userId
                                    
                                    UserDefaults.standard.set(userId, forKey:"userId")
                                    UserDefaults.standard.synchronize()
                                    
                                    
                                    let authToken = String(describing: json["data"]["auth_token"].string!)
                                    Singleton.sharedInstance.auth_token = authToken
                                    
                                    UserDefaults.standard.set(authToken, forKey:"authToken")
                                    UserDefaults.standard.synchronize()
                                    
                                    self.appDelegate.createMenu()
                                    
                                }
                                
                            else if descriptionStr == "Email is already taken."{
                                    
                                self.alertViewMethod(titleStr: "", messageStr: "This email id had been already registered")
                                    
                            }
                                
                            }
                            self.activityIndicator.isHidden = true
                            self.activityIndicator.stopAnimating()
                            self.txtFieldFName.isUserInteractionEnabled = true
                            self.txtFieldEmail.isUserInteractionEnabled = true
                            self.txtFieldLName.isUserInteractionEnabled = true
                        }
                    }
            }
                
            case .failure(let encodingError):
                
                self.alertViewMethod(titleStr: "", messageStr: encodingError.localizedDescription)
                break
                //print encodingError.description
                
            }
        }
        
    }
    
    
  
    
    
    
    
    // MARK: AlertView
    
    func alertViewMethod(titleStr:String, messageStr:String) {
        
        let alertView = UIAlertController(title: titleStr, message:messageStr , preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
            
        })
        alertView.addAction(action)
        self.present(alertView, animated: true, completion: nil)
        
    }
    
    
    // MARK: Email Validation
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
        
    }
    
 }

 extension CompleteProfileVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if (info[UIImagePickerControllerEditedImage] != nil)
        {
            var chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage
             chosenImage = resizeImage(image: chosenImage, newWidth: 400)
            imageView.clipsToBounds = true
            // imageView.contentMode = .scaleAspectFill
            imageView.layer.cornerRadius = imageView.frame.size.height/2
            imageView.image = chosenImage
            
            // code for compressing image
            let compressData = UIImageJPEGRepresentation(chosenImage, 0.5) //max value is 1.0 and minimum is 0.0
            let compressedImage = UIImage(data: compressData!)
        }
        dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    

}



