//
//  DataManager.swift
//  Roasted
//
//  Created by Manish Gumbal on 21/01/17.
//  Copyright © 2017 Belal. All rights reserved.
//

import Foundation
class DataManager{
    
    static var userId:Int? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kUserId)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.integer(forKey: kUserId)
        }
    }

    static var email:String? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kUserEmail)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kUserEmail)
        }
    }
    
    static var userImage:String? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kUserImage)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kUserImage)
        }
    }
    
   
    
    static var deviceToken: String? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kDeviceToken)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.string(forKey: kDeviceToken)
        }
    }
    
  
    static var isCastee: Bool?{
        set{
            UserDefaults.standard.setValue(newValue, forKey: kIsCastee)
            UserDefaults.standard.synchronize()

        }
        get {
            return UserDefaults.standard.bool(forKey:kIsCastee)
        }
    }
    
    static var isNotificationOn: Bool?{
        set{
            UserDefaults.standard.setValue(newValue, forKey: kIsNotificationOn)
            UserDefaults.standard.synchronize()
        }
        get{
            return UserDefaults.standard.bool(forKey: kIsNotificationOn)
        }
    }
    
    static var isUserLoggedIn:Bool?{
        set {
            UserDefaults.standard.setValue(newValue, forKey: kIsUserLoggedIn)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.bool(forKey: kIsUserLoggedIn)
        }
    }
    
    static var patientList: [NSDictionary]? {
        set {
            UserDefaults.standard.setValue(newValue, forKey: kPatientList)
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.object(forKey: kPatientList) as? [NSDictionary]
        }
    }
}

