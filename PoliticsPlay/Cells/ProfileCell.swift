//
//  ProfileCell.swift
//  PoliticsPlay
//
//  Created by Brst981 on 07/12/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell {
    
    
    
    // Outlets
    
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var cellContentView: UIView!
    @IBOutlet var viewHeader: UIView!
    @IBOutlet var viewOuter: UIView!
    @IBOutlet var lblImage: UIImageView!
    @IBOutlet var outerImgView: UIImageView!
    @IBOutlet var thumbnailIcon: UIImageView!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var imgLike: UIImageView!
    
    @IBOutlet weak var lblNoFeeds: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
