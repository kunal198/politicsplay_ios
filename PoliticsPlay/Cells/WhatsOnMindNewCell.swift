//
//  WhatsOnMindNewCell.swift
//  PoliticsPlay
//
//  Created by Brst981 on 27/12/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit

class WhatsOnMindNewCell: UITableViewCell {
    
    
    // outlets
    
    @IBOutlet var imgView: UIImageView!
    
    @IBOutlet var lblDescription: UILabel!
    
    @IBOutlet var lblFollowrsList: TTTAttributedLabel!
    
    
    
    // outlets for tag friends view controller
    
    @IBOutlet var imgViewCheck: UIImageView!
    
    @IBOutlet var btnCheck: UIButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
