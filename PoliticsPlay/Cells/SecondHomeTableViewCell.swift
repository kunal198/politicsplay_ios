//
//  SecondHomeTableViewCell.swift
//  PoliticsPlay
//
//  Created by Tbi-Pc-22 on 23/04/18.
//  Copyright © 2018 Brst981. All rights reserved.
//

import UIKit

class SecondHomeTableViewCell: UITableViewCell {
    
    
    //Outlets
 
    @IBOutlet weak var showLike: UIButton!
    @IBOutlet weak var cellImg: UIImageView!
    @IBOutlet weak var descriptionTxt: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeText: UILabel!
    @IBOutlet weak var likeStatus: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userProfilePic: UIImageView!
    @IBOutlet weak var imgLike: UIImageView!
     @IBOutlet weak var cellView: UIView!
    
    @IBOutlet weak var likeCountSecond: UILabel!
    @IBOutlet weak var commentCount: UILabel!
    @IBOutlet weak var likeUnlikeBtn: UIButton!
    @IBOutlet weak var readMore: UIButton!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
