//
//  FollowingCell.swift
//  PoliticsPlay
//
//  Created by Brst981 on 31/10/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit

class FollowingCell: UITableViewCell {
    
    
    
    //Outlets
    @IBOutlet var btnFollowStatus: UIButton!
    @IBOutlet var lblNameDesp: TTTAttributedLabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
