//
//  HomeTableViewCell.swift
//  LayoutScreens
//
//  Created by Brst on 01/11/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    
    
    //Outlets
    @IBOutlet weak var cellContentView: UIView!
    @IBOutlet weak var cellImg: UIImageView!
    @IBOutlet weak var descriptionTxt: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeText: UILabel!
    @IBOutlet weak var likeStatus: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userProfilePic: UIImageView!
    @IBOutlet weak var imgLike: UIImageView!
    @IBOutlet weak var showImage: UIImageView!
    @IBOutlet weak var thumbnailIcn: UIImageView!
    @IBOutlet weak var outerImgView: UIImageView!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var btnTap: UIButton!
    @IBOutlet weak var likeCount: UILabel!
    @IBOutlet weak var commentCount: UILabel!
    @IBOutlet weak var likeUnlikeBtn: UIButton!
    @IBOutlet weak var readMore: UIButton!
    
    @IBOutlet weak var showLikeImg: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
