//
//  UserProfileTableViewCell.swift
//  LayoutScreens
//
//  Created by Brst on 01/11/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class UserProfileTableViewCell: UITableViewCell {
    @IBOutlet weak var cellImg: UIImageView!
    @IBOutlet var cellImageDown: UIImageView!
    @IBOutlet var userName: UILabel!
    @IBOutlet var userProfilePic: UIImageView!
    @IBOutlet var imgLike: UIImageView!
    
    @IBOutlet var userPostDesp: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var lblLike: UILabel!
    @IBOutlet var outerImgView: UIImageView!
    @IBOutlet var thumbnailOutlet: UIImageView!
    @IBOutlet var cellContentView: UIView!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var textFieldText: UITextField!
    @IBOutlet weak var viewNoComments: UIView!
    @IBOutlet weak var btnLikeDislike: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
