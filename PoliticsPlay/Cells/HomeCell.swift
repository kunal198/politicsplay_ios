//
//  HomeCell.swift
//  PoliticsPlay
//
//  Created by Brst981 on 30/10/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit

class HomeCell: UITableViewCell {
    
    
  //Outlets

    @IBOutlet var imageViewHome: UIImageView!
    @IBOutlet var lblDescriptionHome: UILabel!
    @IBOutlet var dateLabelHome: UILabel!
    @IBOutlet var timeLabelHome: UILabel!
    @IBOutlet var likeLabelHome: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
