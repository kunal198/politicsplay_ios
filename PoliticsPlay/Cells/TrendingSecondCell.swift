//
//  TrendingSecondCell.swift
//  PoliticsPlay
//
//  Created by Tbi-Pc-22 on 25/04/18.
//  Copyright © 2018 Brst981. All rights reserved.
//

import UIKit

class TrendingSecondCell: UITableViewCell {
    
    
    //Outlets
    
    @IBOutlet weak var descriptionTxt: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeText: UILabel!
    @IBOutlet weak var likeStatus: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userProfilePic: UIImageView!
    @IBOutlet weak var imgLike: UIImageView!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var btnTap: UIButton!
    @IBOutlet weak var likeCount: UILabel!
    @IBOutlet weak var commentCount: UILabel!
    @IBOutlet weak var readMore: UIButton!
    @IBOutlet weak var likeUnlikeBtn: UIButton!
   
    @IBOutlet weak var showLikeButton: UIButton!
    
    @IBOutlet weak var showLikeBtn: UIButton!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
