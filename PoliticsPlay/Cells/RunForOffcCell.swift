//
//  RunForOffcCell.swift
//  PoliticsPlay
//
//  Created by Brst981 on 13/11/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit

class RunForOffcCell: UITableViewCell {
    
    
    
    //Outlets
    
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var btnClickable: UIButton!
    @IBOutlet var btnPressed: UIButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
