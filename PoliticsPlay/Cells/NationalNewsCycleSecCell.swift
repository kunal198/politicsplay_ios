//
//  NationalNewsCycleSecCell.swift
//  LayoutScreens
//
//  Created by Brst on 31/10/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class NationalNewsCycleSecCell: UITableViewCell {
    @IBOutlet weak var cellContentView: UIView!
    @IBOutlet var imageViewNational: UIImageView!
    @IBOutlet var lblNationalCycle: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
