//
//  TagFriendsVC.swift
//  PoliticsPlay
//
//  Created by Brst981 on 08/01/18.
//  Copyright © 2018 Brst981. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


  protocol TagFriendsDelegate {
    
    func finishPassing(taggedFriends: NSMutableArray)

  }


class TagFriendsVC: UIViewController,TTTAttributedLabelDelegate {
    
    //outlets
    @IBOutlet var tableView: UITableView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    
    //variables
    var dataMutableArray = NSMutableArray()
    var userIdValueAgain = String()
    var authTokenValue  = String()
    var selectionArray = NSMutableArray()
    var delegateFriends: TagFriendsDelegate?
    var selected = String()
    var tempIndex = Int()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        if UserDefaults.standard.value(forKey: "userId") != nil{
            
            userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as! String
            authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
            print(userIdValueAgain)
            print(authTokenValue)
            self.followersList(userId: userIdValueAgain, authToken: authTokenValue)
        }
        

        
        self.activityIndicator.isHidden = true
        self.tableView.allowsMultipleSelectionDuringEditing = true
        
        
       // code to show the checked friends again for user rememberance
    //    let indexofparty = self.selectionArray.index(of: Singleton.sharedInstance.checkedFriend)
   //     print(indexofparty)
      //  self.tempIndex = indexofparty
        
        print(selectionArray)
        
        if selectionArray.isEqual(""){
        
            
            
        }
        
        else{
        
        print("hello")
     //  let index = selectionArray.index(of: )
        
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // fetching followers user list
    func followersList(userId: String,authToken: String){
        
        
        DispatchQueue.main.async{
        
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
        
        }
        
        
        let json = [
            "user_id":userId,
            "auth_token":authToken,
            ] as [String : Any]
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/politicsplay/api/web/v1/users/view_follower_list", method: .post, parameters:json, encoding: URLEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                DispatchQueue.main.async {
                    let json = JSON(data: response.data!)
                    let jsonDict = json.dictionaryObject
                    let descriptionStr = jsonDict?["message"] as! String
                    if  descriptionStr == "Followers List"{
                        
                        let dataArray = jsonDict?["data"] as! NSArray
                        print(dataArray)
                        self.dataMutableArray = dataArray.mutableCopy() as! NSMutableArray
                        print(self.dataMutableArray)
                    
                    }
                    
                    if (response.error != nil){
                    
                    
                        let alertView = UIAlertController(title: "", message:(response.error?.localizedDescription)! , preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                            
                        })
                        alertView.addAction(action)
                        self.present(alertView, animated: true, completion: nil)
                    }
                    
                    DispatchQueue.main.async{
                        
                        
                        self.activityIndicator.isHidden = true
                        self.activityIndicator.stopAnimating()
                        self.tableView.reloadData()
                        
                    }
                    
                }
        }
        
    }
    
 
    
  // code for sending data to post screen.
    @IBAction func btnDone(_ sender: Any) {
  
        delegateFriends?.finishPassing(taggedFriends:selectionArray)
        print(selectionArray)
        self.dismiss(animated: true, completion: nil)
    
    
    }
    
    
    //code to push the view controller on button click
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let userProfile = storyBoard.instantiateViewController(withIdentifier: "UserProfileVC") as! UserProfileVC
        userProfile.valuePerm = true
        self.navigationController?.pushViewController(userProfile, animated: true)
    
    }
    
    }


extension TagFriendsVC:UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dataMutableArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: "whatsOnMindCell", for: indexPath) as! WhatsOnMindNewCell
        let dict = dataMutableArray[indexPath.row] as! NSDictionary
        print(dict)
       let followersData = dict.value(forKey: "follower_data") as! NSDictionary
        
        let firstName = followersData.value(forKey: "firstname") as! NSString
        let lastName = followersData.value(forKey: "lastname") as! NSString
        let concatenateString = "\(firstName) \(lastName)"
        cell.lblFollowrsList.text = concatenateString
    
        
       // code to provide multiple selection
        if selectionArray.contains(dict) {
            print(selectionArray)
            cell.btnCheck.setImage(UIImage(named: "checkFriends"), for: UIControlState.normal)


        } else {
            
          cell.btnCheck.setImage(UIImage(named: "uncheck"), for: UIControlState.normal)

        }
        

        
        // code to make text underline
        let str : NSString = concatenateString as NSString
        cell.lblFollowrsList.delegate = self
        cell.lblFollowrsList.tag = indexPath.row
        cell.lblFollowrsList.text = str as String
        let range : NSRange = str.range(of: str as String)
        cell.lblFollowrsList.addLink(to: NSURL(string: "")! as URL!, with: range)
        cell.selectionStyle = UITableViewCellSelectionStyle.none;
 
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: "whatsOnMindCell", for: indexPath) as! WhatsOnMindNewCell
      let dict = dataMutableArray[indexPath.row] as! NSDictionary
      let followersId = dict.value(forKey: "id") as? NSInteger
      print(followersId)
        
    // code for sending selected id in add post screen
      let followersIdString: String? = String(describing: followersId)
      Singleton.sharedInstance.followerUserId = followersIdString!
      print(Singleton.sharedInstance.followerUserId)
        
        // code for multiple selection
        if selectionArray.contains(dict) {
       
        selectionArray.remove(dict)
        print(selectionArray)
      
       
        }else {
        
            selectionArray.add(dict)
            print(selectionArray)
            // for controlling delegate properties
        }
        
         print(dict)
      //  tempIndex = selectionArray[indexPath.row]
        self.tableView.reloadData()
    
    
    }
    
    
    
    
    
    
    
    
    
}
