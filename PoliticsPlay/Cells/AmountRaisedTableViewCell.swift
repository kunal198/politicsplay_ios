//
//  AmountRaisedTableViewCell.swift
//  LayoutScreens
//
//  Created by Brst on 31/10/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit

class AmountRaisedTableViewCell: UITableViewCell {
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var cellContentView: UIView!
    @IBOutlet weak var nameButton: UIView!
    @IBOutlet var imageLeftMenu: UIImageView!
    @IBOutlet var lblLeftMenu: UILabel!
    @IBOutlet var viewSeprator: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
