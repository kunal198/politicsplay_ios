//
//  DetailImageVC.swift
//  PoliticsPlay
//
//  Created by Tbi-Pc-22 on 11/04/18.
//  Copyright © 2018 Brst981. All rights reserved.
//

import UIKit
import SDWebImage





class DetailImageVC: UIViewController {
    
    
    // Variables
    var postDictData = NSDictionary()

    @IBOutlet weak var imgView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(self.postDictData)
        // Do any additional setup after loading the view.
        
       let imageUrl = postDictData.value(forKey: "image") as! String
       //self.imgView.image = UIImage(named:"imageUrl")
       self.imgView.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "noimage"))
        
    
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // Mark: Show image and video
    @IBAction func closeWindow(_ sender: Any) {
    
     self.navigationController?.popViewController(animated: true)

   }
    
    
    
    
    
    


}
