
//
//  HomeViewController.swift
//  LayoutScreens
//
//  Created by Brst on 01/11/17.
//  Copyright © 2017 Brst. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage
import AVFoundation
import AVKit


class HomeViewController: UIViewController,DetailPostDelegate{
    
    
    // Outlets
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var table_View: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
   //Variables
    var userIdValueAgain = String()
    var authTokenValue = String()
    var postArray = NSMutableArray()
    var type = String()
    var indexingDict = [String:Any]()
    var userDict = NSDictionary()
    var currentIndex = Int()
    var moreScroll = Bool()
    var img_url = String()
    var categoryType = String()
    var categoryName = String()
    var likedStatus = String()
    var lastPageValue = String()
    var postAdded = Bool()
    var postCount = Int()
    var postID = Int()

    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  self.navigationController?.isNavigationBarHidden = true
        
        postAdded = false
        
        // Mark: Showing the name on category
        if self.categoryName == "NATIONAL POLITICS"{
        
        self.lblCategoryName.text = self.categoryName
            
        }
        
        else if self.categoryName == "STATE POLITICS"{
         
            self.lblCategoryName.text = self.categoryName
            
        }
        
        else{
            
            self.lblCategoryName.text = self.categoryName

        }
        
        
      
    
    }

    
    override func viewWillAppear(_ animated: Bool) {
            
        super.viewWillAppear(true)
        currentIndex = 1

        if UserDefaults.standard.value(forKey: "userId") != nil{
            
            self.userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as! String
            self.authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
            self.postArray.removeAllObjects()
           // self.table_View.reloadData()
            postData(userId: userIdValueAgain , authToken: authTokenValue , page: "1")
            
        }
        self.navigationController?.isNavigationBarHidden = true
    
    }
    
    

    
    
    //@IBAction funcoptionsMenuAction(sender: UIButton)
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    @IBAction func popBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
   
    }

    // Mark: List of all posts through pagination
    func postData(userId: String,authToken: String,page: String){
        
        print(self.categoryType)
        
        DispatchQueue.main.async {
            
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
            
        }
        let json = ["page":page,
                    "user_id":userId,
                    "auth_token":authToken,
                    "cat":self.categoryType
                    ] as [String : Any]
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/politicsplay/api/web/v1/users/getcompainpoststory", method: .post, parameters:  json, encoding: URLEncoding.default)
            .responseJSON { response in
                
                print(response.result)   // result of response serialization
                print(response)
                if((response.error) != nil){
                    
                    let alertView = UIAlertController(title: "", message:(response.error?.localizedDescription)! , preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                        
                    })
                    alertView.addAction(action)
                    self.present(alertView, animated: true, completion: nil)
                    
                }
                DispatchQueue.main.async {
                    
                    let json = JSON(data: response.data!)
                    let data = json["data"].arrayObject
                     let descpStr = json["message"].string
                    if descpStr == "User id and authentication token not matched"{
                   
                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                        let loginVC = storyBoard.instantiateViewController(withIdentifier:
                            "LoginVC") as! LoginVC
                        self.navigationController?.pushViewController(loginVC, animated: true)
                    }
                    
                    if descpStr == "No post and story found under these campaign"{
                        
                        let alertView = UIAlertController(title: "", message:descpStr , preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                            
                        })
                        alertView.addAction(action)
                        self.present(alertView, animated: true, completion: nil)
                        
                        
                    }
                    
                     if ((data) != nil){
                        
                        
                        self.postArray.addObjects(from: data!)
                        self.indexingDict = (json["pagination"].dictionaryObject)!
                
                    }
                    
                
                    
                    DispatchQueue.main.async {
                        
                        self.activityIndicator.isHidden = true
                        self.activityIndicator.stopAnimating()
                        self.table_View.reloadData()
                   
                        

                        
                    } // end of dispatch
                    
                    
                    
                } // end of one step ahead disaptch
                
                
                
        } // end of alamofire methd
        
    }
    
    
    @IBAction func pushToDetailScreen(_ sender: Any) {
    
    
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let donateController = storyBoard.instantiateViewController(withIdentifier:
            "DetailPost") as! DetailPost
        self.navigationController?.pushViewController(donateController, animated: true)
    
    }
    
    
    
  
}

   extension HomeViewController:UITableViewDelegate,UITableViewDataSource, UIScrollViewDelegate{
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
       
        guard indexingDict.count > 0, let currentpage = (indexingDict["currentpage"] as? NSString)?.integerValue, let lastpage = (indexingDict["lastpage"] as? NSString)?.integerValue, currentpage < lastpage , let nextpage = (indexingDict["nextpage"] as? NSString)?.integerValue else {
            print("We are on last page")
             return
        }
        
        self.postData(userId: userIdValueAgain , authToken: authTokenValue, page: String(nextpage))

        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
   
        return postArray.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
      
        let dict = postArray[indexPath.row] as! NSDictionary
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! HomeTableViewCell
            //Mark: Showing the data in the table
            if (dict != nil){
                
                self.type = dict.value(forKey: "type") as! String
                let dateData = dict.value(forKey: "post_date") as?  String
                let timeData = dict.value(forKey: "post_time") as?  String
                 self.img_url  = dict.value(forKey:"image") as! String
                let thumbnail = dict.value(forKey: "thumbnail") as! String
                let likedStatus = (dict.value(forKey: "is_likedbyuser") as? String)!
                let textDescription = dict.value(forKey: "description") as? String
                let postLikes = dict.value(forKey: "post_likes") as? String
                let postComments = dict.value(forKey: "post_comments") as! String
                let userName = dict.value(forKey: "username") as! String
                let userProfilePic = dict.value(forKey: "profile_pic") as?  String
                //Mark: Showing data on cell
                if likedStatus == "yes"{
                    
                  if let image = UIImage(named: "heartlike") {
                        
                        cell.showLikeImg.setImage(image, for: UIControlState.normal)
                    }
                    
                    
                }
                else{
                    
                 
                    if let image = UIImage(named: "heartunlike") {
                        
                        cell.showLikeImg.setImage(image, for: UIControlState.normal)
                        
                    }
                    
                }
                
                cell.likeCount.text = postLikes
                
                if self.type == "post"{
                    
                    if textDescription == ""{
                        
                        cell.descriptionTxt.text = "No description added"
                        if cell.descriptionTxt.numberOfLines > 3{
                            
                            cell.readMore.isHidden = false
                            
                        }
                            
                            
                        else{
                            
                            cell.readMore.isHidden = true
                        }
                        
                    
                    }
                    else{
                        
                        cell.descriptionTxt.text = textDescription
                        if cell.descriptionTxt.numberOfLines > 3{
                            
                            cell.readMore.isHidden = false
                            
                        }
                            
                        else{
                            
                            cell.readMore.isHidden = true
                        }
                    
                    
                    
                    }
                    
                    
                    
                }
                    
                else{
                    
                    cell.descriptionTxt.text = "No description added"
                    if cell.descriptionTxt.numberOfLines > 3{
                        
                        cell.readMore.isHidden = false
                        
                    }
                        
                    else{
                        
                        cell.readMore.isHidden = true
                    }
               
                }
                
                cell.dateLabel.text = dateData
                cell.timeText.text = timeData
                cell.likeCount.text = postLikes
                cell.commentCount.text = postComments
                cell.userName.text = userName
                cell.userProfilePic.sd_setImage(with: URL(string: userProfilePic!), placeholderImage: UIImage(named: "pro"))
                cell.userProfilePic.layer.cornerRadius = cell.userProfilePic.layer.frame.size.height/2
                cell.userProfilePic.clipsToBounds = true
                
               
                let imgType = NSURL(string :img_url)?.pathExtension
                if imgType == "jpg" || imgType == "png" {
                    
                    
                    
                    cell.showImage.sd_setImage(with: URL(string: self.img_url), placeholderImage: UIImage(named: "noimage"))
                    cell.thumbnailIcn.image = UIImage(named: "")
                    cell.outerImgView.sd_setImage(with: URL(string: self.img_url), placeholderImage: UIImage(named: "noimage"))
                    
                    
                }
                else if(imgType == "mp4") {
                    
                    cell.showImage.sd_setImage(with: URL(string: thumbnail), placeholderImage: UIImage(named: "noimage"))
                    cell.outerImgView.sd_setImage(with: URL(string: thumbnail ), placeholderImage: UIImage(named: "noimage"))
                    cell.thumbnailIcn.image = UIImage(named: "playbutton")
                }
             
                    
                    // Mark: For showing other cell if there is no image
                    
                else{

                    let cell = tableView.dequeueReusableCell(withIdentifier: "homeCellData") as! SecondHomeTableViewCell
                    if (dict != nil){
                        
                        //Mark: Showing data on cell
                        if self.type == "post"{

                            if textDescription == ""{

                                cell.descriptionTxt.text = "No description added"
                                if cell.descriptionTxt.numberOfLines > 3{
                                    
                                    cell.readMore.isHidden = false
                                    
                                }
                                    
                                else{
                                    
                                    cell.readMore.isHidden = true
                                }


                            }
                            else{

                                cell.descriptionTxt.text = textDescription
                                if cell.descriptionTxt.numberOfLines > 3{
                                    
                                    cell.readMore.isHidden = false
                                    
                                }
                                    
                                else{
                                    
                                    cell.readMore.isHidden = true
                                }
                            
                            
                            }
                        }
                            // Mark: for cell having no profile picture
                        else{

                            cell.descriptionTxt.text = "No description added"
                            if cell.descriptionTxt.numberOfLines > 3{
                                
                                cell.readMore.isHidden = false
                                
                            }
                                
                            else{
                                
                                cell.readMore.isHidden = true
                            }
                       
                        
                        
                        }

                        cell.dateLabel.text = dateData
                        cell.timeText.text = timeData
                        cell.userName.text = userName
                        cell.userProfilePic.sd_setImage(with: URL(string:userProfilePic!), placeholderImage: UIImage(named: "pro"))
                        cell.userProfilePic.layer.cornerRadius = cell.userProfilePic.layer.frame.size.height/2
                        cell.userProfilePic.clipsToBounds = true
                         cell.likeCountSecond.text = postLikes
                        cell.commentCount.text = postComments
                    }
                    
                    
                    //Mark: Showing data on cell
                    if likedStatus == "yes"{
                        
                     
                        if let image = UIImage(named: "heartlike") {
                            
                            cell.showLike.setImage(image, for: UIControlState.normal)
                        }
                        
                        
                    }
                    else{
                        
                 
                        if let image = UIImage(named: "heartunlike") {
                            
                            cell.showLike.setImage(image, for: UIControlState.normal)
                            
                        }
                        
                    }
                    
                    
                    
                    cell.cellView.layer.cornerRadius = 4
                    cell.cellView.layer.shadowColor = UIColor.black.cgColor
                    cell.cellView.layer.shadowOffset = CGSize(width: 1,height: 1)
                    cell.cellView.layer.shadowOpacity = 0.5
                    cell.cellView.layer.shadowRadius = 1.0
                    cell.selectionStyle = UITableViewCellSelectionStyle.none
                    cell.readMore.tag = indexPath.row
                    cell.readMore.addTarget(self, action: #selector(pushToDetailScreen), for:UIControlEvents.touchUpInside)
                    // Mark: For like unlike scenario
                    cell.likeUnlikeBtn.tag = indexPath.row
                    cell.likeUnlikeBtn.addTarget(self, action: #selector(btnLikeUnlikeMethod), for: UIControlEvents.touchUpInside)
                    return cell
                }
           
            
            }
            
            
            cell.cellView.layer.cornerRadius = 4
            cell.cellView.layer.shadowColor = UIColor.black.cgColor
            cell.cellView.layer.shadowOffset = CGSize(width: 1,height: 1)
            cell.cellView.layer.shadowOpacity = 0.5
            cell.cellView.layer.shadowRadius = 1.0
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.btnTap.tag = indexPath.row
            cell.btnTap.addTarget(self, action: #selector(showPopup), for: UIControlEvents.touchUpInside)
            
            // Mark: For like unlike scenario
            cell.likeUnlikeBtn.tag = indexPath.row
            cell.likeUnlikeBtn.addTarget(self, action: #selector(btnLikeUnlikeMethod), for: UIControlEvents.touchUpInside)
            cell.readMore.tag = indexPath.row
            cell.readMore.addTarget(self, action: #selector(pushToDetailScreen), for:UIControlEvents.touchUpInside)
        cell.selectionStyle = .none
        return cell
            

    }
    // Height for row
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
    
         let dict = postArray[indexPath.row] as! NSDictionary
        self.img_url  = dict.value(forKey:"image") as! String
         let imgType = NSURL(string :img_url)?.pathExtension
        if imgType == "jpg" || imgType == "png" {
            
            return 300.0
            
            
        }
        else if(imgType == "mp4") {
            
          return 300.0
       
        
        }
        
        else{
            
            return 145.0
        }
        
}
    
// Mark: Like Unlike Scenario
  func btnLikeUnlikeMethod(_ sender: UIButton){
    
        self.authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
        self.userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as! String
        let dictProfile = postArray[sender.tag] as! NSDictionary
        print(dictProfile)
        self.postID = (dictProfile.value(forKey: "id") as? Int)!
       
        
        let json = ["user_id": self.userIdValueAgain ,
                    "auth_token":self.authTokenValue,
                    "post_id": self.postID
            
            ] as [String : Any]
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/politicsplay/api/web/v1/posts/likeunlikepost", method: .post, parameters:                                           json, encoding: URLEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                let currentpage = self.indexingDict["currentpage"] as? NSString
              
              //  let json = JSON(data: response.data!)
                if UserDefaults.standard.value(forKey: "userId") != nil{

                    self.postArray.removeAllObjects()
                    self.postData(userId: self.userIdValueAgain,authToken: self.authTokenValue,page:"1")

                }
                
                //  self.tableView.reloadData()
                
                
        }
        
    }
    
    //Mark: Push to detail screen from read more button
    func pushToDetailScreen(sender:UIButton){
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let detailController = storyBoard.instantiateViewController(withIdentifier: 
            "DetailPost") as! DetailPost
        detailController.postType = "Home"
       // detailController.imagePassed = UIImage(named:"")
        detailController.postHomeData = self.postArray[sender.tag] as! NSDictionary
        let dictData  =  self.postArray[sender.tag] as! NSDictionary
       detailController.postIDDetail = dictData.value(forKey: "post_id") as! Int
        self.navigationController?.pushViewController(detailController, animated: true)
   }
    
    // Mark: Show image and video
    @IBAction func showPopup(_ sender: UIButton) {
     
        
      let dict = postArray[sender.tag] as! NSDictionary
      let img_Url  = dict.value(forKey:"image") as! String
       let valueVideo = NSURL(string : img_Url)?.pathExtension
        if valueVideo == "mp4"{
            
            let videoURL = URL(string:img_Url)
            let player = AVPlayer(url: videoURL!)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.view.addSubview(playerViewController.view)
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
                
            }
            
        }
            
        else{
            
            let valueImage = NSURL(string : img_Url )?.pathExtension
                    if valueImage == "jpg"{
                
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let detailController = storyBoard.instantiateViewController(withIdentifier:
                    "DetailImageVC") as! DetailImageVC
                detailController.postDictData = self.postArray[sender.tag] as! NSDictionary
                self.navigationController?.pushViewController(detailController, animated: true)
                
    }
            
        }
    
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let postDetailViewController = storyBoard.instantiateViewController(withIdentifier:
            "DetailPost") as! DetailPost
         postDetailViewController.postType = "Home"
        
         postDetailViewController.delegate = self
         postDetailViewController.postHomeData = self.postArray[indexPath.row] as! NSDictionary
         let dictData  = self.postArray[indexPath.row] as! NSDictionary
         postDetailViewController.postIDDetail = dictData.value(forKey: "id") as! Int
        // print(self.likedStatus)
         postDetailViewController.likedByMe = dictData.value(forKey: "is_likedbyuser") as! String
         self.navigationController?.pushViewController(postDetailViewController, animated: true)
       }
    
    }
    
    
    
    
    
    
    


//MARK: SliderMenu Delegate Methods
extension HomeViewController : SlideMenuControllerDelegate {
    
    func leftDidClose() {
        
        
        
    }
    
}



 
