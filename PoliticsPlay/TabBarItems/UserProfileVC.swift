//
//  UserProfileVC.swift
//  PoliticsPlay
//
//  Created by Brst981 on 31/10/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


protocol UserProfileVCDelegate:class{
    
    func postData(userId: String,authToken: String, completion: @escaping (_ result: Bool) -> Void)
    

}

class UserProfileVC: UIViewController {
    
    
    
    //Oultets:
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet var btnFollow: UIButton!
    @IBOutlet weak var viewLasrRight: UIView!
    @IBOutlet weak var viewSecondRight: UIView!
    @IBOutlet weak var viewMid: UIView!
    @IBOutlet weak var viewFirstLeft: UIView!
    @IBOutlet weak var viewSecondLeft: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var lblFollowers: UILabel!
    @IBOutlet var lblFollowing: UILabel!
    @IBOutlet var lblSupporters: UILabel!
    @IBOutlet var lblAmountRaised: UILabel!
    @IBOutlet var imgView: UIImageView!
    @IBOutlet var viewStatus: UIView!
    @IBOutlet var btnShuffle: UIButton!
    @IBOutlet var btnSupport: UIButton!
    @IBOutlet var btnEndrose: UIButton!
    @IBOutlet var lblPartyCaption: UILabel!
    @IBOutlet var lblStatusCaption: UILabel!
    @IBOutlet var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet var titleFollowers: UILabel!
    @IBOutlet var titleFollowing: UILabel!
    @IBOutlet var titleSupporters: UILabel!
    @IBOutlet var titleAmount: UILabel!
    @IBOutlet var titlePost: UILabel!
    
    //variables
    var otherUserId = Int()
    var userIdValueAgain = String()
    var authTokenValue  = String()
    var dataArray = NSDictionary()
    var postArray = NSMutableArray()
    var statusFollow = NSString()
    var dict = NSDictionary()
    var valuePerm = Bool()
    var btnFollowStatus = NSInteger()
    var userIdOther = NSInteger()
    var statusType = String()
    var campValue = String()
    var otheruserName = String()
    var followPostAdded = String()
    weak var delegate: UserProfileVCDelegate?
    var permFollwer = Bool()
    var likeKeyStatus = String()

    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        customizeViews()
        // Mark: Through search scenrio
        
        if valuePerm {
            
            btnShuffle.setImage(UIImage.init(named: "back2"), for: .normal)
            
        }
            
        else{
            
            btnShuffle.setImage(UIImage.init(named: "tg-bar"), for: .normal)
        }
        
//        // Mark: Coming through the follower list
//        if permFollwer {
//
//            btnShuffle.setImage(UIImage.init(named: "back2"), for: .normal)
//
//        }
//
//        else{
//
//            btnShuffle.setImage(UIImage.init(named: "tg-bar"), for: .normal)
//        }
        
        
      
        
    
       
        // Mark: For fetching other name and updating it as a post
        let otherName = UserDefaults.standard.value(forKey: "profileName") as? String
        if otheruserName != ""{
            
            self.otheruserName = otherName!
        
        }
        
       }

    
    override func viewWillAppear(_ animated: Bool) {
    
        // Mark: For hitting the other user profile api
        if UserDefaults.standard.value(forKey: "userId") != nil{
            
            userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as! String
            authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
            self.postArray.removeAllObjects()
            userProfileMeth(userId: userIdValueAgain , authToken: authTokenValue)
            
        }
        
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func customizeViews(){
        
    viewStatus?.layer.shadowColor = UIColor.black.cgColor
    viewStatus?.layer.shadowOpacity = 0.25
    viewStatus?.layer.shadowRadius = 1.0
    viewStatus?.layer.shadowOffset = CGSize(width: 1, height: 1.0)
    
    btnFollow.layer.cornerRadius = btnFollow.frame.height/2
    btnFollow.clipsToBounds = true
        
    btnSupport.layer.cornerRadius = btnFollow.frame.height/2
    btnSupport.clipsToBounds = true
        
    btnEndrose.layer.cornerRadius = btnFollow.frame.height/2
    btnEndrose.clipsToBounds = true

    }
    
    
    @IBAction func toggleLeft(_ sender: Any) {
        
        // Mark: For search scenario
        if valuePerm {
        if UserDefaults.standard.value(forKey: "userId") != nil{
                
                userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as! String
                authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
                // Mark: when going back to search screen then hitting the api again for getting the results of recent search api
                delegate?.postData(userId: userIdValueAgain , authToken: authTokenValue) {
                    (result:
                    Bool) in
                    print("got back: \(result)")
                }
                
            }
            self.navigationController?.popViewController(animated: true)
   
        }
        
        else{
            
              slideMenuController()?.toggleLeft()
        
        }
        

        
        

    }
    
    
    
    @IBAction func pushToFollowers(_ sender: Any) {
  
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier:
            "FolllowersViewController") as! FolllowersViewController
        nextViewController.otherUserId = dataArray.value(forKey: "user_id") as! Int
        self.navigationController?.pushViewController(nextViewController, animated: true)
    
    }
    
    
    @IBAction func pushToFollowing(_ sender: Any) {
    
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier:
            "FollowingViewController") as! FollowingViewController
        nextViewController.otherUserId = dataArray.value(forKey: "user_id") as! Int
        nextViewController.checkUserProfile = true
        self.navigationController?.pushViewController(nextViewController, animated: true)
    
    }
    
    
    @IBAction func pushToSupporters(_ sender: Any) {
    
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier:
            "CandidateSupportVC") as! CandidateSupportVC
        self.navigationController?.pushViewController(nextViewController, animated: true)
    
    }
    
    
    
    @IBAction func pushToAmountRaised(_ sender: Any) {
    
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier:
            "AmountRaisedVC") as! AmountRaisedVC
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    
    }
    
    
    @IBAction func pushToDetailPost(_ sender: Any) {
    
        
       let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
       let nextViewController = storyBoard.instantiateViewController(withIdentifier:
           "DetailPost") as! DetailPost
        self.navigationController?.pushViewController(nextViewController, animated: true)
    
    }
    
 
    
    
  // code for pushing to donate screen
 @IBAction func pushToDonateScreen(_ sender: Any){

    
    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
    let nextViewController = storyBoard.instantiateViewController(withIdentifier:
    "DonateVC") as! ViewController
    nextViewController.isSlide = false
    self.navigationController?.pushViewController(nextViewController, animated: true)
    
    
    }
    
    // endrose functionality
    
    @IBAction func pushToEndrosePopup(_ sender: Any){
    
      let alertView = UIAlertController(title: "", message:"In Progress" , preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
            
        })
        alertView.addAction(action)
        self.present(alertView, animated: true, completion: nil)
        
    }
    
    
    // api integration in user profile

    func userProfileMeth(userId: String , authToken: String){
    
        
        print(otherUserId)
        
        self.activityIndicatorView.isHidden = false
         self.activityIndicatorView.startAnimating()
        
        
        let json = ["other_user_id":otherUserId,
                    "user_id":userId,
                    "auth_token":authToken,
                    "status":statusType
            ] as [String : Any]
        print(json)
        
        
        Alamofire.request("http://beta.brstdev.com/politicsplay/api/web/v1/users/get_other_user_profile", method: .post, parameters:json, encoding: URLEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                if((response.error) != nil){
                    
                    self.alertView(title: "", message: (response.error?.localizedDescription)!)
                    
                    
                }
                DispatchQueue.main.async {
                    let json = JSON(data: response.data! )
                    let jsonDict = json.dictionaryObject as NSDictionary?
                    let descripStr = json["message"].string
                    if descripStr == "Other user profile"{
                        
                        self.dataArray = (jsonDict?["data"] as? NSDictionary)!
                        self.userIdOther = self.dataArray.value(forKey: "user_id") as! NSInteger
                        Singleton.sharedInstance.userFirstNameStr = self.dataArray.value(forKey: "firstname")  as! String
                        Singleton.sharedInstance.userLastNameStr = self.dataArray.value(forKey: "lastname")  as! String
                        Singleton.sharedInstance.userPartyCaption = self.dataArray.value(forKey: "party")   as! NSString
                        Singleton.sharedInstance.userStatusCaption = self.dataArray.value(forKey: "status_caption")  as! String
                        Singleton.sharedInstance.userFollowers  = (self.dataArray.value(forKey: "total_followers") as? NSInteger)!
                        Singleton.sharedInstance.userFollowing = (self.dataArray.value(forKey: "no_of_following") as? NSInteger)!
                        self.btnFollowStatus = (self.dataArray.value(forKey: "is_followed")  as? NSInteger)!
                        print(self.btnFollowStatus)
                        
                        if self.btnFollowStatus == 1{
                            
                            print(self.btnFollowStatus)
                            self.btnFollow.setTitle("Following",for: .normal)
                        }
                            
                        else{
                            print(self.btnFollowStatus)
                            self.btnFollow.setTitle("Follow",for: .normal)
                            
                        }
                        
                        
                        if  self.userIdValueAgain ==  String(describing:self.userIdOther){
                            
                            print(self.otherUserId)
                            self.btnFollow.isHidden = true
                            
                        }
                        else{
                            
                            self.btnFollow.isHidden = false
                        }
                        
                        Singleton.sharedInstance.userProfilePic = (self.dataArray.value(forKey: "profile_pic") as? NSString)!
                        let post = self.dataArray.value(forKey: "posts") as? NSArray
                        self.postArray = (post?.mutableCopy() as? NSMutableArray)!
                        self.lblUserName.text = "\(String(describing: Singleton.sharedInstance.userFirstNameStr)) \(String(describing: Singleton.sharedInstance.userLastNameStr))"
                        self.lblFollowers.text = String(describing: Singleton.sharedInstance.userFollowers)
                        self.lblFollowing.text = String(describing: Singleton.sharedInstance.userFollowing)
                        self.imgView.layer.cornerRadius = self.imgView.frame.height/2
                        self.imgView.clipsToBounds = true
                        self.imgView.sd_setImage(with: URL(string: Singleton.sharedInstance.userProfilePic as String), placeholderImage: UIImage(named: "placeholder.png"))
                        if self.lblPartyCaption.text == ""{
                            
                            self.lblPartyCaption.text = "No Party Choosen"
                            self.lblPartyCaption.font = self.lblPartyCaption.font.withSize(17)
                            self.lblPartyCaption.textColor = UIColor.gray
                        }
                        else{
                            
                            self.lblPartyCaption.text = Singleton.sharedInstance.userPartyCaption as String
                        }
                        if self.lblStatusCaption.text == ""{
                            
                            self.lblStatusCaption.text = "No Caption Choosen"
                            self.lblStatusCaption.font = self.lblStatusCaption.font.withSize(17)
                            self.lblStatusCaption.textColor = UIColor.gray
                            
                        }
                        else{
                            
                            self.lblStatusCaption.text = Singleton.sharedInstance.userStatusCaption
                        }
                        
                    }
                  else if descripStr == "No data found."{
                        
                       self.alertView(title: "", message:descripStr!)
                       self.btnFollow.isHidden = true
                       self.btnSupport.isHidden = true
                       self.btnEndrose.isHidden = true
                       self.titlePost.isHidden = true
                       self.titleFollowers.isHidden = true
                       self.titleFollowing.isHidden = true
                       self.titleAmount.isHidden = true
                       self.titleSupporters.isHidden = true
                       self.viewStatus.isHidden = true
                       self.lblSupporters.isHidden = true
                       self.lblAmountRaised.isHidden = true
                       self.viewMid.isHidden = true
                       self.viewTop.isHidden = true
                       self.viewBottom.isHidden = true
                       self.viewSecondRight.isHidden = true
                       self.viewSecondLeft.isHidden = true
                       self.viewFirstLeft.isHidden = true
                        
                    }
                                        
                    DispatchQueue.main.async {
                        
                       self.activityIndicatorView.isHidden = true
                       self.activityIndicatorView.stopAnimating()
                       self.tableView.reloadData()
                  
                    }
                    
                }
                
        }

   }
    
    

    
    // Mark: Follow other user
    
    @IBAction func btnFollowOtherUser(_ sender: Any) {
        
        // follow unfollow scenario
        if self.btnFollow.title(for: .normal) == "Follow"{
            
            self.statusFollow = "1"
            self.btnFollow.setTitle("Following",for: .normal)
         
        }
        
            else {
            
            self.statusFollow = "0"
            self.btnFollow.setTitle("Follow",for: .normal)
          
       }
        
        if UserDefaults.standard.value(forKey: "userId") != nil{
            
            if statusFollow == "1"{
            
                userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as! String
                authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
                print(userIdValueAgain)
                print(authTokenValue)
                followOtherUser(userId: userIdValueAgain , authToken: authTokenValue)
                
            }
            
            else{
            
                statusFollow = "0"
                userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as! String
                authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
                print(userIdValueAgain)
                print(authTokenValue)
                followOtherUser(userId: userIdValueAgain , authToken: authTokenValue)
          }
          
            
        }
        
  }
    
   // Mark: Follow other user api integration
    func followOtherUser(userId: String , authToken: String){
    
        self.activityIndicatorView.isHidden = false
        self.activityIndicatorView.startAnimating()
        
        
        let json = ["other_user_id":otherUserId,
                    "user_id":userId,
                    "auth_token":authToken,
                    "status":statusFollow
            ] as [String : Any]
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/politicsplay/api/web/v1/users/follow_user", method: .post, parameters:                                           json, encoding: URLEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
                DispatchQueue.main.async {
                    let json = JSON(data: response.data! )
                    let descripStr = json["message"].string
                    if descripStr == "Follow Successfully"{
                        
                  //   self.alertView(title: "", message: descripStr!)
             
                      
                        if UserDefaults.standard.value(forKey: "userId") != nil{
                            
                            self.userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as! String
                            self.authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
                            self.addPostIntegration(userId: self.userIdValueAgain , authToken: self.authTokenValue)
                            
                        
                        }
                        
                 }
                        
                    else if (descripStr == "Already followed to this user"){
                    
                     self.alertView(title: "", message: descripStr!)
                    
                    }
                    
                    else if (descripStr == "No follower found to unfollow"){
                        
                        self.alertView(title: "", message: descripStr!)
                        
                    }
                        
                    else if (descripStr == "Unfollow Successfully"){
                        
                     //   self.alertView(title: "", message: descripStr!)
                        
                    }
                        
                        
                    else if((response.error) != nil){
                        
                        self.alertView(title: "", message: (response.error?.localizedDescription)!)
                 }
                    
                    DispatchQueue.main.async {
                        
                        self.activityIndicatorView.isHidden = true
                        self.activityIndicatorView.stopAnimating()
                      //  self.tableView.reloadData()
                    }
                    
                }
                
                
               
                
        }

    }
    
    
    // Mark : add post
    
    func addPostIntegration(userId: String , authToken: String){
   
        // id from run for office
        
        let valueId = UserDefaults.standard.value(forKey: "runForOfficeKey") as? String
        if valueId != nil{
            
            self.campValue = valueId!
            
        }
        
        else{
            
            self.campValue = ""

        }
        
        
        
        let value = Singleton.sharedInstance.userFirstNameStr
        self.activityIndicatorView.isHidden = false
        self.activityIndicatorView.startAnimating()
        
        let combinedValue = self.otheruserName + " " + " started following"  + " " + value
        
    
    
      let json = [
                "user_id":self.userIdValueAgain,
                "auth_token":self.authTokenValue,
                "text":combinedValue,
                "runforoffice_id":self.campValue
        ] as [String : Any]
      print(json)
    
       Alamofire.request("http://beta.brstdev.com/politicsplay/api/web/v1/users/add_post", method: .post, parameters:                                           json, encoding: URLEncoding.default)
          .responseJSON { response in
            print(response.result)   // result of response serialization
              print(response)
            self.followPostAdded = "postadded"
            UserDefaults.standard.set(self.followPostAdded, forKey:"followPostAdded")
            UserDefaults.standard.synchronize()
           DispatchQueue.main.async {
           if((response.error) != nil){
    
            self.alertView(title: "", message: (response.error?.localizedDescription)!)
         
            
            }
           DispatchQueue.main.async {
    
          self.activityIndicatorView.isHidden = true
          self.activityIndicatorView.stopAnimating()
           //  self.tableView.reloadData()
      
            }
    
             }
    
        }

    
    }
    // Alert view method
    
    func alertView(title: String, message: String){
    
        let alertView = UIAlertController(title:title, message:message , preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
            
        })
        alertView.addAction(action)
        self.present(alertView, animated: true, completion: nil)
    
    
    }
    

}
extension UserProfileVC:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return postArray.count
   
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        
        let cell  = tableView.dequeueReusableCell(withIdentifier: "userProfileCell", for: indexPath) as! UserProfileTableViewCell
       // customizeCell(indexPath: indexPath, cell: cell!)
    
        dict = (postArray[indexPath.row] as? NSDictionary)!
        let userPostDate = dict.value(forKey: "post_date") as? NSString
        let userPostTime = dict.value(forKey: "post_time") as? NSString
        let postDesp = dict.value(forKey: "description") as? NSString
        let thumbnailImage = dict.value(forKey: "thumbnail")
        // print(thumbnailImage)
        let img_url  = dict.value(forKey:"image") as? String
       // print(img_url)
        cell.lblDate.text = userPostDate! as String
        cell.lblTime.text = userPostTime! as String

     //   cell.userProfilePic.layer.cornerRadius = 10
       // cell.userProfilePic.clipsToBounds = true
        if postDesp == ""{
        
            cell.userPostDesp.text = "No description added"
        }
        else{
            
         cell.userPostDesp.text = postDesp! as String
        
        }
    // for profile pic
        let pathImage = NSURL(string :img_url!)?.pathExtension
        if pathImage  == "jpg" || pathImage  == "png" {
            
       //  cell.userImgView.sd_setImage(with: URL(string: img_url!), placeholderImage: UIImage(named: "noimage"))
         cell.thumbnailOutlet.image = UIImage(named: "")
         cell.outerImgView.sd_setImage(with: URL(string:  img_url!), placeholderImage: UIImage(named: "noimage"))


        }
            
            
        else if(pathImage  == "mp4") {
            
            cell.outerImgView.sd_setImage(with: URL(string: thumbnailImage as! String), placeholderImage: UIImage(named: "noimage"))
          //  cell.userImgView.sd_setImage(with: URL(string: thumbnailImage as! String), placeholderImage: UIImage(named: "noimage"))
            cell.thumbnailOutlet.image = UIImage(named: "playbutton")
            
        }
            
        else{
            
         //   cell.userImgView.image = UIImage(named : "noimage")
            cell.outerImgView.sd_setImage(with: URL(string: thumbnailImage as! String), placeholderImage: UIImage(named: "noimage"))
            cell.thumbnailOutlet.image = UIImage(named: "")
        }
        
        
        self.likeKeyStatus = (dict.value(forKey: "is_likedbyuser") as? String)!
        print(self.likeKeyStatus)
      //  print(self.dataArray)
        if (self.likeKeyStatus == "yes"){
            
            cell.lblLike.text = "Like"
            cell.lblLike.textColor = UIColor.blue
            cell.imgLike.image = UIImage(named:"unlike")
        }
        else{
            
            cell.lblLike.text = "Like"
            cell.lblLike.textColor = UIColor.black
            cell.imgLike.image = UIImage(named:"like")
            
        }
        
        
        
        
        
        
        
        
      //  cell.selectionStyle =   UITableViewCellSelectionStyle.none;
        
       // for shadow
        cell.cellContentView.layer.cornerRadius = 4
        cell.cellContentView.layer.shadowColor = UIColor.black.cgColor
        cell.cellContentView.layer.shadowOffset = CGSize(width: 1,height: 1)
        cell.cellContentView.layer.shadowOpacity = 0.5
        cell.cellContentView.layer.shadowRadius = 1.0
        
        
        //Mark: For like and unlike the other user profileand
        cell.btnLikeDislike.tag = indexPath.row
        cell.btnLikeDislike.addTarget(self, action: #selector(btnLikeDislikeMethd), for: UIControlEvents.touchUpInside)
        cell.selectionStyle = .none

        return cell
        
    }
    
    // mark: Like unlike scenario in other user profile
    func btnLikeDislikeMethd(sender: UIButton){
        
        self.authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
        self.userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as! String
        let dictOtherUserProfile = postArray[sender.tag] as! NSDictionary
        let postID = dictOtherUserProfile.value(forKey: "post_id") as! Int
        var index = IndexPath()
        index = IndexPath.init(item: Int(sender.tag), section: 0)
        let cell = self.tableView.cellForRow(at: index) as! UserProfileTableViewCell
        
        let json = ["user_id":self.userIdValueAgain,
                    "auth_token":self.authTokenValue,
                    "post_id":postID
            
            ] as [String : Any]
        
        print(json)
        
        Alamofire.request("http://beta.brstdev.com/politicsplay/api/web/v1/posts/likeunlikepost", method: .post, parameters:                                           json, encoding: URLEncoding.default)
            .responseJSON { response in
                print(response.result)   // result of response serialization
                print(response)
               // let json = JSON(data: response.data!)
                // Mark: For hitting the other user profile api
                if UserDefaults.standard.value(forKey: "userId") != nil{
                    
                   let userIdValueAgain = UserDefaults.standard.value(forKey: "userId") as! String
                   let authTokenValue = UserDefaults.standard.value(forKey: "authToken") as! String
                   // print(userIdValueAgain)
                   // print(authTokenValue)
                    self.userProfileMeth(userId: userIdValueAgain , authToken: authTokenValue)
                }
                
            }
        
        
        }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier:
            "DetailPost") as! DetailPost
        nextViewController.checkuserProfile = true
        nextViewController.userProfilePic = Singleton.sharedInstance.userProfilePic as String
        nextViewController.userProfileDict = (postArray[indexPath.row] as? NSDictionary)!
        let dict = postArray[indexPath.row] as? NSDictionary
        nextViewController.postIDDetail = dict?.value(forKey: "post_id") as! Int
        nextViewController.likedByMe = (dict?.value(forKey: "is_likedbyuser") as? String)!
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
   
    
    //  customize cell
    func customizeCell(indexPath: IndexPath,cell:UITableViewCell)  {
      
    }
    
   
}




