//
//  NationalNewsCycle.swift
//  PoliticsPlay
//
//  Created by Brst981 on 27/12/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit

class NationalNewsCycle: UIViewController{
    
    
    
    //Variables
    var arrImages = [UIImage]()
    var arrCategory = [String]()
    var newImage = UIImage()
    var imageNational = UIImage()
    var imageState = UIImage()

    //Outlets
    @IBOutlet var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        arrCategory = ["NATIONAL POLITICS","STATE POLITICS","LOCAL POLITICS"]
        
        // changing tab bar images as per screen sizes
   
            UITabBar.appearance().tintColor = UIColor.white
            self.tabBarController?.tabBar.barTintColor = UIColor (colorLiteralRed: 13/255, green: 55/255, blue: 127/255, alpha: 1.0)
        }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func toggleLeft(_ sender: Any) {
        
        slideMenuController()?.toggleLeft()
    }
    
    
    //MARK: Resize Image
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
       
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0,y: 0,width: newWidth,height: newHeight))
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
  
}


extension NationalNewsCycle:UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrCategory.count
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)as! WhatsOnMindNewCell
        cell.lblDescription.text = String(describing:arrCategory[indexPath.row])
        
        if String(describing:arrCategory[indexPath.row]) == "STATE POLITICS"{
           
             cell.imgView.image = UIImage(named : "imageState")
           
         
            
            
            
     //   let value = UserDefaults.standard.value(forKey: "zipCodeValue")
//       // Mark: Albama
//            for var i in 35004...36925  {
//
//                if value as! String == String(describing:i){
//
//                  cell.imgView.image = UIImage(named : "alabama")
//
//                }
//           } // end of first if loop
//
////            // Mark: Alaska
//            for var i in 99501...99950  {
//
//                if value as! String == String(describing:i){
//
//                    cell.imgView.image = UIImage(named : "alaska")
//
//                }
//            } // end of first if loop
////
////            // Mark: Arkansas
//            for var i in 71601...72959 {
//
//                if value as! String == String(describing:i){
//
//
//                   cell.imgView.image = UIImage(named : "arkansas")
//
//                }
//            } // end of first if loop
////
////
////            // Mark: Arizona
//            for var i in 85001...86556 {
//
//                if value as! String == String(describing:i){
//
//                 cell.imgView.image = UIImage(named : "arizona")
//
//                }
//            } // end of first if loop
//
////
////            // Mark:  South California
//            for var i in 90001...96162 {
//
//                if value as! String == String(describing:i){
//
//                cell.imgView.image = UIImage(named : "South Carolina")
//
//                }
//            } // end of first if loop
////            // Mark: Colorado
//            for var i in 80001...81658 {
//
//                if value as! String == String(describing:i){
//
//                 cell.imgView.image = UIImage(named : "Colorado")
//                }
//            } // end of first if loop
//
////            // Mark: Connecticut
//            for var i in 06001...06928 {
//               if value as! String == String(describing:i){
//
//                cell.imgView.image = UIImage(named : "Connecticut")
//
//                }
//            }
//
//           // Mark: Delaware
//            for var i in 19701...19980 {
//
//                if value as! String == String(describing:i){
//                   cell.imgView.image = UIImage(named : "Delaware")
//                }
//            }
//
////            // Mark: Florida
//            for var i in 32003...34997 {
//
//                if value as! String == String(describing:i){
//                   cell.imgView.image = UIImage(named : "Florida")
//                }
//            }
////
////            // Mark: Georgia
//            for var i in 30002...39901 {
//
//                if value as! String == String(describing:i){
//
//                    cell.imgView.image = UIImage(named : "Georgia")
//                }
//            }
//
////            // Mark: Hawai
//            for var i in 96701...96898 {
//
//                if value as! String == String(describing:i){
//
//                  cell.imgView.image = UIImage(named : "Hawaii")
//                }
//            }
//
////
////            // Mark: Idaho
//            for var i in 83201...83877 {
//
//                if value as! String == String(describing:i){
//
//                   cell.imgView.image = UIImage(named : "Idaho")
//                }
//            }
//        // Mark: Ilinois
//            for var i in 60001...62999 {
//
//                if value as! String == String(describing:i){
//
//
//                    cell.imgView.image = UIImage(named : "Illinois")
//                }
//            }
//
////            // Mark: Indiana
//            for var i in 46001...47997 {
//
//                if value as! String == String(describing:i){
//
//
//                    cell.imgView.image = UIImage(named : "Indiana")
//                }
//            }
//
////
////            // Mark: lowa
//            for var i in 50001...52809 {
//
//                if value as! String == String(describing:i){
//
//                    cell.imgView.image = UIImage(named : "Iowa")
//                }
//            }
//
////
////            // Mark: Kansas
//            for var i in 66002...72959 {
//
//                if value as! String == String(describing:i){
//
//
//                    cell.imgView.image =  UIImage(named : "Kansas")
//                }
//            }
//
////            // Mark: Kentucky
//            for var i in 40003...42788 {
//
//                if value as! String == String(describing:i){
//
//                    cell.imgView.image = UIImage(named : "Kentucky")
//                }
//            }
//
////
////            // Mark: Loiusana
//            for var i in 70001...71497 {
//
//                if value as! String == String(describing:i){
//
//                    cell.imgView.image = UIImage(named : "Louisiana")
//                }
//            }
//
//          // Mark: Maine
//            for var i in 03901...04992 {
//
//                if value as! String == String(describing:i){
//
//                 cell.imgView.image = UIImage(named : "Maine")
//
//                }
//            }
//
////
////            // Mark: Maryland
//            for var i in 20601...21930 {
//
//                if value as! String == String(describing:i){
//
//                    cell.imgView.image = UIImage(named : "Maryland")
//                }
//            }
//
//
////            // Mark: Massachusetts
//            for var i in 01001...05544 {
//
//                if value as! String == String(describing:i){
//                    cell.imgView.image = UIImage(named : "Massachusetts")
//                }
//            }
//
////            // Mark: Michigan
//            for var i in 48001...49971 {
//
//                if value as! String == String(describing:i){
//                    cell.imgView.image = UIImage(named : "Michigan")
//                }
//            }
////
////            // Mark: Minnesota
//            for var i in 55001...56763 {
//
//                if value as! String == String(describing:i){
//
//                cell.imgView.image = UIImage(named : "Minnesota")
//
//                }
//            }
//
//           // Mark: Missouri
//            for var i in 63001...65899 {
//
//                if value as! String == String(describing:i){
//
//                    cell.imgView.image = UIImage(named : "Missouri")
//
//                }
//            }
//
//
//            // Mark: Montana
//            for var i in 59001...59937 {
//
//                if value as! String == String(describing:i){
//
//                    cell.imgView.image = UIImage(named : "Montana")
//                }
//            }
//
////            // Mark: nebraska
//            for var i in 68001...69367 {
//
//                if value as! String == String(describing:i){
//
//                  cell.imgView.image = UIImage(named : "Nebraska")
//                }
//            }
//
////            // Mark: nevada
//            for var i in 88901...89883 {
//
//                if value as! String == String(describing:i){
//
//
//                    cell.imgView.image = UIImage(named : "Nevada")
//                }
//            }
//
////            // Mark: new hampshire
//            for var i in 03031...03897 {
//
//                if value as! String == String(describing:i){
//
//                    cell.imgView.image = UIImage(named : "New Hampshire")
//                }
//            }
//
////
////            // Mark: new jersey
//            for var i in 07001...08989 {
//
//                if value as! String == String(describing:i){
//
//
//                    cell.imgView.image = UIImage(named : "New Jersey")
//                }
//            }
////
////            // Mark: new mexico
//            for var i in 87001...88439 {
//
//                if value as! String == String(describing:i){
//
//
//                    cell.imgView.image = UIImage(named : "New Mexico")
//                }
//            }
//
////            // Mark: new york
//            for var i in 00501...14925 {
//
//                if value as! String == String(describing:i){
//
//
//                    cell.imgView.image = UIImage(named : "New York")
//                }
//            }
//
////            // Mark: north carolina
//            for var i in 27006...28909 {
//
//                if value as! String == String(describing:i){
//
//
//                    cell.imgView.image = UIImage(named : "North Carolina")
//                }
//            }
//
////
////            // Mark: north dakota
//            for var i in 58001...58856 {
//
//                if value as! String == String(describing:i){
//
//
//                    cell.imgView.image = UIImage(named : "North Dakota")
//                }
//            }
//
////
////            // Mark: ohio
//            for var i in 43001...45999 {
//
//                if value as! String == String(describing:i){
//
//                    cell.imgView.image = UIImage(named : "Ohio")
//                }
//
//            }
//
////            // Mark: oklahoma
//                    for var i in 73001...74966 {
//
//                   if value as! String == String(describing:i){
//
//                        cell.imgView.image = UIImage(named : "Oklahoma")
//                    }
//            }
//
////            // Mark: oregon
//                for var i in 97001...97920 {
//                          if value as! String == String(describing:i){
//
//                            cell.imgView.image = UIImage(named : "Oregon")
//
//                    }               }
//            // Mark: pennsylvania
//                    for var i in 15001...19640 {
//                        if value as! String == String(describing:i){
//
//
//                            cell.imgView.image = UIImage(named : "Pennsylvania")
//                       }
//                }
//            // Mark: rhode island
//                for var i in 02801...02940 {
//                if value as! String == String(describing:i){
//
//                        cell.imgView.image = UIImage(named : "Rhode Island")
//
//                }
//            }
//                    // Mark: south carolina
//                    for var i in 29001...29945 {
//                        if value as! String == String(describing:i){
//
//                            cell.imgView.image = UIImage(named : "South Carolina")
//                        }
//            }
//
////            // Mark: south dakota
//                    for var i in 57001...57799 {
//                            if value as! String == String(describing:i){
//
//                                cell.imgView.image = UIImage(named : "South Dakota")
//                                }
//                        }
//
////            // Mark: tennessee
//                for var i in 37010...38589 {
//                    if value as! String == String(describing:i){
//
//                          cell.imgView.image = UIImage(named : "Tennessee")
//                    }
//            }
////               // Mark: texas
//               for var i in 73301...88595 {
//                        if value as! String == String(describing:i){
//
//                            cell.imgView.image = UIImage(named : "Texas")
//                        }
//            }
//                // Mark: utah
//                for var i in 84001...84791 {
//                    if value as! String == String(describing:i){
//
//                        cell.imgView.image = UIImage(named : "Utah")
//                    }
//            }
//
//            // Mark: Vermont
//                    for var i in 05001...05907 {
//                        if value as! String == String(describing:i){
//
//                            cell.imgView.image = UIImage(named : "Vermont")
//                        }
//            }
//          // Mark: Virginia
//                        for var i in 20101...26886 {
//                            if value as! String == String(describing:i){
//
//                                cell.imgView.image = UIImage(named : "Virginia")
//                            }
//               }       // Mark: Washington
//                for var i in 98001...99403 {
//                                if value as! String == String(describing:i){
//
//                                    cell.imgView.image = UIImage(named : "Washington")
//                                }
//                      }
//        // Mark: West Virginia
//                    for var i in 24701...26886 {
//                        if value as! String == String(describing:i){
//
//                            cell.imgView.image = UIImage(named : "West Virginia")
//                        }
//              }
//            // Mark: Wisconsin
//                        for var i in 53001...54990 {
//                            if value as! String == String(describing:i){
//
//                                cell.imgView.image = UIImage(named : "Wisconsin")
//                            }
//            }
//
//           // Mark: Wyoming
//                            for var i in 82001...83414 {
//                                if value as! String == String(describing:i){
//                                 cell.imgView.image = UIImage(named : "Wyoming")
//                               }
//              }
            
            
            
            
            

}  // end of state politics if
        
    if String(describing:arrCategory[indexPath.row]) == "NATIONAL POLITICS"{

            cell.imgView.image = UIImage(named: "imageNational")

        }

        if String(describing:arrCategory[indexPath.row]) == "LOCAL POLITICS"{

            cell.imgView.image = UIImage(named: "imagecategory3")
    }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
      //  self.performSegue(withIdentifier: "push", sender: nil)
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let homeController = storyBoard.instantiateViewController(withIdentifier:
            "HomeVC") as! HomeViewController
        if String(describing:arrCategory[indexPath.row]) == "NATIONAL POLITICS"{
            
            homeController.categoryType = "1"
            homeController.categoryName = "NATIONAL POLITICS"
        }
        if String(describing:arrCategory[indexPath.row]) == "STATE POLITICS"{
            
            homeController.categoryType = "2"
            homeController.categoryName = "STATE POLITICS"
        }
        if String(describing:arrCategory[indexPath.row]) == "LOCAL POLITICS"{
            
            homeController.categoryType = "3"
            homeController.categoryName = "LOCAL POLITICS"

        }
        
        
        self.navigationController?.pushViewController(homeController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return tableView.frame.height/3 - 16
    
    }
    

}







