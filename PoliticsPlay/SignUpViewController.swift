//
//  SignUpViewController.swift
//  PoliticsPlay
//
//  Created by Brst981 on 03/11/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {
    
    
    
    //Outlets
    
    @IBOutlet var btnSignUp: UIButton!
    @IBOutlet var btnLogin: UIButton!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var txtFieldFName: UITextField!
    @IBOutlet var txtFieldLName: UITextField!
    @IBOutlet var txtFieldEmail: UITextField!
    @IBOutlet var txtFieldCreatePass: UITextField!
    @IBOutlet var txtFieldConformPass: UITextField!

    
    
    
    
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        customizeTF()
        customizeView()
        
        txtFieldFName.delegate = self as? UITextFieldDelegate
        txtFieldLName.delegate = self as? UITextFieldDelegate
        txtFieldEmail.delegate = self as? UITextFieldDelegate
        txtFieldCreatePass.delegate = self as? UITextFieldDelegate
        txtFieldConformPass.delegate = self as? UITextFieldDelegate
        
        }
    
    
    
    
    
    
    
    
    
    override func viewWillLayoutSubviews() {
        scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height+100)
    }
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func customizeTF(){
        
        let fullStringEmail = NSMutableAttributedString()
        let fullStringFName = NSMutableAttributedString()
        let fullStringLName = NSMutableAttributedString()
        let fullStringCreatePass = NSMutableAttributedString()
        let fullStringConformPass = NSMutableAttributedString()

        let image1Attachment = NSTextAttachment()
        let image2Attachment = NSTextAttachment()
        let image3Attachment = NSTextAttachment()
        let image4Attachment = NSTextAttachment()
        let image5Attachment = NSTextAttachment()

        let image1 = UIImage(named: "user")
        let image2 = UIImage(named: "user")
        let image3 = UIImage(named: "message")
        let image4 = UIImage(named: "padlock")
        let image5 = UIImage(named: "padlock")



        
        image1Attachment.image = image1
        image2Attachment.image = image2
        image3Attachment.image = image3
        image4Attachment.image = image4
        image5Attachment.image = image5

        
        let font = txtFieldEmail.font!
        let mid = font.descender + font.capHeight
        image1Attachment.bounds = CGRect(x: 0, y: font.descender - image1!.size.height / 2 + mid + 2, width: image1!.size.width, height:image1!.size.height).integral
        image2Attachment.bounds = CGRect(x: 0, y: font.descender - image2!.size.height / 2 + mid + 2, width: image2!.size.width, height:image2!.size.height).integral
        image3Attachment.bounds = CGRect(x: 0, y: font.descender - image3!.size.height / 2 + mid + 2, width: image3!.size.width, height:image2!.size.height).integral
         image4Attachment.bounds = CGRect(x: 0, y: font.descender - image4!.size.height / 2 + mid + 2, width: image4!.size.width, height:image2!.size.height).integral
         image5Attachment.bounds = CGRect(x: 0, y: font.descender - image5!.size.height / 2 + mid + 2, width: image5!.size.width, height:image5!.size.height).integral
        
        
        let image1String = NSAttributedString(attachment: image1Attachment)
        let image2String = NSAttributedString(attachment: image2Attachment)
        let image3String = NSAttributedString(attachment: image3Attachment)
        let image4String = NSAttributedString(attachment: image4Attachment)
        let image5String = NSAttributedString(attachment: image5Attachment)

       
        fullStringFName .append(image1String)
        fullStringLName .append(image2String)
        fullStringEmail .append(image3String)
        fullStringCreatePass .append(image4String)
        fullStringConformPass .append(image5String)

        
        
        fullStringEmail.append(NSAttributedString(string: "  Email"))
        fullStringFName.append(NSAttributedString(string: "  FirstName"))
        fullStringLName.append(NSAttributedString(string: "  LastName"))
       fullStringCreatePass.append(NSAttributedString(string: "  CreatePassword"))
        fullStringConformPass.append(NSAttributedString(string: "  ConformPassword"))

        
        
        txtFieldEmail.attributedPlaceholder = fullStringEmail
        txtFieldFName.attributedPlaceholder = fullStringFName
        txtFieldLName.attributedPlaceholder = fullStringLName
        txtFieldCreatePass.attributedPlaceholder =  fullStringCreatePass
        txtFieldCreatePass.attributedPlaceholder = fullStringConformPass

    }

    
    func textFieldShouldReturn(textField: UITextField!) -> Bool
    {
        txtFieldFName.resignFirstResponder()
        txtFieldLName.resignFirstResponder()
        txtFieldCreatePass.resignFirstResponder()
        txtFieldConformPass.resignFirstResponder()
        txtFieldEmail.resignFirstResponder()
        return true;

    
    }
    
    
    
    
    func customizeView(){
        
        btnSignUp.layer.cornerRadius = btnSignUp.frame.height/2;
        btnSignUp.clipsToBounds = true
      
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    


}
