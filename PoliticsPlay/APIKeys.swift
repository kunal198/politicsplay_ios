//
//  APIKeys.swift
//  Roasted
//
//  Created by Manish Gumbal on 21/01/17.
//  Copyright © 2017 Belal. All rights reserved.
//

class APIKeys {
   
    
    
    
    //Common
    static let kReturn     = "return"
    static let kId         = "id"
    static let kMessage    = "message"
    static let kSuccess    = "success"
    static let kData       = "data"
    static let kDate       = "date"
    static let kTimeSlot   = "time_slot"


    
    
    //User
    static let kUserId            = "user_id"
    static let kEmailId           = "email"
    static let kUserName          = "username"
    static let kDoctorRating      = "doctor_rating"
    static let kRating            = "rating"
    static let kFirstName         = "firstname"
    static let kName              = "name"

    static let kMiddleName        = "middlename"


    static let kLastName          = "lastname"
    static let kInsuranceId       = "insurance_id"
    static let kInsuranceName     = "insurance_name"
    static let kUserType          = "user_type"
    static let kPassword          = "password"
    static let kVerificationCode  = "verification_code"
    static let kPatient           = "patient"
    static let kDeviceToken       = "device_token"
    static let kDeviceType        = "device_type"
    static let kVerified          = "verified"
    static let kPhone             = "phone"
    static let kAddress           = "address"
    static let kCity              = "city"
    static let kState             = "state"
    static let kCountry           = "county"


    static let kDob               = "dob"
    static let kDoctorName        = "doctor_name"
    static let kDoctor            = "doctor"
    static let kDoctorId          = "doctor_id"
    static let kAction            = "action"

    
    

    static let kDoctorImage       = "doctor_image"
    static let kParentGuardian    = "parent_guardian"
    static let kHeight            = "height"
    static let kweight            = "weight"
    static let kEmergencyNumber   = "emergency_number"
    static let kNote              = "note"
    static let kPatientId         = "patient_id"
    static let kIsPrime           = "is_prime"
    static let kIsUsingPrimaryIns = "is_using_primary_patient_insurance"
    static let kSpeciality        = "specialty"
    static let kSpecialities        = "specialties"

    
    static let kSpecialityId      = "specialty_id"
    static let kAppointmentTypeId = "appointment_type_id"
    static let kAppointmentId     = "appointment_id"

    static let kLatitude          = "lat"
    static let kLongitude         = "longi"
    static let klong              = "lng"
    static let kAppointmentType   = "appointment_type"
    static let kAppointment       = "appointment"

    static let kApptType          = "appt_type"
    static let kType              = "type"


    static let kDescription       = "description"
    static let kAppointmentStatus = "appointment_status_id"
    static let kDateCreated       = "date_created"

    
    


    
    static let kSocialType = "social_type"

    static let kSocialId = "social_id"

  //  static let kDob = "dob"
    static let kLoginType = "login_type"
    static let kFacebookId = "facebook_id"
    static let kProfilePic = "profile_pic"
    static let kUserInfo = "user_info"

    //Settings
    static let kTitle = "title"
    static let kComment = "comment"
    
   
    
}

