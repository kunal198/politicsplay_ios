//
//  Singleton.swift
//  PoliticsPlay
//
//  Created by Brst981 on 04/12/17.
//  Copyright © 2017 Brst981. All rights reserved.
//

import UIKit

class Singleton: NSObject {

    var deviceTokenStr = String()
    var userIdStr = String()
    var auth_token = String()
    var socialLoginType = String()
    var userImageStr = String()
    var firstNameStr = String()
    var lastNameStr = String()
    var emailStr = String()
    var noOfFollwers = Int()
    var noOfFollowing = Int()
    var noOfSupporters = Int()
    var chooseParty = String()
    var statusCaption = String()
    var saveImage = String()
    var selected = String()
    var savedEmail = String()
    var chooseOffice = String()
    var addedOffice = String()
    var followerUserId = String()
    var getPostImgStr = String()
    var taggedPeople = NSMutableArray()
    var checkedFriend = String()
    var userFirstNameStr = String()
    var userLastNameStr = String()
    var userStatusCaption = String()
    var userFollowers = NSInteger()
    var userFollowing = NSInteger()
    var userPartyCaption = NSString()
    var userProfilePic = NSString()
    var zipCodeValue = NSString()
    class var sharedInstance: Singleton {
        struct Static {
            static let instance: Singleton = Singleton()
            
        }
        return Static.instance
    }

}




